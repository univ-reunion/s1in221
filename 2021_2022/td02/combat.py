"""
Simulation de combat de perso
Auteur : Seb
Date   : 2021-03-12
"""

import random

class Personnage:

    def __init__(self, nom, vie, attaque, defense):
        self.nom = nom
        self.vie = vie
        self.sante = vie
        self.attaque = attaque
        self.defense = defense
        
    def out(self):
        return self.sante == 0    
    
    def soigner(self, soin):
        self.sante = self.sante + soin
        if self.sante > self.vie:
            self.sante = self.vie
            
    def attaquer(self):
        """Attaque perso : retourne un montant de degats calculés ainsi :
        dans 5% des cas : l'attaque rate et inflige 0 dégât
        sinon un montant aléatoire est tiré entre attaque//2 et attaque*2"""
        if random.randint(1, 100) > 94:
            return 0
        else:
            return random.randint(self.attaque // 2, self.attaque * 2)
        
    
    
    def defendre(self, degats):
        """Modère un montant de dégâts : un tirage est fait entre la défense // 2
        et la défense ce montant est retiré des degats et on retourne le nouveau montant"""
        reduction = random.randint(self.defense // 2, self.defense)
        return max(0, degats - reduction)
    
    
    def subir(self, degats):
        """Mettre à jour la santé en retirant un montant de dégâts"""
        self.sante = max(0, self.sante - degats)
        

def combat(attaquant, defenseur, nb_tour_max):
    nb_tour = 0
    while nb_tour < nb_tour_max and not attaquant.out() and not defenseur.out():
        degats = attaquant.attaquer()
        degats = defenseur.defendre(degats)
        defenseur.subir(degats)
        if degats > 0:
            print(f'{attaquant.nom} porte une attaque à {defenseur.nom}')
            print(f'{degats} dégâts ; {defenseur.nom} : {defenseur.sante}/{defenseur.vie} PV')
        else:
            print(f'{attaquant.nom} rate {defenseur.nom}')
        nb_tour += 1
        if 0 < defenseur.sante < defenseur.vie * 0.2:
            soin = random.randint(5, 25)
            defenseur.soigner(soin)
            print(f'{defenseur.nom} décide de se soigner pour {soin} PV')
        else:
            attaquant, defenseur = defenseur, attaquant
        
    if attaquant.out():
        print(f'{defenseur.nom} gagne')
    else:
        print('Combat terminé')

# Programme principal
#
merlin = Personnage('Merlin', vie=80, attaque=32, defense=12)
arthur = Personnage('Arthur', vie=100, attaque=28, defense=25)
combat(arthur, merlin, 10)