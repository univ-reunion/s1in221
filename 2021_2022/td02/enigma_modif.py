# Machine Enigma

# ----------------------------
# -- CONSTANTES  DE LA MACHINE
# ----------------------------

ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

# Description des cinq rotors de la machine (Codage historique)
ROTORS = ('EKMFLGDQVZNTOWYHXUSPAIBRCJ',
        'AJDKSIRUXBLHWTMCQGZNPYFVOE',
        'BDFHJLCPRTXVZNYEIWGAKMUSQO',
        'ESOVPZJAYQUIRHXLNFTGKDCMWB',
        'VZBRGITYUPSDNHLXAWMJQOFECK')

ROTORS_NUM = tuple([ALPHABET.index(c) for c in ROTORS[i]] for i in range(5))

# Description des deux réflecteurs (Codage historique)
REFLECTEURS = ('YRUHQSLDPXNGOKMIEBFZCWVJAT', 'RDOBJNTKVEHMLFCWZAXGYIPSUQ')
REFLECTEURS_NUM = tuple([ALPHABET.index(c) for c in REFLECTEURS[i]] for i in range(2))


# ----------------------------
# -- FONCTIONS
# ----------------------------

def lettre_en_nombre(lettre) :
    """
    Fonction retournant la position d'une lettre dans l'ALPHABET
    """
    return ALPHABET.index(lettre)


def choix_rotor(numero) :
    """
    Sélection d'un rotor (version numérique) par l'utilisateur
    """
    return ROTORS_NUM[numero - 1]


def choix_reflecteur(numero) :
    """
    Sélection du réflecteur (version numérique) par l'utilisateur
    """
    return REFLECTEURS_NUM[numero - 1]


def conf_cablage_depart():
    """
    Configuration du branchement du câblage par l'utilisateur
    6 câbles relient les 6 paires de lettres
    Paramètres :
        Entrée au clavier d'une chaine de 12 caractères
    Résultat :
        Renvoie un tableau contenant les valeurs numériques correspondant aux caractères de la chaine entrée
    """
    cablage_num = []
    cables = input("\nEntrez le câblage (6 paires de  lettres) sous forme d'une chaîne de 12 lettres dans l'ordre : ").upper()
    for i in range(12):
        cablage_num.append(lettre_en_nombre(cables[i]))
    return cablage_num


def decalage_un_rang(rotor):
    """
    Décalage à gauche d'un cran du rotor
    Paramètres :
        rotor une liste de 26 nombres
    Effet :
        La liste rotor est modifiée : le premier élément devenant le dernier, etc.
    """
    a = rotor[25]
    rotor[25] = rotor[0]
    for i in range(25):
        rotor[i] = rotor[i + 1]
    rotor[24] = a


def pos_init_rotor(rotor, pos):
    """
    Réalise l'initialisation du rotor par décalage de pos crans
    Paramètres :
        rotor une liste de 26 nombres
        pos est un entier
    Effet : décale le rotor à gauche de pos crans
    """
    for _ in range(pos):
        decalage_un_rang(rotor)


def val_apres_cablage_depart(valeur, cablage_num):
    """
    Fonction de décodage des lettres câblées
    Paramètres :
        cablage_num est une liste de 12 nombres
            - les nombres d'indice impair sont ceux à coder
            - les nombres d'indice pair sont le codage à obtenir
        valeur est un entier
    Résultat :
        La nouvelle valeur (inchangée si non dans la liste du cablage)
    """
    nouv = valeur
    if nouv in cablage_num:
        n = cablage_num.index(valeur)
        if n % 2 == 0:
            nouv = cablage_num[n + 1]
        else:
            nouv = cablage_num[n - 1]
    return nouv


def passage_dans_rotor(rotor, valeur):
    """
    Fonction retournant la nouvelle valeur
    Après passage dans un rotor
    Paramètres :
        rotor une liste de 26 nombres
        valeur est un entier
    Résultat :
        Renvoie le nombre du rotor à l'indice 'valeur'
    """
    return rotor[valeur]


def passage_dans_reflecteur(reflecteur, valeur):
    """
    Fonction retournant la nouvelle valeur
    Après passage dans un réflecteur        
    Paramètres :
        reflecteur une liste de 26 nombres
        valeur est un entier
    Résultat :
        Renvoie le nombre de la liste contenu à l'indice 'valeur'
    """
    return reflecteur[valeur]


def passage_inverse_rotor(rotor, valeur):
    """
    Fonction retournant la nouvelle valeur
    Après le passage dans un rotor consécutif à celui dans un réflecteur        Paramètres :
        liste de 26 nombres
        valeur est un entier
    Résultat :
        Renvoie l'indice du nombre 'valeur'
    """
    return rotor.index(valeur)



# ----------------------------
# -- PROGRAMME PRINCIPAL
# ----------------------------

print("Machine ENIGNMA M3\n")

# INITIALISATION DES ELEMENTS
# 

## Choix des 3 rotors parmi les 5
print("\nChoissisez 3 rotors parmi les 5 : 1, 2, 3, 4 ou 5")
rotors = [None, None, None]
for i in range(3):
    rotors[i] = choix_rotor(int(input("Entrez le numéro du premier rotor choisi : ")))


## Choix de la position initiale des rotors choisis
print("\nPosition des rotors")
for i in range(3):
    p = input(f"Entrez la ‘lettre’ initiale du rotor {i+1} choisi : ").upper()
    a = rotors[i].index(lettre_en_nombre(p))
    pos_init_rotor(rotors[i], a)


## Choix du réflecteur parmi les deux
nl = int(input("\nEntrez le numéro du réflecteur choisi (1 ou 2) : "))
ref = choix_reflecteur(nl)

## Initialisation du cablage 
cables = conf_cablage_depart()

## Le message à décoder
message = input("\n\nEntrez le message à décoder : ").upper()

## Le message décodé, initialement vide
message_decode = ''

## tour est l'indice de la lettre à décoder dans le message
tour = 0  

# Boucle principale du Programme ENIGMA
# On s'arrête lorsque toutes les lettres du message sont codées
while tour < len(message):

    # pre-traitement
    if tour % 78 > 25 and tour % 78 <= 52:
    # A partir du 26° tour, c'est le rotor '2' qui se met en mouvement !
        decalage_un_rang(rotors[1])
    elif tour % 78 > 51 and tour % 78 <= 77:
        # C'est au tour du rotor '3' !
        decalage_un_rang(rotors[2])

    # On effectue les étapes : 
    # 1. recup de l'entier correspondant à la lettre courante du message à décoder
    # 2. on passe par le cablage
    # 3. on passe par les 3 rotors 1, 2, 3
    # 4 . on passe par le réflecteur
    # 5. on repasse par les rotors 3, 2, 1
    # 6. on repasse par le cable
    nombre = lettre_en_nombre(message[tour]) 
    v = val_apres_cablage_depart(nombre, cables)
    for i in range(3):
        v = passage_dans_rotor(rotors[i], v)
    v = passage_dans_reflecteur(ref, v)
    for i in range(3):
        v = passage_inverse_rotor(rotors[2-i], v)
    v = val_apres_cablage_depart(v, cables)
    message_decode += ALPHABET[v]

    # post-traitement
    if tour % 78 <= 25 : # 3 tours de 26 lettres, ça fait modulo 78 !
    # C'est le rotor'1' qui se met en mouvement !	

        # Le rotor '1' tourne d'un rang vers la gauche jusqu'au 26° tour, sa position initiale
        decalage_un_rang(rotors[0])
    tour = tour + 1

print("\n\nMessage décodé :\n", message_decode)
