# Contrôle 2e session -- Projet Python
**durée : 1h30 -- Aucun document autorisé**

On se propose de modéliser par des objets un carnet de contacts.

## La classe `Personne` 

On se propose de modéliser les contacts du carnet par un objet `Personne` caractérisée par :

- un `nom` (**obligatoire**)
- un `prenom` (**obligatoire**)
- un `email`, facultatif et qui vaudra la chaîne de caractères vide par défaut
- une `adr`(adresse), facultative aussi et valant la chaîne vide par défaut.
- un `tel` qui sera **toujours** initialisé comme un dictionnaire vide.

`nom`, `prenom` seront toujours en lettres minuscules ; `email` et `adr` lorsque fournies seront toujours stockées telles quelles. Pour les téléphones, une personne pourra en avoir plusieurs, nous détaillerons cela plus tard.

Voici des exemples de création d'instances de `Personne` :

```python
seb = Personne('Hoarau', 'Sébastien', 'seb.hoarau@univ-reunion.fr')
guido = Personne('Guido', 'van Rossum')
marie = Personne('marie', 'payet', 'marie.payet@unemail.com', '1, rue du stade')
```

**Question 1 (3 pts)**

Définissez le **constructeur** de la classe `Personne` qui respecte les informations ci-dessus.

### Les téléphones

Un téléphone c'est une catégorie et une valeur. 

**Question 2 (2 pts)**

Définir une méthode `ajoute_tel` qui prend 2 arguments en paramètres : la catégorie `categorie` et la valeur `val ` du numéro et qui ajoute au dictionnaire la paire correspondante en prenant `categorie` comme clé et `val` comme valeur.

<br>
<br>
<br>
<br>
<br>
<br>
<br>

## La classe `Carnet` 

```python
class Carnet:

    def __init__(self, iterable=None):
        self.personnes = [] if iterable is None else list(iterable)
```

### Exemple

On exécute l'instruction suivante :

```python
amis = Carnet([
    Personne('pequin','laurent', 'laurent.pequin@univ-reunion.fr'),
    Personne('sebastien', 'hoarau', 'seb.hoarau@univ-reunion.fr'),
    Personne('marie-laure', 'hoarau', 'marie.hoarau@email.com', '1 rue du stade'),
])
```

**Question 3 (4 pts)**

- Quel est le type de `amis.personnes[1]` ?
- Que vaut `amis.personnes[-1].prenom` ?
- on exécute :
    ```python
    p = amis.personnes[0]
    p.ajoute_tel('gsm', '0693001122')
    ```
    Que vaut `p.tel` ?

### Ajouter une personne

**Question 4 (3 pts)**

Ecrire une méthode `ajoute` qui prend en paramètre un `nom`, un `prenom` et éventuellement d'autres paramètres et qui crée une nouvelle personne à ajouter dans la liste des personnes. La méthode retournera l'instance `Personne` créée.

Voici le début de la méthode :

```python
def ajoute(self, nom, prenom, *args):
    # à compléter
```

<br>
<br>

### Ajouter un téléphone

**Question 5 (3 pts)**

Ecrire une méthode `ajoute_tel` qui prend une instance de `Personne` et un numéro de téléphone en paramètres et qui ajoute ce numéro à la personne concernée. Concernant les catégories, voici la règle à respecter :

- si le numéro commence par la chaîne de caractères `'0262'` ou `O263` on choisira `'fixe'` comme catégorie,
- on choisira `'gsm'` pour des numéros commençant par `'0692'` ou `'0693'` 
- pour tous les autres numéros la catégorie sera `'autre'` 


### Récupérer les données d'un fichier

Un fichier csv contient des contacts ; voici le début de ce fichier (les `...` ne font pas partie du fichier mais signifient qu'il y a d'autres entrées):

```
sophie;brugnon;sophie@coolmail.com;12, rue de l'église;0692123456;026200112233
mairie;saint-denis;contact@saint-denis.re;rue de la victoire;0262888888
...
```
**attention** le contact _Sophie_  est sur 2 lignes uniquement à cause de l'affichage mais il s'agit bien d'une unique ligne dans le fichier.

**Question 6 (5 pts)**

Définir une méthode `read` qui prend le nom d'un tel fichier en paramètre et qui ajoute dans le carnet les contacts qui y sont enregistrés. On utilisera des appels à la méthode `ajoute` définie précédemment et on rajoutera le numéro de téléphone via la méthode `ajoute_tel` créée aussi précédemment.

_Indication_ 

Pour le traitement des éventuels numéros de téléphones, inspirez-vous de ceci : si on a une liste : `datas = [1, 2, 3, 4, 5, 6, 7]` alors on peut récupérer les valeurs dans des variables comme ceci :

```python
a, b, c, d, *reste = datas
```

Dès lors `reste` vaut la liste `[5, 6, 7]`.

