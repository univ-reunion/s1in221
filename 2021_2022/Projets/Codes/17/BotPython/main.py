import discord
from discord.ext import commands
import youtube_dl
import asyncio
import os
import random
from bisect import bisect_left
import sys
import subprocess

bot = commands.Bot(command_prefix="!", description = "pythonTest")
musics = {}
ytdl = youtube_dl.YoutubeDL()
args = []
current_channel = 799215062513877042


@bot.event
async def on_ready():
    print("Ready")

@bot.command()
async def coucou(ctx):
    await ctx.send("Coucou !")

@bot.command()
async def serverInfo(ctx):
    server = ctx.guild
    numberOfTextChannels = len(server.text_channels)
    numberOfVoiceChannels = len(server.voice_channels)
    serverDescription = server.description
    numberOfPerson = server.member_count
    serverName = server.name
    message = f"Le serveur **{serverName}** contient *{numberOfPerson}* personnes ! \nLa description du serveur est {serverDescription}. \nCe serveur possède {numberOfTextChannels} salons écrit et {numberOfVoiceChannels} salon vocaux."
    await ctx.send(message)
    

    
@bot.event
async def on_member_join(member):
    channel = member.guild.get_channel(714786510238384532)
    await channel.send(f" {member.mention} ! Bienvenue :)")
    
async def createMutedRole(ctx):
    mutedRole = await ctx.guild.create_role(name = "Muted",
                                            permissions = discord.Permissions(
                                                send_messages = False,
                                                speak = False),
                                            reason = "Creation du role Muted pour mute des gens.")
    for channel in ctx.guild.channels:
        await channel.set_permissions(mutedRole, send_messages = False, speak = False)
    return mutedRole

async def getMutedRole(ctx):
    roles = ctx.guild.roles
    for role in roles:
        if role.name == "Muted":
            return role
    
    return await createMutedRole(ctx)

@bot.command()
async def mute(ctx, member : discord.Member, *, reason = "Aucune raison n'a été renseigné"):
    mutedRole = await getMutedRole(ctx)
    await member.add_roles(mutedRole, reason = reason)
    await ctx.send(f"{member.mention} a été mute !")

@bot.command()
async def unmute(ctx, member : discord.Member, *, reason = "Aucune raison n'a été renseigné"):
    mutedRole = await getMutedRole(ctx)
    await member.remove_roles(mutedRole, reason = reason)
    await ctx.send(f"{member.mention} a été unmute !")
    
#Fonction pour exécuter une commande et récupérer la sortie ( le message de sortie sur stdout )
@bot.command()
async def runCommand(cmd):
    #Stocker l'exécution de la commande dans la variable result
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    #Retourner le message retourné par l'exécution de la commande
    await result.stdout.read()

    #Exemple pour utiliser avec un script python
    path = "C:\Users\pcgamer\Desktop\BotPython\lolFonction\principal.py"
    msgDuScript = runCommand("python {0}".format(path))
    await("Le résultat du script: {0}".format(msgDuScript))
    


bot.run("ODM5NzgzNTk5MjYzODQyMzQ1.YJOrbA.o9lfL1PTD0dmBEnLunwdjHuTeKo")


