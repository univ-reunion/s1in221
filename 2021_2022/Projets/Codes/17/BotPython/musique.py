import discord
import os

from discord.ext import commands

TokenFile = open("./data/Token.txt", "r") 
TOKEN = TokenFile.read() 

OWNERID = 733370212199694467


bot = commands.Bot(command_prefix = "!", case_insensitive=True)

@bot.event
async def on_ready():
    print("Bot is ready")
    


@bot.event 
async def on_command_error(ctx,error):
    embed = discord.Embed(
    title='',
    color=discord.Color.red())
    if isinstance(error, commands.CommandNotFound):
        pass
    if isinstance(error, commands.MissingPermissions):
        embed.add_field(name=f'pas de permission', value=f'Tu n a pas cette permission {error.missing_perms} .')
        await ctx.send(embed=embed)
    else:
        embed.add_field(name = f':x: Terminal Error', value = f"```{error}```")
        await ctx.send(embed = embed)
        raise error

@bot.command()
async def load(ctx, extension):
    if ctx.author.id == OWNERID:
        bot.load_extension(f'Cogs.{extension}')
        await ctx.send(f"Enabled the Cog!")
    else:
        await ctx.send(f"nope")

@bot.command()
async def unload(ctx, extension):
    if ctx.author.id == OWNERID:
        bot.unload_extension(f'Cogs.{extension}')
        await ctx.send(f"Disabled the Cog!")
    else:
        await ctx.send(f"nope")

@bot.command(name = "reload")
async def reload_(ctx, extension):
    if ctx.author.id == OWNERID:
        bot.reload_extension(f'Cogs.{extension}')
        await ctx.send(f"Reloaded the Cog!") 
    else:
        await ctx.send(f"You are not cool enough to use this command")

for filename in os.listdir('./Cogs'):
    if filename.endswith('.py'):
        try:
            bot.load_extension(f'Cogs.{filename[:-3]}')
        except Exception:
            raise Exception
        

bot.run(str(TOKEN)) 