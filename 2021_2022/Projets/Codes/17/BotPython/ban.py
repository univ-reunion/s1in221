import discord
from discord.ext import commands
import random

bot = commands.Bot(command_prefix = "!", description = "Bot python")
funFact = ["oui ?"]

@bot.event
async def on_ready():
    print("Ready !")

@bot.command()
async def ban(ctx, user : discord.User, *, reason = "Aucune raison n'a été donné"):
    #await ctx.guild.ban(user, reason = reason)
    embed = discord.Embed(title = "**Banissement**", description = "Bye Bye", url = "https://www.bing.com/videos/search?q=rick+roll&docid=608008030215424307&mid=717C085AC21BA976797D717C085AC21BA976797D&view=detail&FORM=VIRE", color=0xfa8072)
    embed.set_author(name = ctx.author.name, icon_url = ctx.author.avatar_url, url = "https://www.bing.com/videos/search?q=rick+roll&docid=608008030215424307&mid=717C085AC21BA976797D717C085AC21BA976797D&view=detail&FORM=VIRE")
    embed.set_thumbnail(url = "https://discordemoji.com/assets/emoji/BanneHammer.png")
    embed.add_field(name = "Membre banni", value = user.name, inline = True)
    embed.add_field(name = "Raison", value = reason, inline = True)
    embed.add_field(name = "Modérateur", value = ctx.author.name, inline = True)
    embed.set_footer(text = random.choice(funFact))
    reason = " ".join(reason)
    await ctx.guild.ban(user, reason = reason)
    await ctx.send(f"{user} à été ban pour la raison suivante : {reason}.")

    await ctx.send(embed = embed)

@bot.command()
async def unban(ctx, user, *reason):
    reason = " ".join(reason)
    userName, userId = user.split("#")
    bannedUsers = await ctx.guild.bans()
    for i in bannedUsers:
        if i.user.name == userName and i.user.discriminator == userId:
            await ctx.guild.unban(i.user, reason = reason)
            await ctx.send(f"{user} à été unban.")
            return
    #Ici on sait que lutilisateur na pas ete trouvé
    await ctx.send(f"L'utilisateur {user} n'est pas dans la liste des bans")

@bot.command()
async def kick(ctx, user : discord.User, *reason):
    reason = " ".join(reason)
    await ctx.guild.kick(user, reason = reason)
    await ctx.send(f"{user} à été kick.")

@bot.command()
async def clear(ctx, nombre : int):
    messages = await ctx.channel.history(limit = nombre + 1).flatten()
    for message in messages:
        await message.delete()
    

bot.run("ODM5NzgzNTk5MjYzODQyMzQ1.YJOrbA.o9lfL1PTD0dmBEnLunwdjHuTeKo")