import tkinter as tk
import csv
from random import *

first=[]
init_item = []
init = []

#def boutton
def r_top():
    with open("topchamp.csv") as f:
        tc = csv.reader(f)
        for n in tc:
            first.append(n)
        print("fini top")

def r_mid():
    with open("midchamp.csv") as f:
        i= csv.reader(f)
        for n in i:
            first.append(n)
        print("fini mid")

def r_jgl():
    with open("jglchamp.csv") as f:
        j= csv.reader(f)
        for n in j:
            first.append(n)
        print("fini jgl")

def r_supp():
    with open("suppchamp.csv") as f:
        s = csv.reader(f)
        for n in s:
            first.append(n)
        print("fini supp")

def r_adc():
    with open("adcchamp.csv") as f:
        a= csv.reader(f)
        for n in a:
            first.append(n)
        print("fini adc")
            
def random_champ():
    x = choice(first)
    return x

def random_items():
    for i in init_item:
        a = choice(init_item)
        for i in init_item:
            b = choice(init_item)
            for i in init_item:
                c = choice(init_item)
                for i in init_item:
                    d = choice(init_item)
                    for i in init_item:
                        e = choice(init_item)
                        while a!=b and a!=c and a!=d and a!=e and b!=c and b!=d and b!=e and c!=d and c!=e and d!=e:
                            return (a, b, c, d, e)
                        
with open("items.csv") as i_tem:
    reader = csv.reader(i_tem)
    for n in reader:
        init_item.append(n[0])

with open("rune.csv") as r_une:
    reader = csv.reader(r_une)
    for n in reader:
        init.append(n[0])

def aleat():
    a = choice(init)
    b = choice(init)
    while a == b:
        b = choice(init)
    return a,b
runes=aleat()
#--------------------------------------------------------------------------
class Rune: # contient les infos sur les runes
    def __init__(self, nom, keystone, slot1, slot2, slot3):
        self.nom = nom # nom de la rune
        self.sous_rune =[keystone, slot1, slot2, slot3] # faciliter le choix aleatoir
        self.keystone = keystone # 
        self.slot1 = slot1
        self.slot2 = slot2
        self.slot3 = slot3
precision_keystone = ['Press the Attack','Lethal Tempo','Fleet Footwork', 'Conqueror']
precision_slot1 = ['Overheal','Triumph','Presence of Mind']
precision_slot3 = ['Coup de Grace','Cut Down','Last Stand']
precision_slot2 = ['Legend Alacrity','Legend Tenacity','Legend Bloodline']

rune_precision = Rune('PRECISION', precision_keystone, precision_slot1, precision_slot2, precision_slot3)  # cree la classe rune avec les valeur pour precision


domination_keystone = ['Electrocute','Predator','Dark Harvest','Hail of Blades']
domination_slot1 = ['Cheap Shot','Taste of Blood','Sudden Impact']
domination_slot2 = ['Zombie Ward','Ghost Poro','Eyeball Collection']
domination_slot3 = ['Ravenous Hunter','Ingenious Hunter','Relentless Hunter','Ultimate Hunter']
rune_domination = Rune('DOMINATION', domination_keystone, domination_slot1, domination_slot2, domination_slot3)

sorcery_keystone = ['Summon Aery','Arcane Comet','Phase Rush']
sorcery_slot1 = ['Nullifying Orb','Manaflow Band','Nimbus Cloak']
sorcery_slot2 = ['Transcendence','Celerity','Absolute Focus']
sorcery_slot3 = ['Scorch','Waterwalking','Gathering Storm']
rune_sorcery = Rune('SORCERY', sorcery_keystone, sorcery_slot1, sorcery_slot2, sorcery_slot3)

resolve_keystone = ['Grasp of the Undiying','Aftershock','Guardian']
resolve_slot1 = ['Demolish','Front of line','Shiled Bash']
resolve_slot2 = ['Conditioning','Second wind','Bone Plating']
resolve_slot3 = ['Overgrowth','Revitalize','Unflitching']
rune_resolve = Rune('RESOLVE', resolve_keystone, resolve_slot1, resolve_slot2, resolve_slot3)

inspiration_keystone = ['Glacial Augment','Unsealed Spellbook','Pototype Omnistone']
inspiration_slot1 = ['Hextech Flashtrption','Magical Footwear','Perfect Timing']
inspiration_slot2 = ['Futures Market','Minion Dematerializer','Biscuit Delivery']
inspiration_slot3 = ['Cosmic Insight','Approach Velocity','Time Warp Tonic']

rune_inspiration = Rune('INSPIRATION', inspiration_keystone, inspiration_slot1, inspiration_slot2, inspiration_slot3)


rune_array = [rune_precision, rune_domination, rune_sorcery, rune_resolve, rune_inspiration]  # tableau qui contien tte les classes
choix=[]
for rune in rune_array:
    if rune.nom == runes[0]:
      for sous_rune in rune.sous_rune:
          choix.append(choice(sous_rune))
          
for rune in rune_array:
    if rune.nom == runes[1]:
        choix1 = randint(1,3)
        choix2 = randint(1,3)
        while choix1 == choix2:
            choix2 = randint(1,3)
        choix_rune1 = choice(rune.sous_rune[choix1])
        choix_rune2 = choice(rune.sous_rune[choix2])
        h= (choix_rune1,choix_rune2)
#-------------------------------------------------------------------------
            
def affichage_resultat():
    s=random_items()
    l=random_champ()
    res.create_text(50,25,text=l, fill="red")
    res.create_text(350,52, text=s, fill="orange")
    res.create_text(50,80, text=runes[0], fill="blue")
    res.create_text(200,115, text= choix, fill="black")
    res.create_text(50,150,text=runes[1], fill="blue")
    res.create_text(100,185 ,text=h, fil="black")


#affichage
app = tk.Tk()
app.title("randomiser")
app.geometry("825x400")
res = tk.Canvas(app, bg="white", height=400, width=750)
res.create_text(50,10,text="Votre champion:", fill="black")
res.create_text(35,40,text="Vos items:", fill="black")
res.create_text(85,65,text="Votre premiere rune princiaple:", fill="black")
res.create_text(70,100,text="Vos premiere sous runes:", fill="black")
res.create_text(90,135,text="Votre deuxieme rune princiaple:", fill="black")
res.create_text(65,170,text="Deuxiemes sous runes:", fill="black")
top =tk.Button(app, text= "top ",command=r_top)
mid =tk.Button(app, text= "mid ",command=r_mid)
jgl =tk.Button(app, text= "jgl ",command=r_jgl)
supp =tk.Button(app, text= "supp",command=r_supp)
adc =tk.Button(app, text= "adc ",command=r_adc)
alea = tk.Button(app, text="aléatoire",command=random)
show = tk.Button(app,text="afficher", command= affichage_resultat)
fermer = tk.Button(app,text = "exit", command = app.destroy)

res.pack(side=tk.LEFT,padx=5)
top.pack(side=tk.TOP,padx=5,pady=7)
mid.pack(side=tk.TOP,padx=5,pady=7)
jgl.pack(side=tk.TOP,padx=5,pady=7)
supp.pack(side=tk.TOP,padx=5,pady=7)
adc.pack(side=tk.TOP,padx=5,pady=7)
alea.pack(side=tk.BOTTOM, pady=3)
show.pack(side=tk.BOTTOM, pady=3)
fermer.pack(side=tk.BOTTOM, pady=3)


    


app.mainloop()