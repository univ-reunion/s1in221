n = int(input())
l=[]
ecart=[]
#initialisation des variable et listes
for i in range(n):
    pi = int(input())
    l.append(pi)
#boucle qui récupère les valeurs de puissances et les range dans une liste
l.sort()
#tri de la liste en ordre croissant
for i in range(len(l)-1):
    ec=abs(l[i]-l[i+1])
#récupération de la valeur de l'écart entre chaque puissance deux à deux dans l'ordre 
    ecart.append(ec)
#ajoute chaque valeur d'écart dans la liste ecart
print(min(ecart))
#affiche la valeur de l'écart le plus faible de la liste