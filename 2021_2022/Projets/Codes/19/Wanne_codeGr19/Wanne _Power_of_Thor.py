import sys
import math

light_x, light_y, initial_tx, initial_ty = [int(i) for i in input().split()]
#initialisation des variables 

# game loop
while True:
    remaining_turns = int(input()) 
    move=""
    
    if initial_ty>light_y:
        move+="N"
        initial_ty-=1
    elif initial_ty<light_y: 
        move+="S"
        initial_ty+=1
    #Donne le déplacement de Thor sur l'axe des y

    if initial_tx>light_x:
        move+="W"
        initial_tx-=1
    elif initial_tx<light_x: 
        move+="E"
        initial_tx+=1
    #Donne le déplacement de Thor sur l'axe des x
    
    print (move)
    #affiche la direction vers laquelle Thor doit aller