import sys
import math


surface_n = int(input())  #nombre de points utilisés pour dessiner la surface de mars
for i in range(surface_n):
    # land_x: coordonnée x d'un point de la surface (0 à 6999)
    # land_y: coordonnée y d'un point de la surface.  En reliant les points entre eux de manière séquentielle on obtient la surface de Mars
    land_x, land_y = [int(j) for j in input().split()]

# game loop
while True:
    # h_speed: la vitesse horizontale de Mars Lander(en m/s), peut être negative
    # v_speed:  vitesse verticale de Mars Lander (en m/s), peut être négative
    # fuel:  la quantité de fuel restant en litre.
    # rotate: l’angle de rotation de Mars Lander en degré (-90 à 90).
    # power: la puissance des fusées de la capsule  (0 à 4).
    x, y, h_speed, v_speed, fuel, rotate, power = [int(i) for i in input().split()]
#initialisation des variables
    if v_speed<-40 and power<4:
        power+=1
#augmente la puissance de 1 lorsque la vitesse verticale est inférieure à 40 et la puissance inférieure à 4
    print("0 "+str(power))
#affiche l'angle de rotation et la puissance de la fusée