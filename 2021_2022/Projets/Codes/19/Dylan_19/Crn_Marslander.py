import sys
import math


surface_n = int(input())  # the number of points used to draw the surface of Mars.
for i in range(surface_n):
    # land_x : Coordonnée X d'un point de la surface. (0 à 6999)
    # land_y : Coordonnée Y d'un point de la surface. En reliant tous les points entre eux de manière séquentielle, vous formez la surface de Mars.
    land_x, land_y = [int(j) for j in input().split()]

# game loop
while True:
    # h_speed : la vitesse horizontale (en m/s), peut être négative.
    # v_speed : la vitesse verticale (en m/s), peut être négative.
    # fuel : la quantité de carburant restant en litres.
    # rotate : l'angle de rotation en degrés (-90 à 90).
    # power : la puissance de la poussée (0 à 4).
    x, y, h_speed, v_speed, fuel, rotate, power = [int(i) for i in input().split()]


    if v_speed <= -40: #La condition indique si la vitese est inférieur à 40
        #affichel la puissance de poussée qui est compris entre 0 et 4
        print("0 4") 
    else:           
        print("0 0") 
        #affiche la puissance de la poussée de la poussée qui est de zero 