import sys
import math


n = int(input())  # le nombre de températures à analyser
j = 0
temperature_plus_proche_de_zero = 0
for i in input().split():
  
      
    # t: une température exprimée sous la forme d'un nombre entier compris entre -273 et 5526
    t = int(i)
    # Si c'est bien la température qui nous est donnée dans l'énoncé de l'exercice
    if j == 0 :
        temperature_plus_proche_de_zero = t

    elif abs(t) <abs(temperature_plus_proche_de_zero):
        temperature_plus_proche_de_zero = t
     
    elif abs(t) == abs (temperature_plus_proche_de_zero) and t > temperature_plus_proche_de_zero:
        temperature_plus_proche_de_zero = t

    j = j + 1    

print(temperature_plus_proche_de_zero)