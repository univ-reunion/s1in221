import sys, math

lightX, lightY, initialTX, initialTY = [int(i) for i in input().split()]
thorX=initialTX
thorY=initialTY
# game loop
while 1:
    remainingTurns = int(input())

    directionX="" #Ces deux variables contient les directions du Dieu du tonnerre qu'il vas emprunter sur les deux axes.
    directionY="" 
   
    #La condition indique le déplacement du Dieu du tonnerre sur l'axe des Y : N, S
    if thorY>lightY:
        directionY+="N"
        thorY-=1


    elif thorY<lightY:
        directionY+="S"
        thorY+=1


    #La condition indique le déplacement du Dieu du tonnerre sur l'axe des X : W, E
    if thorX>lightX:
        directionX+="W"
        thorX+=1


    elif thorX<lightX:
        directionX+="E"
        thorX-=1
    
    
    print(directionY+directionX)  