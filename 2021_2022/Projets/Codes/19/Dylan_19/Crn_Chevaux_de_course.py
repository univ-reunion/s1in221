import sys
import math


n = int(input())
listes_des_chevaux = list()
puissance_inférieur = 10000000
for i in range(n):
    pi = int(input())

    #Si on nous donne la puissance du premier cheval
    if i==0:
        listes_des_chevaux.append(pi)

    else:
        #On parcourt la liste des chevaux déjà effectuer pour l'insérer au bon endroit puis on vas trier le tableau
        for j in range (i):
            if pi < listes_des_chevaux[j]:
                listes_des_chevaux.insert(j, pi)
                break

            elif j==i-1:
                listes_des_chevaux.append(pi)
# On parcourt la liste des chevaux déjà effectuer pour l'insérer au bon endroit puis on vas trier le tableau 

for i in range(1, len(listes_des_chevaux)): 
    if listes_des_chevaux[i] -listes_des_chevaux[i-1] <puissance_inférieur:
        puissance_inférieur = listes_des_chevaux[i] - listes_des_chevaux[i-1]  


print(puissance_inférieur)