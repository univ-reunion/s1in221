import sys
import math


# game loop
while True:
    montagne_max = 0
    indice_max = 0
    for i in range(8):
        mountain_h = int(input()) #Cette variable représente la taille des montagnes de 0 à 9.
        #Cette condition permettra au vaisseau de savoir sur quelle montagne tirer en affichant le plus grand à chaque fois 
        if mountain_h > montagne_max:
            montagne_max = mountain_h
            indice_max = i

    print(indice_max)
