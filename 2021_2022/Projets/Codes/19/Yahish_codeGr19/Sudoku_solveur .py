import sys
import math


#cette boucle va transformer l'entrée standard (la grille) en liste de listes pour pouvoir la parcourir et donc la manipuler     
grille=[]
for i in range (9):
    line=input()
    grille.append([int(ligne) for ligne  in list (line)]) 

def chiffre_valide (ligne,colonne,chiffre):
    #Ici on détermine si le chiffre n'est pas déja présent dans la ligne  
    for colonne2 in range (9):
        if grille[ligne][colonne2] == chiffre :
            return False

     #Ici on détermine si le chiffre n'est pas déja présent dans la colonne
    for ligne2 in range (9):
        if grille[ligne2][colonne] == chiffre:
            return False
    #Ici on détermine si le chiffre n'est pas déja présent dans la sous-grille 
    ligne2 = (ligne//3)*3
    colonne2 = (colonne//3)*3
    for i in range (0,3):
        for j in range(0,3):
            if grille[ligne2+i][colonne2+j] == chiffre:
                return False 

    return True                   
        
    #fonction principale qui sert à parcourir la grille et la remplir tout en vérifiant avec la fonction chiffre_valide 
def resoudre():
    for ligne  in range(9):
        for colonne in range(9):
            if grille[ligne][colonne] == 0:
                for chiffre in range(1,10):
                    if chiffre_valide (ligne,colonne,chiffre):
                        grille[ligne][colonne]=chiffre
                        resoudre()
                        grille[ligne][colonne] = 0
                return

    for i in range (9):
        for j in range (9):
            print (grille[i][j], end="")
        #Ce print va imprimer la grille sous la forme de l'entrée standard    
        print()         
       
#On appelle la fonction pour avoir l'affichage voulue
resoudre()