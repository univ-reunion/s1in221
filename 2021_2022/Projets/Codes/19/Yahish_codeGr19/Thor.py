import sys
import math

light_x, light_y, initial_tx, initial_ty = [int(i) for i in input().split()]

# game loop
while True:
    remaining_turns = int(input())  #ceci est le nombre de tours restant pour Thor.

  
    deplacement= ""

# Les conditions qui permettront à Thor de se déplacer dans la direction adéquate. 
    if initial_ty > light_y:
        deplacement += "N"
        initial_ty -= 1
    
    elif initial_ty < light_y:
        deplacement += "S"
        initial_ty += 1
    
    if initial_tx > light_x:
        deplacement += "W"
        initial_tx -= 1 

    elif initial_tx < light_x:
        deplacement += "E"
        initial_tx += 1   

#Ce print imprimera tous les déplacements que Thor doit faire (la variable deplacement qui corespond à une chaîne de caractères)

    print(deplacement)
