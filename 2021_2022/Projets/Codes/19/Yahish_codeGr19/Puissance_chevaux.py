import sys
import math



N = int(input())
# cette liste en compréhension correspond à la puissance de chaque cheval que l'on va ensuite trier
pi= [int(input()) for i in range (N)]
pi.sort( )

#Ce programme va ensuite prendre cette liste triée pour voir l'écart de puissance le plus faible 
minimum_ecart_de_puissance = 10000000
for j in range (N-1):
    if (pi[j+1]-pi[j]) < minimum_ecart_de_puissance:
        minimum_ecart_de_puissance = pi[j+1] - pi[j]


print(str(minimum_ecart_de_puissance))
