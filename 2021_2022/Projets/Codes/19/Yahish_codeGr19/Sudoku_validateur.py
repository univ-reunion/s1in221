import sys
import math

# On construit une liste grille pour pouvoir manipuler la grille en question pour pouvoir le valider.
grille=[]
for i in range(9):
    n = [int(j) for j in input().split()]
    grille.append(n)

#Cette fonction permet de savoir si oui ou non les lignes de cette grille sont valides en les parcourant. 
def check_rows(grille):
    for ligne in range (len(grille)):
        for colonne in range (len(grille)):
            for colonna in range (len(grille)):
                if colonna!=colonne and grille[ligne][colonne]==grille[ligne][colonna]:
                    return False
    else:
        return True

#Cette fonction permet de savoir si oui ou non les colonnes de cette grille sont valides en les parcourant. 

def check_cols(grille):
    for ligne in range (len(grille)):
        for colonne in range (len(grille)):
            for colonna in range (len(grille)):
                if colonna!=colonne and grille[colonne][ligne]==grille[colonna][ligne]:
                    return False
    else:
        return True

#Cette fonction permet de savoir si oui ou non les sous-grilles de cette grille sont valides en les parcourant. 
def check_regions(grille):
    for l in (0,6,3) :
        for c in (0,6,3):
             for la in range (3):
                 for ca in range (3):
                      for lb in range (3):
                          for cb in range (3):
                              if (la,ca)!=(lb,cb) and grille[l+la][c+ca]==grille[l+lb][c+cb]:
                                  return False
    else:
        return True

#Celle-ci est la fonction principale qui permet de valider l'ensemble de la grille en rassemblant les autres fonctions
def check_sudoku (grille):
    if check_rows(grille)==False or check_cols(grille)==False or check_regions(grille)==False:
        return "false"
    else:
        return "true"


answer= check_sudoku(grille)
print (answer)