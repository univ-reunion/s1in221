#Lire la taille du diagramme.
W, H = map(int, input().split())

#Lire les meilleures étiquettes et créer des indices de démarrage.
T = input().split('  ')
Ti = list(range(len(T)))

#Lire les lignes du diagramme.
for i in range(H - 2):
    line = input().split('|')
    
    #Recherche des lignes horizontales.
    for j in range(len(line)):
        if line[j] == '--':
            
            #Vérifier où se trouve la ligne horizontale et modifier les indices.
            for k in range(len(Ti)):
                if Ti[k] == j - 1:
                    Ti[k] += 1
                elif Ti[k] == j:
                    Ti[k] -= 1

#Lire les étiquettes du bas.
B = input().split('  ')

#affiché le résultat.
for i in range(len(T)):
    print(T[i] + B[Ti[i]])