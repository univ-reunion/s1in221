import pygame

pygame.init()
screen = pygame.display.set_mode((1080, 600))
background = pygame.image.load('fond_jouer.jpg') # importer l'image pour mettre en fond dans la variable background 
running = True
while running:
	screen.blit(background, (0, 0))
	police = pygame.font.Font("game_over.ttf", 200) # définition de la police (taille et nom de la police)
	image_texte = police.render("Game over", True, (255,0,0))
	screen.blit(image_texte, (70,200))
	pygame.display.flip() # mis à jour de la fenetre 
	for event in pygame.event.get(): # event est une fonction pygame qui prend en compte ce qui se passe
		if event.type == pygame.QUIT:
				running = False # le jeu ne tourne plus
				pygame.quit() # quitter pygame