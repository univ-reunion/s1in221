# Ce jeu est un jeu de parcours/plateforme dont le but est de franchir
# les obstacles et les monstres pour atteindre la porte menant au niveau suivant.
# Difficulté moyenne
# Niveaux modifiable a volonté via le script
# Durée : de 40 sec a 1 heure selon le talent du joueur


import pygame # importation de pygame
from pygame.locals import *  # importation de tous les fonctions pygame
import pickle # importation module pickle (transforme données python en binaire)
from os import path # importation de la fonction path du module os (manipulation de chemins de fichiers)
from Porte import Porte
from Pic import Pic
from Bouton import Bouton
from Ennemy import Enemy


pygame.init() # initialisation de pygame

clock = pygame.time.Clock()
fps = 60

# Variable de la taille de la fenêtre
screen_width = 1050 # valeur longueur de la fenêtre
screen_height = 650 # valeur largeur de la fenêtre

# fenêtre de jeu
screen = pygame.display.set_mode((screen_width, screen_height)) # génerer la fenêtre
pygame.display.set_caption('Plateforme') # nom fenêtre

# Variables du jeu
longueur_cube = 35 # longueur division de la fenêtre (pour création plateforme)
largeur_cube = 25 # largeur division de la fenêtre (pour création plateforme)
game_over = 0 # variable jeu perdu
menu_principal = True # variable page d'accueil du jeu
niveau = 0 # variable niveau (d'abord à 0)
niveau_max = 2 # nombre de niveaux

# Chargement d'images
fond = pygame.image.load("fond.jpg") # fond du jeu
restart_img = start_img = pygame.image.load("bouton.png") # boutton "jouer"
fin = pygame.image.load("fin.png")

# fonction rénitialisation du niveau
def renitialisation_niv(niveau):
	player.reset(100, screen_height - 130) # rénitialiser de la position du personnage
	monstre_group.empty() # vider monstre_group
	pic_group.empty() # vider pic_group
	exit_group.empty() # vider port_group
	# charger les données du niveau et sa plateforme
	if path.exists(f'niveau{niveau}_data'): # si dans le répertoire il existe un fichier de type ...
		pickle_in = open(f'niveau{niveau}_data', 'rb') # mettre dans la variable pickle_in la version binaire du fichier
		plateforme_data = pickle.load(pickle_in) # charger le fichier en code binaire et insertion dans la variable plateforme_data
	plateforme = Plateforme(plateforme_data) # insérer dans la variable plateforme l'application de la class Plateforme sur plateforme_data
	return plateforme # la fonction renitialisation_niv retourne plateforme



class Hero(): # class pour le personnage

	def __init__(self, x, y):
		self.reset(x,y) # permettra la renitialisation de la position du personnage

	def update(self, game_over):
		dx = 0
		dy = 0
		marche = 5
		if game_over == False: # si la partie n'est pas perdu
			key = pygame.key.get_pressed() # prend ce que le joueur clique
			# déplacement du héro
			if key[pygame.K_UP] and self.jumped == False and self.in_air == False:
				self.vel_y = -15
				self.jumped = True
			if key[pygame.K_UP] == False:
				self.jumped = False
			if key[pygame.K_LEFT]:
				dx -= 5
				self.counter += 1
				self.direction = -1
			if key[pygame.K_RIGHT]:
				dx += 5
				self.counter += 1
				self.direction = 1
			if key[pygame.K_LEFT] == False and key[pygame.K_RIGHT] == False:
				self.counter = 0
				self.index = 0
				if self.direction == 1:
					self.image = self.images_right[self.index]
				if self.direction == -1:
					self.image = self.images_left[self.index]
			#animation
			if self.counter > marche:
				self.counter = 0
				self.index += 1
				if self.index >= len(self.images_right):
					self.index = 0
				if self.direction == 1:
					self.image = self.images_right[self.index]
				if self.direction == -1:
					self.image = self.images_left[self.index]
			#gravité
			self.vel_y += 1
			if self.vel_y > 10:
				self.vel_y = 10
			dy += self.vel_y
			#collision
			self.in_air = True
			for cube in plateforme.cube_list:
				# collision sur l'axe x
				if cube[1].colliderect(self.rect.x + dx, self.rect.y, self.width, self.height):
					dx = 0
				# colision sur l'axe y
				if cube[1].colliderect(self.rect.x, self.rect.y + dy, self.width, self.height):
					if self.vel_y < 0:
						dy = cube[1].bottom - self.rect.top
						self.vel_y = 0
					elif self.vel_y >= 0:
						dy = cube[1].top - self.rect.bottom
						self.vel_y = 0
						self.in_air = False
			#condition des ennemis
			if pygame.sprite.spritecollide(self, monstre_group, False):
				game_over = -1

			if pygame.sprite.spritecollide(self, pic_group, False):
				game_over = -1

			if pygame.sprite.spritecollide(self, exit_group, False):
				game_over = 1
			self.rect.x += dx
			self.rect.y += dy
		# Si la partie est perdu
		elif game_over == True:
			self.image = self.dead_image
			if self.rect.y > 200:
				self.rect.y -= 5
		screen.blit(self.image, self.rect)
		return game_over

	def  reset(self, x, y):
		self.images_right = []
		self.images_left = []
		self.index = 0
		self.counter = 0
		for num in range(1,5):
			img_right = pygame.image.load(f'guy{num}.png')
			img_right = pygame.transform.scale(img_right, (40, 80))
			img_left = pygame.transform.flip(img_right, True, False)
			self.images_right.append(img_right)
			self.images_left.append(img_left)
		self.dead_image = pygame.image.load("ghost.png")
		self.image = self.images_right[self.index]
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.width = self.image.get_width()
		self.height = self.image.get_height()
		self.vel_y = 0
		self.jumped = False
		self.direction = 0
		self.in_air = True


class Plateforme(): # classe pour la plateforme
	def __init__(self, data):
		self.cube_list = [] # plateforme sous forme de liste
		terre_img = pygame.image.load('terre.png') # charger l'image d'un cube de terre
		grass_img = pygame.image.load('herbe.png') # charger l'image d'un cube de surface
		# grillage de la plateforme (division en cube) et on le parcours grâce à une boucle for
		ligne_count = 0
		for ligne in data:
			colonne_count = 0
			for cube in ligne:
				if cube == 1:
					img = pygame.transform.scale(terre_img, (longueur_cube, largeur_cube)) # rempli le cube avec terre_img
					img_rect = img.get_rect() # prend la position
					img_rect.x = colonne_count * longueur_cube # position du cube sur l'horizontale
					img_rect.y = ligne_count * largeur_cube # position du cube sur la verticale
					cube = (img, img_rect) # le cube prend la forme img en position img_rect
					self.cube_list.append(cube) # ajoute à cube_list
				if cube == 2:
					img = pygame.transform.scale(grass_img, (longueur_cube, largeur_cube))
					img_rect = img.get_rect()
					img_rect.x = colonne_count * longueur_cube
					img_rect.y = ligne_count * largeur_cube
					cube = (img, img_rect)
					self.cube_list.append(cube)
				if cube == 3:
					monstre = Enemy(colonne_count * longueur_cube, ligne_count * largeur_cube - 8)
					monstre_group.add(monstre)
				if cube == 4:
					pic = Pic(colonne_count * longueur_cube, ligne_count * largeur_cube + 14)
					pic_group.add(pic)
				if cube == 5:
					exit = Porte(colonne_count * longueur_cube, ligne_count * largeur_cube - 25)
					exit_group.add(exit)


				colonne_count += 1 # on passe à la colonne suivante
			ligne_count += 1 # on passe à la ligne suivante

	def draw(self):
		for cube in self.cube_list:
			screen.blit(cube[0], cube[1])




# position initiale de l'héros
player = Hero(100, screen_height - 130)

monstre_group = pygame.sprite.Group() # définition d'une classe pour pouvoir afficher tout
pic_group = pygame.sprite.Group()
exit_group = pygame.sprite.Group()

# charger les données du niveau et créer la map
if path.exists(f'niveau{niveau}_data'):
	pickle_in = open(f'niveau{niveau}_data', 'rb')
	plateforme_data = pickle.load(pickle_in)
plateforme = Plateforme(plateforme_data)

restart_button = Bouton(screen_width // 2.6, screen_height // 2.5, restart_img) # afficher et faire fonctionner le bouton restart
start_button = Bouton(screen_width // 2.6 , screen_height // 2.5, start_img) # afficher et faire fonctionner le bouton start
fin_button = Bouton(screen_width // 3, screen_height // 3, fin)

running = True # variable pour faire tourné le jeu
while running: # boucle principale : tant que le jeu tourne
	clock.tick(fps)

	screen.blit(fond,(0,0)) # appliquer "fond" comme image de fond

	if menu_principal == True: # Si on est dans le menu principal
		if start_button.draw(): # Si le bouton start n'est pas afficher
			menu_principal = False # On n'est pas dans le menu principal

	else: 
		plateforme.draw() # Si c'est la plateforme qui est affiché

		if game_over == 0: # Si la partie n'est pas perdu
			monstre_group.update() # 

		monstre_group.draw(screen) # afficher les monstres
		pic_group.draw(screen) # afficher les pics
		exit_group.draw(screen) # afficher la porte

		game_over = player.update(game_over) 

		# si le joueur meurt
		if game_over == -1: 
			if restart_button.draw(): # si le bouton restart s'affiche
				plateforme_data = [] 
				plateforme = renitialisation_niv(niveau)
				game_over = 0 # partie relancée

		# si le joueur a passé le niveau
		if game_over == 1: 
			niveau += 1 # passé au niveau suivant
			if niveau <= niveau_max: # si le niveau n'est pas encore au dernier niveau
				plateforme_data = []
				plateforme = renitialisation_niv(niveau)
				game_over = 0 # partie relancée
			if niveau > niveau_max :
				if fin_button.draw() :

					niveau = 0
					plateforme_data = []
					plateforme = renitialisation_niv(niveau)
					game_over = 1

			else:
				if restart_button.draw():
					niveau = 0
					plateforme_data = []
					plateforme = renitialisation_niv(niveau) # renitialisation du niveau
					game_over = 0 # partie relancée



	for event in pygame.event.get(): # boucle qui permet de savoir ce que le joueur fait
		if event.type == pygame.QUIT: # Si on quitte pygame
			running = False # le jeu ne tourne plus
	
	pygame.display.update() # Mise à jour de la fenêtre			

pygame.quit() # quitter pygame


