# Script de création de la porte de sortie de chaque niveau .

import pygame

longueur_cube = 35  # longueur division de la fenêtre (pour création plateforme)
largeur_cube = 25  # largeur division de la fenêtre (pour création plateforme)
class Porte(pygame.sprite.Sprite):
	def __init__(self,x, y):
		pygame.sprite.Sprite.__init__(self)
		img = pygame.image.load('porte.png')
		self.image = pygame.transform.scale(img, (longueur_cube, largeur_cube * 2))
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y