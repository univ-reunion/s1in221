# Script qui créer les monstres du jeux et leurs déplacements .

import pygame


class Enemy(pygame.sprite.Sprite):
	def __init__(self,x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load('monstre.png')
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.move_direction = 1
		self.move_counter = 0

	def update(self):
		self.rect.x += self.move_direction
		self.move_counter += 1
		direction_monstre = self.move_direction
		if abs(self.move_counter) > 50:
			self.move_direction *= -1
			self.move_counter *= -1
		if direction_monstre < 0 :
			self.image = pygame.image.load('monstre.png')
		if direction_monstre > 0 :
			self.image = pygame.image.load('monstre2.png')