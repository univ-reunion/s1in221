# Script pour l'apparition des différent boutton du jeu.

import pygame

screen_width = 1050 # valeur longueur de la fenêtre
screen_height = 650 # valeur largeur de la fenêtre

screen = pygame.display.set_mode((screen_width, screen_height)) # génerer la fenêtre
class Bouton():  # classe pour boutton

    def __init__(self, x, y, image):
        self.image = image  # image du bouton
        self.rect = self.image.get_rect()  # on prend la position du boutton
        self.rect.x = x  # position du bouton sur l'horizontale
        self.rect.y = y  # position du bouton sur la verticale
        self.clicked = False  # on ne clique pas encore dessus

    def draw(self):  # afficher le bouton et le faire marcher
        action = False  # variable action
        pos = pygame.mouse.get_pos()  # prend la position de la souris
        if self.rect.collidepoint(pos):  # si la position du bouton coincide avec celle de la souris
            if pygame.mouse.get_pressed()[0] == 1:  # and self.clicked == False: # si la souris a cliqué et que
                action = True
                self.clicked = True
            else:
                if pygame.mouse.get_pressed()[0] == 0:  # si la souris n'a pas cliqué
                    self.clicked = False  # le bouton n'a pas été cliqué
        screen.blit(self.image, self.rect)  # affiche le boutton
        return action