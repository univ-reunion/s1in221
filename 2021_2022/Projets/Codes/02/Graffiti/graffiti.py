

def lecture():
    longueur_palissade = int(input())    
    nb_intervalles = int(input())
    intervalles = [tuple(int(j) for j in input().split()) for _ in range(nb_intervalles)]
    return longueur_palissade, intervalles


def un_troncon(unpainted, intervalle):
    d, f = intervalle
    i = 0
    while unpainted[i][1] < d:
        i += 1
    dd, df = unpainted.pop(i)
    if dd < d:
        unpainted.insert(i, (dd, d))
        i += 1
    if f < df:
        unpainted.insert(i, (f,df))


def tronconne(unpainted, intervalles):
    for intervalle in intervalles:
        un_troncon(unpainted, intervalle)

def disjoints(intervalles):
    d,f = intervalles.pop()
    inter_disjoints = [(d, f)]
    while intervalles:
        d, f = intervalles.pop()
        dd, df = inter_disjoints.pop()
        if dd <= d < df:
            if f < df:
                inter_disjoints.append((dd, df))
            else:
                inter_disjoints.append((dd, f))
        else:
            inter_disjoints.append((dd, df))
            inter_disjoints.append((d, f))
    return inter_disjoints


def reponse(unpainted):
    if unpainted:
        for d, f in unpainted:
            print(d,f)
    else:
        print('All painted')


longueur_palissade, intervalles = lecture()
unpainted = [(0, longueur_palissade)]
intervalles.sort(reverse=True)
intervalles = disjoints(intervalles)
tronconne(unpainted, intervalles)
reponse(unpainted)
