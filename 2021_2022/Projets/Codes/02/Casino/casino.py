import math

def parite(data):
    if data == 'ODD':
        return 1
    elif data == 'EVEN':
        return 0
    else:
        return data%2

class Player:

    def __init__(self, casino):
        self.casino = casino
        self.cash = 0
        self.bet = None
        self.pari = None

    def round(self):
        mise = math.ceil(self.cash / 4)
        if self.bet in ('ODD', 'EVEN'):
            if self.casino.tirage and parite(self.casino.tirage) == parite(self.bet):
                self.cash += mise
            else:
                self.cash -= mise
        elif self.bet == 'PLAIN':
            if self.casino.tirage == self.pari:
                self.cash += 35 * mise
            else:
                self.cash -= mise 



class Casino:

    def __init__(self):
        self.player = Player(self)
        self.rounds = 0
        self.tirage = 0


    def setup(self):
        self.rounds = int(input())
        self.player.cash = int(input())


    def play(self):
        for _ in range(self.rounds):
            tirage, self.player.bet, *pari = input().split()
            self.tirage = int(tirage)
            if pari:
                self.player.pari = int(pari[0]) 
            self.player.round()

    def stop(self):
        print(self.player.cash)


jeu = Casino()
jeu.setup()
jeu.play()
jeu.stop()