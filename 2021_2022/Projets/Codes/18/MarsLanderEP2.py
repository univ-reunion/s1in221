import sys
import math


def distance_tup(pos1, pos2):
    return math.sqrt((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2)


def distance_pt(c1, c2):
    return c1 - c2


def check_direction_speed(h_speed):
    if h_speed < 0:
        return -1
    else:
        return 1


def check_target_direction(x, tar_x):
    if x > tar_x:
        return 1
    else:
        return -1


prev_x, prev_y, land_x, land_y = 0, 0, 1, 1

surface_n = int(input())  
for i in range(surface_n):
    prev_x, prev_y = land_x, land_y
    land_x, land_y = [int(j) for j in input().split()]
    if land_y == prev_y:
        landing_x = (prev_x, land_x)
        landing_y = land_y


target = ((landing_x[0] + landing_x[1]) // 2, landing_y)  
print('Target acquired - {}'.format(target), file=sys.stderr)


g = 3.711  
pi = math.pi
start_dist_x = -1
start_h_speed = -1
start_v_speed = -1
starting_calc = True  
counter_start_motion = False  
landing = False  
movement = False  

while True:

    x, y, h_speed, v_speed, fuel, rotate, acc = [int(i) for i in input().split()]

    dist_x = distance_pt(x, target[0])
    acc_h = acc * math.sin(rotate * (pi / 180))
    acc_v = (acc * math.cos(rotate * (pi / 180))) - g 
    
    if starting_calc:
        
        start_dist_x = abs(distance_pt(x, target[0]))
        start_h_speed = h_speed
        start_v_speed = v_speed
        
        tar_acc_h = (start_h_speed ** 2) / (2 * start_dist_x)  
        try:
            tar_time = (2 * start_dist_x) // (start_h_speed)
        except:
            pass
        if abs(tar_acc_h) < 2 and start_h_speed != 0:
            print(0, 4)
            continue
        starting_calc = False  
        if start_h_speed != 0:
            counter_start_motion = True
        else:
            movement = True
            turn = (x + target[0]) // 2
    
    if counter_start_motion is True:
        thrust = 4  
        direction = check_direction_speed(start_h_speed)  
        rotate_to = direction * (int(math.asin(tar_acc_h / thrust) * 180 / pi) + 2)
        if v_speed > 0:
            thrust = 2
        if abs(h_speed) < 2:
            rotate_to = 0
            counter_start_motion = False  

            if landing_x[0] < x < landing_x[1]:  
                landing = True
            else:  
                movement = True
                turn = (x + target[0]) // 2
    
    if movement is True:
        thrust = 4
        if x < turn:
            rotate_to = -30
        elif x >= turn:
            rotate_to = 30

        if landing_x[0] < x < landing_x[1] and abs(h_speed) < 2:
            movement = False
            landing = True
            mov_dir = -1
            rotate_to = 0

    
    if landing is True:
        if abs(v_speed) < 36:
            thrust = 2
        else:
            thrust = 4

    
    print('Current Target is {}'.format(target), file=sys.stderr)
    print('Starting distance to land is', start_dist_x, file=sys.stderr)
    print('Horizontal acceleration is {}'.format(acc_h), file=sys.stderr)
    print('Vertical acceleration is {}'.format(acc_v), file=sys.stderr)
    print('Distance to flat surface is ', dist_x, file=sys.stderr)

    print(rotate_to, thrust)