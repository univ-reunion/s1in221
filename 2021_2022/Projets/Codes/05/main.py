import pygame
import time
from pygame.locals import *
from pygame import mixer
pygame.init()
pygame.mixer.init()

pygame.display.set_caption("Kirumi")
ecran = pygame.display.set_mode((1200, 600))
rectEcran = ecran.get_rect()

pygame.mouse.set_cursor(*pygame.cursors.diamond)

pos = pygame.mouse.get_pos()

#musique d'acceuil
pygame.mixer.music.load('Musique/son1.ogg')
pygame.mixer.music.play(2)


#mise en place du boutton play
boutton_play = pygame.image.load('Images/boutton_play.png')
boutton_play =pygame.transform.scale(boutton_play, (360, 180))
boutton_play_rect = boutton_play.get_rect()
boutton_play_rect.x = ecran.get_width() / 3
boutton_play_rect.y = ecran.get_width() / 10

#mise en place du boutton quitter
boutton_quitter = pygame.image.load('Images/boutton23.png')
boutton_quitter = pygame.transform.scale(boutton_quitter, (300, 150))
boutton_quitter_rect = boutton_quitter.get_rect()
boutton_quitter_rect.x = ecran.get_width() / 2
boutton_quitter_rect.y = ecran.get_width() / 4

fond = pygame.image.load('Fond/fond3.png')

class Message:

    def __init__(self, police, texte, couleur, couleur2):
        self.police = pygame.font.Font(police,72)
        self.text = self.police.render(texte,True,pygame.Color(couleur))
        self.recttexte = self.text.get_rect()
        self.couleur = couleur2

class Personnage(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.nom="Kirumi"
        self.health = 100
        self.max_health = 100
        self.attaque = 5
        self.vitesse = 2
        self.all_shoot = pygame.sprite.Group()
        self.image = pygame.image.load('Personnages/bogoss.png')
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 275
        self.presser = {}

    #tir du perso
    def launch_shoot(self):
        self.all_shoot.add(Shoot(self))

    #Deplacement du perso
    def deplacement_droite(self):
        self.rect.x += self.vitesse

    def deplacement_gauche(self):
        self.rect.x -= self.vitesse

    #barre de vie du perso
    def barre_vie_perso(self, surface):
        position_vie = [self.rect.x + 35, self.rect.y - 5, self.health, 15]
        back_vie = [self.rect.x + 35, self.rect.y - 5, self.max_health, 15]
        back_color = (60, 63, 60)
        color = (111, 210, 46)
        pygame.draw.rect(surface, back_color, back_vie)
        pygame.draw.rect(surface, color, position_vie)


personnage = Personnage()

class Shoot(pygame.sprite.Sprite):

    def __init__(self,personnage):
        super().__init__()
        self.velocity = 10
        self.image = pygame.image.load('Images/feu.png') #on charge l'image de la boule de feu
        self.image = pygame.transform.scale(self.image, (100,100))
        self.rect = self.image.get_rect()
        self.rect.x = personnage.rect.x +140
        self.rect.y = personnage.rect.y +80
        self.personnage = personnage

    #suppression de mini kamehameha   
    def remove(self):
        self.personnage.all_shoot.remove(self)

    #mise en mouvement du mini kamehameha
    def move(self):
        self.rect.x += self.velocity #vitesse de deplacement des mini kameha
        if self.rect.x > 1200:
            self.remove()

class Monstre:

    def __init__(self, nom, vie, attaque, image, back_image, velocity, maxdroit, x, y, musical, multi):
        self.nom = nom
        self.vie = vie
        self.sante = vie
        self.attaque = attaque
        self.image = pygame.image.load(image)
        self.back_image = pygame.image.load(back_image)
        self.rect = self.image.get_rect()
        self.velocity = velocity
        self.maxdroit = maxdroit
        self.rect.x = x
        self.rect.y = y
        self.music = musical
        self.multi = multi


    # Barre de vie du mob
    def barre_vie(self, surface):
        couleur = (197, 0, 0)
        back_couleur = (0, 0, 0)
        position = [0, 20, self.sante * self.multi, 20]
        back_position = [0, 20, self.vie * self.multi, 20]
        pygame.draw.rect(surface, back_couleur, back_position)
        pygame.draw.rect(surface, couleur, position)
    
    #mouvement du mob  
    def deplacement_gauche(self):
        self.rect.x -= self.velocity

    def deplacement_droite(self):
        self.rect.x += self.velocity

mob_1 = Monstre('Gobelin', 100, 5, "Personnages/troll1.png", "Fond/donjon.jpg", 3, 975, 720, 300, "Musique/lvl1.mp3", 12)
mob_2 = Monstre('Bandit Dragonoid', 100, 10, "Personnages/troll2.png", "Fond/moutain.jpg", 4, 1015, 760, 265, "Musique/lvl2.mp3", 12)
mob_3 = Monstre('Chevalier Volvidon', 100, 15, "Personnages/troll3.png", "Fond/lave.png", 5, 960, 760, 305, "Musique/lvl3.mp3", 12)
mob_4 = Monstre('Baggi', 100, 5, "Personnages/troll4.png", "Fond/night.jpg", 6, 960, 760, 259, "Musique/lvl4.mp3", 12)
boss = Monstre("L'étripeur", 200, 30, "Personnages/boss.png", "Fond/lastlvl.png", 3, 730, 660, 68, "Musique/lvl5.mp3", 6)

list_monstre = [mob_1, mob_2, mob_3, mob_4, boss]

gameover = Message("Police/johnnytorch.ttf", "Game Over", "#687c74", "#8B0000")
gameover.recttexte.center = ecran.get_rect().center
victory = Message("Police/victory.ttf", "Victory", "#687c74", "#8B0000")
victory.recttexte.center = ecran.get_rect().center
level1 = Message("Police/zorque.ttf", "Level 1", "#6e8390", "#474d44")
level1.recttexte.center = ecran.get_rect().center
level2 = Message("Police/zorque.ttf", "Level 2", "#a498a4", "#abd7cf")
level2.recttexte.center = ecran.get_rect().center
level3 = Message("Police/zorque.ttf", "Level 3", "#d47731", "#702b12")
level3.recttexte.center = ecran.get_rect().center
level4 = Message("Police/zorque.ttf", "Level 4", "#c193c9", "#83638d")
level4.recttexte.center = ecran.get_rect().center
lastlevel = Message("Police/guevara_.ttf", "Last Level", "#362212", "#7c3617")
lastlevel.recttexte.center = ecran.get_rect().center

list_message = [level1, level2, level3, level4, lastlevel]

jeu_en_cours = True
Position = True
Accueil = True
x = -1

while jeu_en_cours:
    #chargement de l'ecran d'acceuil
    while Accueil:
        ecran.blit(fond, (0, 0))
        ecran.blit(boutton_play, boutton_play_rect)
        ecran.blit(boutton_quitter, boutton_quitter_rect)
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                jeu_en_cours = False
                pygame.quit()
            #interactioin des boutons avec la souris 
            if event.type == pygame.MOUSEBUTTONDOWN:
                if boutton_play_rect.collidepoint(event.pos):
                    Accueil = False
                    pygame.mixer.music.stop()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if boutton_quitter_rect.collidepoint(event.pos):
                    jeu_en_cours = False
                    pygame.quit()
    # chargement des differents music et messages d'annonces pour chaque level
    for monstre in list_monstre:
        mixer.music.load(monstre.music)
        x = x + 1
        if monstre.nom == "L'étripeur":
            personnage.health = 100
            personnage.max_health+=50
            personnage.health+=50
        if not x > 4:
            ecran.fill(pygame.Color(list_message[x].couleur))
            ecran.blit(list_message[x].text, list_message[x].recttexte)
            pygame.display.flip()
            time.sleep(1)
        if not monstre.sante == 0:
            mixer.music.play(-1)
        #action quand les pv du monstre ne sont pas égales à 0
        while monstre.sante != 0:
            #chargement des differents images(monstres, perso principale, avec leurs barres de vie)
            ecran.blit(monstre.back_image, (0, 0))
            ecran.blit(monstre.image, monstre.rect)
            if monstre.nom == "Baggi":
                personnage.rect.y = 315
            if monstre.nom == "L'étripeur":
                personnage.rect.y = 275
            ecran.blit(personnage.image, personnage.rect)
            monstre.barre_vie(ecran)
            personnage.barre_vie_perso(ecran)
            while personnage.health <= 0:
                ecran.fill(pygame.Color(gameover.couleur))
                ecran.blit(gameover.text, gameover.recttexte)
                pygame.display.flip()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        jeu_en_cours = False
                        pygame.quit()
            #systeme d'apparition et disparition des mini kamehameha et les degats qu'ils infligent 
            for shoot in personnage.all_shoot:
                shoot.move()
                if shoot.rect.x > monstre.rect.x:
                    shoot.remove()
                    monstre.sante -= personnage.attaque
                if monstre.sante == 0:
                    shoot.remove()
            personnage.all_shoot.draw(ecran)  
            #systeme de deplacement du mob
            if Position:
                monstre.deplacement_gauche()
            else:
                monstre.deplacement_droite()
                if monstre.rect.x > monstre.maxdroit:
                    Position = True
                if monstre.rect.x <= 0:
                     Position = False
            if monstre.rect.x <= personnage.rect.x:
                    Position = False
            # systeme de degats sur le perso
            if personnage.rect.x <= 0:
                personnage.rect.x = False
            if personnage.rect.x == monstre.rect.x or personnage.rect.x == monstre.rect.x+1 or personnage.rect.x == monstre.rect.x+2 or personnage.rect.x == monstre.rect.x+3 or personnage.rect.x == monstre.rect.x+4 or personnage.rect.x == monstre.rect.x+5 or personnage.rect.x == monstre.rect.x+6:
                personnage.health -= monstre.attaque
            #systeme de deplacement du perso principale 
            if personnage.presser.get(pygame.K_d):
                if not personnage.rect.x > monstre.rect.x:
                    personnage.deplacement_droite()
            elif personnage.presser.get(pygame.K_q):
                personnage.deplacement_gauche()
            pygame.display.flip()
            for event in pygame.event.get():
                #systeme de click ou la souris doit etre sur le feu et ou le perso lance le mini kamehameha
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if monstre.rect.collidepoint(event.pos):
                        personnage.launch_shoot()  
                if event.type == pygame.QUIT:
                    jeu_en_cours = False
                    pygame.quit()
                #on verifie si une touche du clavier est appuyé ou pas
                elif event.type == pygame.KEYDOWN:
                    personnage.presser[event.key] = True
                elif event.type == pygame.KEYUP:
                    personnage.presser[event.key] = False
    #la musique s'arrete
    mixer.music.stop()
    #chargement de la fenetre victoire
    ecran.fill(pygame.Color(victory.couleur))
    ecran.blit(victory.text, victory.recttexte)
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jeu_en_cours = False
            pygame.quit()
