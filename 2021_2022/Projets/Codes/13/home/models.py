from django.db import models

class menu(models.Model):
	titre_menu = models.CharField(max_length=20, default='')
	liens = models.CharField(max_length=20, default='')

	def __str__(self):
		return self.titre_menu

class affiche(models.Model):
	titre_affiche = models.CharField(max_length=20, default='Titre')
	plateforme = models.CharField(max_length=20, default='PC/Xbox/Ps4')
	description = models.TextField(default='Mettre une description')
	prix = models.CharField(max_length=10, default=' €')
	année = models.CharField(max_length=10, default='')
	template = models.CharField(max_length=40, default='Choisir la ou va apparaitre l\'affiche')
	categorie1 = models.CharField(max_length=20, default='obligatoire')
	categorie2 = models.CharField(max_length=20, default='none')
	categorie3 = models.CharField(max_length=20, default='none')
	categorie4 = models.CharField(max_length=20, default='none')
	categorie5 = models.CharField(max_length=20, default='none')
	categorie6 = models.CharField(max_length=20, default='none')
	lien_image = models.CharField(max_length=10000, default='')

	def __str__(self):
		return self.titre_affiche

class list_genre(models.Model):
	titre_genre = models.CharField(max_length=20, default='')

	def __str__(self):
		return self.titre_genre

class plateforms(models.Model):
	titre_plateform = models.CharField(max_length=20, default='')

	def __str__(self):
		return self.titre_plateform