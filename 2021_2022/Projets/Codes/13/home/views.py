import random
from django.shortcuts import render
from home.models import menu, affiche, list_genre, plateforms

global titres, genres, affiches

titres = menu.objects.all()
genres = list_genre.objects.order_by('titre_genre')
affiches = list(affiche.objects.all())

random.shuffle(affiches)


def index(request):
	data = {'titres':titres, 'affiches':affiches, 'genres':genres}
	return render(request, 'home/index.html', data)


def affiche_detail(request, titre_Affiche):
	affiche_rechercher = affiche.objects.get(titre_affiche=titre_Affiche)
	data = {'affiche_rechercher':affiche_rechercher, 'titres':titres, 'genres':genres}
	return render(request, 'affiche/affiche.html', data)

def plateform_detail(request, nom_plateform):
	plateform_rechercher = plateforms.objects.get(titre_plateform=nom_plateform)
	data = {'titres':titres, 'affiches':affiches, 'genres':genres, 'plateform_rechercher':plateform_rechercher}
	return render(request, 'plateform/plateform.html', data)

def search(request):
	query=request.GET["Jeux"]
	jeux_rechercher = affiche.objects.filter(titre_affiche__contains=query)
	data = {'titres':titres, 'affiches':affiches, 'genres':genres, 'query':query, 'jeux_rechercher':jeux_rechercher}
	return render(request, 'affiche/jeux_rechercher.html', data)