from django.contrib import admin
from .models import menu, affiche, list_genre, plateforms

admin.site.register(menu)
admin.site.register(affiche)
admin.site.register(list_genre)
admin.site.register(plateforms)