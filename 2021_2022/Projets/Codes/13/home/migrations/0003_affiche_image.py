# Generated by Django 3.2 on 2021-04-12 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_affiche'),
    ]

    operations = [
        migrations.AddField(
            model_name='affiche',
            name='image',
            field=models.ImageField(default='', upload_to='affiche/'),
        ),
    ]
