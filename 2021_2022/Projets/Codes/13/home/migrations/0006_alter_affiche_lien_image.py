# Generated by Django 3.2 on 2021-04-16 05:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_auto_20210415_1755'),
    ]

    operations = [
        migrations.AlterField(
            model_name='affiche',
            name='lien_image',
            field=models.CharField(default='', max_length=10000),
        ),
    ]
