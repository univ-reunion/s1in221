from django.urls import path
from .views import index, affiche_detail, plateform_detail, search

urlpatterns = [
	path('', index, name='index'),
	path('jeux_name/<str:titre_Affiche>/', affiche_detail, name='affiche_detail'),
	path('plateform/<str:nom_plateform>/', plateform_detail, name='plateform_detail'),
	path('rechercher/', search, name='search')
]