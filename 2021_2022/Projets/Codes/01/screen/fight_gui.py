"""
Name : Fight GUI

Description : This is the fight GUI generated from the information of the fight received from the server and the

starring : K/BIDI Thomas
"""

#Libraries import
import pygame
import pygame_gui
import threading
import time
from saves.constants import *

class Fight_Client():
    def __init__(self, fight_name, name, op_name, attack_dict, max_life_p, max_life_o, client):
        #Definition of the attributes of the class
        self.fight_name = fight_name
        self.writing = False
        self.text = ""
        self.name = name
        self.opponent = op_name
        self.max_lp = int(max_life_p)
        self.max_lo = int(max_life_o)
        self.end = False

        #Store the client as an attribute
        self.client = client
        self.settings(attack_dict)
        self.run_loop()

    #Settings function who generate all the elements of the interface
    def settings(self, attack_dict):
        pygame.font.init()
        myfont = pygame.font.SysFont('Arial', 30)
        #Setup of the image
        self.background = pygame.image.load(BACKGROUND_PATH)
        self.coq = pygame.image.load(COQ_PATH)
        self.coq_reverse = pygame.image.load(REVERSE_COQ_PATH)

        #Setup the window
        pygame.display.set_caption(WINDOW_NAME)
        #Player_info
        self.player_name = myfont.render(self.name, False, (0, 0, 0))
        self.opponent_name = myfont.render(self.opponent, False, (0, 0, 0))

        #Setup of the attack_list
        self.attack_list = list(attack_dict.keys())

        #Store the name of the attacks in a dictionnary with the attacks ids as keys
        self.attack_name = {"#attack0" : self.attack_list[0], "#attack1" : self.attack_list[1],
        "#attack2" : self.attack_list[2], "#attack3" : self.attack_list[3]}

        #Made the attack of the player a property of this class
        self.attack_dict = attack_dict

        #Generate the screen, clock, run variable and manager
        self.screen = pygame.display.set_mode((1280, 720))
        self.run = True
        self.manager = pygame_gui.UIManager((1280, 720))
        self.clock = pygame.time.Clock()

        #Button's informations
        button_size = (300, 150)
        button_pos = [(0,420), (300,420), (0,570), (300,570)]

        #For loop to generate the button in the manager
        for attack_ind in range(len(self.attack_list)):
            pygame_gui.elements.UIButton(relative_rect=pygame.Rect(button_pos[attack_ind], button_size),
                                         text=self.attack_list[attack_ind],
                                         manager=self.manager,
                                         object_id=f'#attack{attack_ind}')
        #Quit button
        quit = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((1205,670), (75,50)),
                                     text="Quit",
                                     manager=self.manager,
                                     object_id=f'#quit')

        #Text box to display information about the fight
        text_box = pygame_gui.elements.ui_text_box.UITextBox(relative_rect= pygame.Rect((600,420), (680,150)), manager=self.manager, html_text="Début du combat.")
        text_box.set_active_effect("typing_appear")

        #Size of the green life bar
        self.player_life = 400
        self.opponent_life = 400

        #Position of the bar for the player and opponent
        self.player_life_set = {"player" : (870, 300), "opponent" : (50, 50)}

    #Same algorith as the client_test and return the string generate
    def a2c(self, id):
        att_tup = ""

        #For loop to add the element from the attack tuple to the string "att_tup"
        for i in self.attack_dict[self.attack_name[id]]:
            att_tup += str(i)+"/"
        att_tup = att_tup.rstrip("/")

        return att_tup

    def life_bar_updater(self):
        #Draw opponent life and name
            #Red background for life
        back_life = pygame.Rect(self.player_life_set["opponent"][0], self.player_life_set["opponent"][1], 400, 20)
        pygame.draw.rect(self.screen, (255, 0, 0), back_life)
            #Green bar over the red one
        op_life = pygame.Rect(self.player_life_set["opponent"][0], self.player_life_set["opponent"][1], self.opponent_life, 20)
        pygame.draw.rect(self.screen, (0, 255, 0), op_life)
            #Name of the opponent
        self.screen.blit(self.opponent_name, (self.player_life_set["opponent"][0], self.player_life_set["opponent"][1]-30))

        #Draw player life and name
            #Red background for life
        back_life = pygame.Rect(self.player_life_set["player"][0], self.player_life_set["player"][1], 400, 20)
        pygame.draw.rect(self.screen, (255, 0, 0), back_life)
            #Green bar over the red one
        player_life = pygame.Rect(self.player_life_set["player"][0], self.player_life_set["player"][1], self.player_life, 20)
        pygame.draw.rect(self.screen, (0, 255, 0), player_life)
            #Name of the player
        self.screen.blit(self.player_name, (self.player_life_set["player"][0], self.player_life_set["player"][1]-30))

    #Change the life bars according to ratio between max and current life
    def life_checker(self, life_player, life_opponent):
        player = 400*(life_player/self.max_lp)
        opponent = 400*(life_opponent/self.max_lo)

        #Checking for the player
        if player > 0:
            self.player_life = player
        else:
            self.player_life = 0
            self.text = f"Vous avez perdu le combat."
            self.writing = True
            self.end = True

        #Checking for the opponent
        if opponent > 0:
            self.opponent_life = opponent
        else:
            self.opponent_life = 0
            self.text = f"Vous avez gagner le combat."
            self.writing = True
            self.end = True


    def run_loop(self):
        while self.run:
            #Delta time between two frames
            time_delta = self.clock.tick(60)/1000.0

            #Blit of the images on a the screen
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.coq, (980, 0))
            self.screen.blit(self.coq_reverse, (50, 100))

            for event in pygame.event.get():
                #Close the game
                if event.type == pygame.QUIT:
                    import sys
                    sys.exit()

                #Check if the event is on a pygame_gui element
                if "user_type" in event.dict:
                    #Check if it's a button clicked
                    if event.dict['user_type'] == "ui_button_pressed":
                        #Close the GUI
                        if event.dict["ui_object_id"] == "#quit":
                            self.run = False

                        if "#attack" in event.dict["ui_object_id"]:
                            #Algorithm to generate the server's command from the attack choose

                            #Get the attack id
                            id = event.dict["ui_object_id"]
                            #Generate the command text
                            att_cmd = self.a2c(id)

                            self.writing = True
                            return_info = self.client.send_and_wait(self.fight_name, self.name, f"{self.attack_name[id]}/"+att_cmd)

                            #Show text related to the player attack choice
                            self.text = f"{self.name} attaque {self.opponent} avec {self.attack_name[id]} <br>En attente de l'autre joueur..."
                #listen to the manager process (from pygame_gui)
                self.manager.process_events(event)

            #Update the life bars
            self.life_bar_updater()

            #Check if some text have to be write and write them in the textbox
            if self.writing:
                textbox = pygame_gui.elements.ui_text_box.UITextBox(relative_rect= pygame.Rect((600,420), (680,150)), manager=self.manager, html_text=self.text)
                textbox.set_active_effect("typing_appear")
                self.writing = False

            #Check if information is received in the fight
            if self.client.return_info:
                list = self.client.return_info.split("/")
                self.life_checker(int(list[0]), int(list[1]))

                #Check the fight state
                if not(self.end):
                    self.text = f"{self.opponent} vous a attaque avec {list[2]}"
                    self.writing = True

                self.client.return_info = None

            #update the manager of pygame_gui
            self.manager.update(time_delta)
            #draw on the screen from the manager of pygame_gui
            self.manager.draw_ui(self.screen)
            #update the display (pygame)
            pygame.display.update()
