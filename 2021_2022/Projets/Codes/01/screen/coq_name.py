"""
Name : Choix du Nom

Description : Interface permettant le choix du nom par les joueurs et l'enregistre dans un fichier JSON.

starring: Morel Noah
"""

import pygame, pygame_gui
from pygame.locals import *
from saves.constants import *
import json

class Name:
    def __init__(self):
        #Définition du nom de la fenêtre
        pygame.display.set_caption(WINDOW_NAME)
        #Génération de l'écran pour l'affichage dans une dimension de 1280x720 pixels
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        #Variable run pour la boucle principale de l'interface
        self.run = True
        #Manager de pygame_gui
        self.manager = pygame_gui.UIManager(SCREEN_SIZE)
        #Horloge interne de l'interface pour son affichage
        self.clock = pygame.time.Clock()

        #Chargement des images à afficher sur l'interface
        self.background = pygame.image.load(BACKGROUND_PATH)
        self.titre = pygame.image.load(NAME_SELECT_PATH)

        #Entrer dans le manager de l'input label et stockage dans la variable name afin de pouvoir récupérer son contenu
        name = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((240,500), (800,100)),
                                            manager=self.manager,
                                            object_id=f'#NAME')
        #Bouton quit
        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((1200,10), (75,50)),
                                     text='Quit',
                                     manager=self.manager,
                                     object_id=f'#QUIT')
        #Lancement de la boucle principale
        self.loop()

    def loop(self):
        #Boucle infini tant que self.run est égale à True
        while self.run:
            #Calcul du delta temps entre deux frame
            time_delta = self.clock.tick(60)/1000.0
            #Blit des images sur l'interface aux coordonnées choisies
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.titre, (230, 100))

            #Parcours des events intervenant sur l'interface
            for event in pygame.event.get():
                #Fermeture du jeu si la croix est appuyée
                if event.type == pygame.QUIT:
                    import sys
                    sys.exit()

                #Vérification de l'input comme étant une action sur un élément de pygame_gui
                if "user_type" in event.dict:
                    #Si le bouton quit à était appuyée passe self.run à False afin de retourner sur l'interface précèdente
                    if event.dict['user_type'] == "ui_button_pressed":
                        if event.dict["ui_object_id"] == "#QUIT" :
                            self.run = False

                    #Vérification de l'entrée dans l'input label
                    if event.type == pygame.USEREVENT:
                        if event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
                            #Lorsque le joueur appuie sur entrée, vient charger dans la variable perso_info le fichier JSON et enregistre à la clé "Name" le texte entrée dans
                            with open("saves/personnage.json", "r") as perso:
                                perso_info = json.load(perso)
                                perso_info["Name"] = event.text

                            #Stockage dans le fichier JSON du nouveau nom du personnage
                            with open("saves/personnage.json", "w") as perso:
                                json.dump(perso_info, perso)


                #Processing des events intervenant sur les éléments du manager de pygame_gui
                self.manager.process_events(event)

            #Mise à jour de l'interface
            self.manager.update(time_delta)
            self.manager.draw_ui(self.screen)
            pygame.display.update()
