"""
Name : Connection GUI

Description : This is the connection GUI. The player can enter their username and password and connect to the server to be registered on the waiting queue and launch a fight.

Dependencies :
-json
-Fight_Client
-Name
-Class
-pygame
-pygame_gui

starring : K/BIDI Thomas
"""

#Libraries import
import pygame, pygame_gui
from screen.fight_gui import Fight_Client
import json
from screen.coq_name import Name
from screen.choose_class import Class
from saves.constants import *

class Connection_GUI:
    def __init__(self, client):
        #Store the client as an attributes of the class
        self.client = client
        #Définition du nom de la fenêtre
        pygame.display.set_caption(WINDOW_NAME)
        self.settings()
        #Launch the loop
        self.loop()

    def settings(self):
        #Generate the screen to blit the elements
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        #Variable run pour la boucle principale de l'interface
        self.run = True
        #Manager de pygame_gui
        self.manager = pygame_gui.UIManager(SCREEN_SIZE)
        #Horloge interne de l'interface pour son affichage
        self.clock = pygame.time.Clock()

        #Dictionnary with all the attack inside
        self.attack_dict = ATTACK_INFO

        #Initialisation of the font
        pygame.font.init()
        #Registering of the two font for this GUI
        self.myfont = pygame.font.SysFont('Helvetica', 25)
        self.info_font = pygame.font.SysFont('Helvetica', 40)

        #Information about the position of different elements of the GUI
        self.gen_pos = (560,320)
        self.info_pos = ((0, 0), (0, 40), (0, 80))
        #Set the connected state to false
        self.connected = False

        #Display of all the information of the GUI
            #Username text
        self.display_text = (self.myfont.render("Username : ", False, (0, 0, 0)), self.myfont.render("Password : ", False, (0, 0, 0)))
        self.background = pygame.image.load(BACKGROUND_PATH)

        #Button of the interface
        pygame_gui.elements.UIButton(relative_rect=pygame.Rect(self.gen_pos, (160,80)), text="Connect", manager=self.manager, object_id=f'#connection')
        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((480,640), (160,80)), text="Choose Name", manager=self.manager, object_id=f'#name')
        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((640,640), (160,80)), text="Choose Class", manager=self.manager, object_id=f'#class')

        #Username entry line
        self.username = pygame_gui.elements.ui_text_entry_line.UITextEntryLine(relative_rect=pygame.Rect((self.gen_pos[0]-20, self.gen_pos[1]-100), (200, 50)), manager=self.manager, object_id="#usr")
        self.username.set_text("id")

        #Password entry line
        self.password = pygame_gui.elements.ui_text_entry_line.UITextEntryLine(relative_rect=pygame.Rect((self.gen_pos[0]+5, self.gen_pos[1]-50), (150, 50)), manager=self.manager, object_id="#pswd")
        self.password.set_text("mdp")

    #Method to update the player info from the JSON file
    def info_updater(self):
        with open("saves/personnage.json") as data:
            self.player_info = json.load(data)

    #Method for the Name, Class and Level display
    def display(self):
        self.info = (self.info_font.render(self.player_info["Name"], False, (0, 0, 0)), self.info_font.render(self.player_info["Class"].capitalize(), False, (0, 0, 0)), self.info_font.render(self.player_info["Level"], False, (0, 0, 0)))

    def loop(self):
        while self.run:
            #Calcul of the delta time between two frame
            time_delta = self.clock.tick(60)/1000.0
            #Blit the images and text on the screen
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.display_text[0], (self.gen_pos[0]-20, self.gen_pos[1]-130))
            self.screen.blit(self.display_text[1], (self.gen_pos[0]+5, self.gen_pos[1]-75))
            #Update the screen
            self.info_updater()
            self.display()
            #Blit the player information in the left corner of the screen
            for information, position in zip(self.info, self.info_pos):
                self.screen.blit(information, position)

            for event in pygame.event.get():
                #Close the game when the quit event appeared
                if event.type == pygame.QUIT:
                    import sys
                    self.run = False
                    sys.exit()

                #Check if the event is on a pygame_gui element
                if "user_type" in event.dict:
                    #Check if it's a button clicked
                    if event.dict['user_type'] == "ui_button_pressed":
                        #Launch the connection procedure
                        if event.dict["ui_object_id"] == "#connection":
                            #Check the connection state
                            if not(self.connected):
                                #Send the username and password to the server
                                text_conn=f"{API_KEY}/{self.username.get_text()}/{self.password.get_text()}"
                                self.client.client.sendall(bytes(f"§{len(text_conn)}µ{text_conn}", "utf-8"))

                                #Wait for the return
                                self.client.information_return()

                            #Check the return_info and change the state if it's the string "connected"
                            if self.client.return_info == "connected":
                                self.connected = True
                                self.client.return_info = None

                            #If the player is connected send his player information to the server
                            if self.connected:
                                self.info_updater()
                                text_conn=f"{API_KEY}/player_info/{self.player_info['Name']}/{self.player_info['Class']}/{self.player_info['Level']}"

                                self.client.client.sendall(bytes(f"§{len(text_conn)}µ{text_conn}", "utf-8"))

                                #Wait for the server response for a fight
                                self.client.information_return()

                                fight_info = self.client.return_info.split("/")

                                self.client.return_info = None

                                #Launch the fight with the information received from the server
                                Fight_Client(fight_info[0], self.player_info['Name'], fight_info[1], self.attack_dict[self.player_info['Class']], fight_info[2], fight_info[3], self.client)

                                #----------------------------------------------
                        #Change the GUI depends of the desired one, after a button click
                        elif event.dict["ui_object_id"] == "#name":
                            Name()

                        elif event.dict["ui_object_id"] == "#class":
                            Class()

                #listen to the manager process (from pygame_gui)
                self.manager.process_events(event)

            #update the manager of pygame_gui
            self.manager.update(time_delta)
            #draw on the screen from the manager of pygame_gui
            self.manager.draw_ui(self.screen)
            #update the display (pygame)
            pygame.display.update()
