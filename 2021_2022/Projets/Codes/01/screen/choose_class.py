"""
Name : Choix de la Classe

Description : Interface permettant le choix de la classe par les joueurs et l'enregistre dans un fichier JSON.

starring: Morel Noah
"""
#Importation des librairies
import pygame, pygame_gui
from saves.constants import *
import json

#Classe permettant de générer l'interface
class Class:
    def __init__(self):
        #Définition du nom de la fenêtre
        pygame.display.set_caption(WINDOW_NAME)
        #Génération de l'écran pour l'affichage dans une dimension de 1280x720 pixels
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        #Variable run pour la boucle principale de l'interface
        self.run = True
        #Manager de pygame_gui
        self.manager = pygame_gui.UIManager(SCREEN_SIZE)
        #Horloge interne de l'interface pour son affichage
        self.clock = pygame.time.Clock()

        #Chargement des images à afficher sur l'interface
        self.background = pygame.image.load(BACKGROUND_PATH)
        self.titre = pygame.image.load(CLASS_TITLE_PATH)
        self.logo = pygame.image.load(CLASS_LOGO)

        #Enregistrement des boutons pour le choix des classes dans le manager
        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((190,300), (350,100)),
                                     text="MAGE",
                                     manager=self.manager,
                                     object_id=f'#MAGE')

        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((740,300), (350,100)),
                                     text="PRETRE",
                                     manager=self.manager,
                                     object_id=f'#PRETRE')

        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((110,450), (350,100)),
                                     text="CHEVALIER",
                                     manager=self.manager,
                                     object_id=f'#CHEVALIER')

        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((820,450), (350,100)),
                                     text="PALADIN",
                                     manager=self.manager,
                                     object_id=f'#PALADIN')

        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((190,600), (350,100)),
                                     text="VOLEUR",
                                     manager=self.manager,
                                     object_id=f'#VOLEUR')

        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((740,600), (350,100)),
                                     text="DANSEUR",
                                     manager=self.manager,
                                     object_id=f'#DANSEUR')


        pygame_gui.elements.UIButton(relative_rect=pygame.Rect((1200,10), (75,50)),
                                     text='Quit',
                                     manager=self.manager,
                                     object_id=f'#QUIT')

        #Lancement de la boucle principale
        self.loop()

    def loop(self):
        #Boucle infini tant que self.run est égale à True
        while self.run:
            #Calcul du delta temps entre deux frame
            time_delta = self.clock.tick(60)/1000.0
            #Blit des images sur l'interface aux coordonnées choisies
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.titre, (250, 100))
            self.screen.blit(self.logo, (535, 400))


            #Parcours des events intervenant sur l'interface
            for event in pygame.event.get():
                #Fermeture du jeu si la croix est appuyée
                if event.type == pygame.QUIT:
                    import sys
                    sys.exit()

                #Vérification de l'input comme étant une action sur un élément de pygame_gui
                if "user_type" in event.dict:
                    #vérification permettant de savoir si un bouton à était pressé
                    if event.dict['user_type'] == "ui_button_pressed":
                        #Chargement du fichier JSON personnage dans la variable perso_info
                        with open("saves/personnage.json", "r") as perso:
                            perso_info = json.load(perso)
                            class_name = perso_info["Class"]

                        #Stockage dans la variable class_name de la classe du personnage en fonction du bouton appuyée
                        if event.dict["ui_object_id"] == "#MAGE":
                            class_name = "mage"

                        elif event.dict["ui_object_id"] == "#PRETRE":
                            class_name = "pretre"

                        elif event.dict["ui_object_id"] == "#CHEVALIER":
                            class_name = "chevalier"

                        elif event.dict["ui_object_id"] == "#PALADIN":
                            class_name = "paladin"

                        elif event.dict["ui_object_id"] == "#VOLEUR":
                            class_name = "voleur"

                        elif event.dict["ui_object_id"] == "#DANSEUR":
                            class_name = "danseur"

                        #Stockage dans le fichier JSON du personnage la classe choisi à la clé "Class"
                        with open("saves/personnage.json", "w") as perso:
                            perso_info["Class"] = class_name
                            json.dump(perso_info, perso)

                        #Si le bouton quit à était appuyée passe self.run à False afin de retourner sur l'interface précèdente
                        if event.dict["ui_object_id"] == "#QUIT" :
                            self.run = False


                #Processing des events intervenant sur les éléments du manager de pygame_gui
                self.manager.process_events(event)

            #Mise à jour de l'interface
            self.manager.update(time_delta)
            self.manager.draw_ui(self.screen)
            pygame.display.update()
