"""
Name : Main

Description : L'écran principale du jeu permettant de lancer les autres écrans.

Dependencies :
-pygame
-pygame_gui
-Client
-Connection_GUI

starring : SAUTRON Quentin, MOREL Noah, K/BIDI Thomas
"""

#Importation des librairies
import pygame, pygame_gui
from network.client import Client
from screen.connection_gui import Connection_GUI
from saves.constants import *

#Initialisation de pygame
pygame.init()

#Chargement du client
client = Client()

#Horloge interne de l'interface pour son affichage
clock = pygame.time.Clock()
#Variable run pour la boucle principale de l'interface
run = True

#Définition du nom de la fenêtre
pygame.display.set_caption(WINDOW_NAME)
#Génération de l'écran pour l'affichage dans une dimension de 1280x720 pixels
screen = pygame.display.set_mode(SCREEN_SIZE)
#Manager de pygame_gui
manager = pygame_gui.UIManager(SCREEN_SIZE)

#Chargement des images à afficher sur l'interface
background = pygame.image.load(BACKGROUND_PATH)
logo = pygame.image.load(LOGO_PATH)

pygame_gui.elements.UIButton(relative_rect=pygame.Rect((540,450), (200,100)),
                                 text="PLAY !",
                                 manager=manager,
                                 object_id='#PLAY')

pygame_gui.elements.UIButton(relative_rect=pygame.Rect((1200,10), (75,50)),
                                 text='Quit',
                                 manager=manager,
                                 object_id='#QUIT')

while run:
    #Calcul du delta temps entre deux frame
    time_delta = clock.tick(60)/1000.0
    #Blit des images sur l'interface aux coordonnées choisies
    screen.blit(background, (0, 0))
    screen.blit(logo, (290, 0))

    for event in pygame.event.get():
        #Fermeture du jeu si la croix est appuyée
        if event.type == pygame.QUIT:
            import sys
            sys.exit()

        if "user_type" in event.dict:
            if event.dict['user_type'] == "ui_button_pressed":
                #Lancement de la prochaine interface
                if event.dict["ui_object_id"] == "#PLAY":
                    Connection_GUI(client)

                #Si le bouton quit à était appuyée passe self.run à False ce qui ferme le jeu
                if event.dict["ui_object_id"] == "#QUIT" :
                    run = False

        #Processing des events intervenant sur les éléments du manager de pygame_gui
        manager.process_events(event)

    #Mise à jour de l'interface
    manager.update(time_delta)
    manager.draw_ui(screen)
    pygame.display.update()
