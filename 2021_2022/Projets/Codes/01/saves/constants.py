"""
Name : Constants

Description : All the constants for the game are stored here.

starring: K/BIDI Thomas
"""

#Screen Size and window name of the all the interface of the game
WINDOW_NAME = "The Quest for the Sacred Dodo - ZQSD"
SCREEN_SIZE = (1280, 720)

#Path for the assets
BACKGROUND_PATH = 'assets/fond.jpg'
LOGO_PATH = 'assets/Logo.png'
CLASS_TITLE_PATH = 'assets/selection coq.png'
CLASS_LOGO = 'assets/Logo2.png'
NAME_SELECT_PATH = 'assets/selection nom.png'
COQ_PATH = 'assets/coq.png'
REVERSE_COQ_PATH = 'assets/coq_reverse.png'

#Network constants
API_KEY = "zqsd"
HOST = "127.0.0.1"
PORT = 1234

#Dictionnary where all the attack from each class are store
ATTACK_INFO = {"mage" : {"Boule de feu" : ("Maj", 2, 90, 0, 2),
    "Explosion céleste" : ("Maj", 3, 80, 0, 2),
    "Picorage" : ("Phy", 3, 100, 0, 5),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},

"pretre" : {"Touché divin" : ("Soin", 90, 0, 5, 3),
    "Prière vengeresse" : ("Maj", 3, 85, 0, 3),
    "Déplumage" : ("Phy", 2, 100, 0, 2),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},

"chevalier" : {"Epée enflamée" : ("Maj", 2, 80, 0, 2),
    "Dernier Recours" : ("Maj", 0, 10, 0, 30),
    "Bouclier Ailés" : ("Phy", 3, 90, 0, 1),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},

  "paladin" : {"Foi Divine" : ("Soin", 90, 0, 3, 2),
    "Eclair Pourfendeur" : ("Maj", 4, 50, 0, 10),
    "Epee de Pouladin" : ("Phy", 4, 95, 0, 1),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},

  "voleur" : {"Larcin Vitale" : ("Soin", 80, 0, 2, 5),
    "Dégâts des Ombres" : ("Maj", 3, 100, 0, 2),
    "Dague Empoisonnées" : ("Phy", 4, 85, 0, 2),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},

  "danseur" : {"Danse Revigorante" : ("Soin", 100, 0, 3, 2),
    "Esprits Dansant" : ("Maj", 6, 50, 0, 5),
    "Kapoera" : ("Phy", 4, 95, 0, 3),
    "Coup de Z'aile" : ("Phy", 2, 50, 0, 8)},
}
