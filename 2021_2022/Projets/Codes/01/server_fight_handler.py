"""
Name: Server Fight Handler

Description: This the launcher for the server and the Fight_Handler.
It contains a while loop to launch fights when the waiting queue is above 2.

Dependencies :
-json
-Personnage
-JRPG_Server

starring : K/BIDI Thomas
"""

#Libraries import
from game_dependencies.personnage import *
from network.server_base import JRPG_Server
import json
#Constants imports
from saves.constants import *

#Class to handle the fight between two player from their choice
class PVP_Fight:
    def __init__(self, opponent1, opponent2):

        #Choose which player will play first depends on their speed
        if opponent1.vitesse > opponent2.vitesse:
            self.first = opponent1
            self.second = opponent2
        elif opponent1.vitesse < opponent2.vitesse:
            self.first = opponent2
            self.second = opponent1
        else:
            import random
            rand = random.randint(1,2)

            if rand == 1:
                self.first = opponent1
                self.second = opponent2
            else:
                self.first = opponent2
                self.second = opponent1

    #Methods to apply the attack to the players
    def fighting(self, attack_list):
        fpa = attack_list[self.first.nom]
        spa = attack_list[self.second.nom]

        if fpa[1][0] == "Phy":
            #The first attack will apply on the second player
            self.second.retour_attaque(self.first.attaque_phy(fpa[1]))
            print(self.first.nom, "a utilisé", fpa[0])

        elif fpa[1][0] == "Maj":
            #The first attack will apply on the second player
            self.second.retour_attaque(self.first.attaque_maj(fpa[1]))
            print(self.first.nom, "a utilisé", fpa[0])

        elif fpa[1][0] == "Soin":
            #The first attack will apply on the second player
            self.first.attaque_stat(fpa[1])
            print(self.first.nom, "a utilisé", fpa[0])

        if spa[1][0] == "Phy":
            #And the second attack on the first player
            self.first.retour_attaque(self.second.attaque_phy(spa[1]))
            print(self.second.nom, "a utilisé", spa[0])

        elif spa[1][0] == "Maj":
            #And the second attack on the first player
            self.first.retour_attaque(self.second.attaque_maj(spa[1]))
            print(self.second.nom, "a utilisé", spa[0])

        elif fpa[1][0] == "Soin":
            #The first attack will apply on the second player
            self.second.attaque_stat(spa[1])
            print(self.second.nom, "a utilisé", spa[0])

        return fpa[0], spa[0]


class Fight_Handler:
    def __init__(self, fight_obj, fight_name):
        #Print on the server console when a fight begin
        print("Fight Start")
        #Fight object and fight name
        self.fight = fight_obj
        self.fight_name = fight_name
        #Launch the loop of the fight
        self.loop()

    def loop(self):
        #Loop of the fight, until an opponent is dead
        while self.fight.first.sante > 0 and self.fight.second.sante > 0:
            #Check if the fight list for the "fight1" have all the necessary data for the round
            if server.fight_list[self.fight_name][self.fight.first.nom] != None and server.fight_list[self.fight_name][self.fight.second.nom] != None:
                #If the two player have send their attack choice, change the fight in Ready Mode
                server.fight_list[self.fight_name]["Ready"] = True
                #Print on the server log when the two player have send their attack
                print("Ready")

            #Check if the fight is in the Ready Mode
            if server.fight_list[self.fight_name]["Ready"]:

                #A pretty print for the server logs
                print("-------------------------------------------")
                print(self.fight.first.nom, "-", self.fight.second.nom)
                attack_name = self.fight.fighting(server.fight_list[self.fight_name])
                print("Santé : ")
                print(self.fight.first.nom, ":", self.fight.first.sante, "            ", self.fight.second.nom, ":", self.fight.second.sante)

                #Reset the opponents attack to None
                server.fight_list[self.fight_name][self.fight.first.nom] = None; server.fight_list[self.fight_name][self.fight.second.nom] = None
                #Change the fight in Unready Mode
                server.fight_list[self.fight_name]["Ready"] = False
                print("-------------------------------------------")

                #Send to the two player the information about the fight
                server.player_socket[self.fight.first.nom].sendall(bytes(f"{self.fight.first.sante}/{self.fight.second.sante}/{attack_name[1]}","utf-8"))
                server.player_socket[self.fight.second.nom].sendall(bytes(f"{self.fight.second.sante}/{self.fight.first.sante}/{attack_name[0]}","utf-8"))

        #Show the "LOSER"
        if self.fight.first.sante <= 0:
            print(self.fight.first.nom, "à perdu !")
        elif self.fight.second.sante <= 0:
            print(self.fight.second.nom, "à perdu !")

        #Delete the player information on the server player to socket dictionnary
        del server.player_socket[self.fight.first.nom]
        del server.player_socket[self.fight.second.nom]
        #Delete the fight from the fight list
        del server.fight_list[self.fight_name]

#Generate the server and store it in the server variable
server = JRPG_Server(HOST, PORT)

#Number of the fight to generate the ID
fight_nb = 1

#Loading of the json file of the class inside the CLASS_DICT constant
with open("saves/class.json") as class_info:
    CLASS_DICT = json.load(class_info)

#Start a fight from the PVP_Fight class by using the two player
while True:
    #If more than 2 players are in the waiting queue for fight, start a fight betweend the first two player of the queue
    if len(server.waiting_player) >= 2:
        #Get the 2 first player in queue informations
        player1_info, player2_info = server.waiting_player[0], server.waiting_player[1]

        #Creation of the player object
        player1 = Personnage(player1_info[0], CLASS_DICT[player1_info[1]], player1_info[2], equipement)
        player2 = Personnage(player2_info[0], CLASS_DICT[player2_info[1]], player2_info[2], equipement)

        #Creation of the data to send the player for the generation of the fight GUI
        p1_info = f"fight{fight_nb}/{player2_info[0]}/{player1.PV}/{player2.PV}"
        p2_info = f"fight{fight_nb}/{player1_info[0]}/{player2.PV}/{player1.PV}"

        #Creation of a new fight by generated it inside the fight_list in the server
        server.fight_list[f"fight{fight_nb}"] = {player1.nom : None, player2.nom : None, "Ready" : False}

        #Send the player the informations about their fight
        server.player_socket[player1_info[0]].sendall(bytes(p1_info, "utf-8"))
        server.player_socket[player2_info[0]].sendall(bytes(p2_info, "utf-8"))

        #Launch of a fight handler with the Player Fight Object and the fight ID
        Fight_Handler(PVP_Fight(player1, player2), f"fight{fight_nb}")

        #Delete the player in the waiting queue
        server.waiting_player = server.waiting_player[2:]

        #Increment the fight_nb variable to change the next fight id
        fight_nb+=1
