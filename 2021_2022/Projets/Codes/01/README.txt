To launch the game you have to do the following steps:
- Launch the server_fight_handler.py
- launch the main.py

---Warning---
If main.py crash when you try to run it.
Launch it from a shell with "python /path/to/main.py"


If you want to try the fight alone :
- Launch the server_fight_handler.py
- Launch 2 main.py
- From the first one choose your class and name and click on connect
- From the second one choose your class and a different name and then click on connect


---Warning---
The first game to click on connect will freeze until another one click on connect.

The HOST and PORT constants are inside saves/constants.py if you want to make an open to internet server.
