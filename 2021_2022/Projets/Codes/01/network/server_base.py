"""
Name : Server Base

Description : This is the server who handle all the data from the client and their identification for the fights.

Dependencies :
-threading
-time
-NSPy_Server

starring : K/BIDI Thomas
"""

#Libraries import
from network.modules.NSPy import NSPy_Server
import time
import threading
from saves.constants import *

class JRPG_Server(NSPy_Server, threading.Thread):
    def __init__(self, HOST="127.0.0.1", PORT=1234):
        #Launch of this server in a Thread
        threading.Thread.__init__(self)
        #Buffer of the JRPG_Server
        self.buffer = {}
        #Dictionnary of the fight's list
        self.fight_list = {}
        #List to handle the registered user
        self.register_user = []
        #Dictionnary to match the player name and their socket
        self.player_socket = {}
        #Tuple with the API key name and the buffer name
        server_element = (API_KEY, "buffer")

        self.waiting_player = []

        #Initialisation of the parents class
        super().__init__(HOST, PORT, server_element)

        #Launch of the loop_buffer in a Thread
        self.loop_buffer = threading.Thread(target=self.loop_buffering)
        self.loop_buffer.start()

    #Methods who check if a player who send an attack is really in a fight
    def fight_checker(self, fight, name):
        res = False
        #Get the player name in the fight
        fight_name = list(self.fight_list[fight].keys())[:2]
        if name in fight_name:
            res = True

        #Return the res
        return res

    #loop to see the data inside the buffer
    def loop_buffering(self):
        #Infinite loop
        while True:
            #For each socket registered inside the buffer we get the data they send
            for socket in self.buffer:
                #Check if the socket is registered on the JRPG_Server
                if socket in self.register_user:
                    #Check if some element are inside the buffer
                    if len(self.buffer[socket]):
                        #Store the list inside the client_info list
                        client_info = self.buffer[socket][0]

                        if client_info[0] == "attack":

                            if self.fight_checker(client_info[1], client_info[2]):
                                #Add inside the fight_list in the right fight and for the right player the attack information
                                self.fight_list[client_info[1]][client_info[2]] = (client_info[3], (client_info[4], int(client_info[5]), int(client_info[6]), int(client_info[7]), int(client_info[8])))


                        elif client_info[0] == "player_info":
                            #In this case the syntax of the command must be the following from the "player_info" : player_info/Name/Class/Level

                            #Check if the player are already or not in the player_socket dictionnary
                            if not(client_info[1] in list(self.player_socket.keys())):
                                #Create a new key with the player name and bind his socket to this key
                                self.player_socket[client_info[1]] = socket
                                #Add the player to the waiting queue
                                self.waiting_player.append((client_info[1], client_info[2], [client_info[3]]))

                        #Remove the data already process inside the buffer
                        self.buffer[socket].remove(self.buffer[socket][0])

                #If the player is not registered check his connection data
                else:
                    #Check if some element are inside the buffer
                    if len(self.buffer[socket]):
                        #Registered the element in an id variable
                        id = (self.buffer[socket][0][0], self.buffer[socket][0][1])

                        #Check if the id exist in the "database" (WORK IN PROGRESS)
                        if id == ("id", "mdp"):
                            #Register the user
                            self.register_user.append(socket)
                            socket.sendall(b"connected")
                        else:
                            socket.sendall(b"wrong id")
                        #Remove the data already process in the buffer
                        self.buffer[socket].remove(self.buffer[socket][0])

            #Use the built-in NSPy's buffer_updater function to update the buffer at the end of the loop
            self.buffer_updater()
