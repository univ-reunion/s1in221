"""
Name : Client Class

Description : This class manages the client side of the game by allowing players to connect to the game server and play against each other.

Dependencies :
-threading
-NSPY_Client

starring : K/BIDI Thomas
"""
#Libraries import
from network.modules.NSPy import NSPy_Client
import threading
from saves.constants import *

class Client(NSPy_Client):
    def __init__(self):
        import time
        #Initialisation of the parent class
        super().__init__(HOST, PORT)
        #A pause for the client to connect
        time.sleep(1)
        #A packet to be add in the server buffer
        self.client.sendall(bytes(f"§{len(API_KEY)}µ{API_KEY}", "utf-8"))
        #Variable designed to store the information received by the server
        self.return_info = None

    #Send and wait methods
    def send_and_wait(self, fight_name, nom, param):
        #Generate the attack command in the right format
        info = f"{API_KEY}/attack/{fight_name}/{nom}/"+param
        len_info = len(info)
        x_shellify = bytes(f"§{len_info}µ{info}", "utf-8")
        self.client.sendall(x_shellify)

        #Launch in a thread the listening methods
        self.info = threading.Thread(target=self.information_return)
        self.info.start()

    #Listen to the data received by the server
    def information_return(self):
        self.data = self.client.recv(1024)
        self.return_info = self.data.decode("utf-8")
