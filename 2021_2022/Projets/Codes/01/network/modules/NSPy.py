"""
NSPy - Networking System Python

Description :
First version of NSPy. This module is design to ease the creation of server in
python.
You just have to create a child class of NPSy_Server, give im a tuple
with the api_key and the name of the dictionnary you want to use as buffer.
After that you init the parent class and all the data who will be
send to the Server with your api_key will be redirect in your buffer.
Connection and disconnection are handled by the library.

You can also use the NSPy_Client to ease the client creation.
---WARNING--- W.I.P. State

starring : K/BIDI Thomas
"""

class NSPy_Server:
    def __init__(self, HOST, PORT, buffer_list=None):
        import socket
        import threading
        import selectors

        #Define the socket and register it in the selectors
        self.selectors = selectors.DefaultSelector()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((HOST, PORT))
        self.socket.listen()
        self.socket.setblocking(False)
        self.selectors.register(self.socket, selectors.EVENT_READ, data=None)

        self.deconnection_buffer = []
        self.connection_buffer = []

        if buffer_list:
            if isinstance(buffer_list[0], str):
                self.buffer_list = (None, buffer_list)

            elif isinstance(buffer_list[0], tuple):
                self.buffer_list = buffer_list

            #Réviser ce bout de code pour le mettre à jour, ajouter la gestion de plusieurs buffers
            for i in self.buffer_list:
                if i != None:
                    if i[0] != "x":
                        creation = f"""if not(connection in self.{i[1]}):\n
                        self.{i[1]}[connection] = []\n
                        """

                        appending = f"""if key.fileobj in self.{i[1]}:\n
                        self.{i[1]}[key.fileobj].append(data.outb.decode("utf-8").split("/")[1:])
                        """

                        deletion = f"""if deconnection in self.{i[1]}:\n
                        del self.{i[1]}[deconnection]
                        """

                        self.compiled = (compile(creation, "compiled", "exec"), compile(appending, "compil", "exec"), compile(deletion, "compil", "exec"))
        else:
            self.buffer_list = buffer_list

        #Starting of the listener in a Thread to handle the different connection and packets
        self.listener_handler = threading.Thread(target=self.listener)
        self.listener_handler.start()

        print("Server is up")

#This function have to listen to new event on the server like new connection or incoming packets (shell commands or data)
    def listener(self):
        import threading
        import time
        #Boolean to control the listener state
        up = True

        while up:
            #Checking incoming events in the selectors
            self.events = self.selectors.select(timeout=None)

            #For the different key and mask in the events check if it's another connection or incoming packets
            for key, mask in self.events:
                try:
                    #If no data sent that mean that it's the first connection attempt
                    if key.data is None:
                        #send the fileobj to be handle and set up the connection
                        self.connection_handler(key)

                    #send the incoming packets to the analyzer, to be analyze
                    else:
                        self.analyzer(key, mask)
                #Handled when a client disconnect
                except ConnectionResetError:
                    print("Someone have disconnect")
                    self.selectors.unregister(key.fileobj)
                    self.deconnection_buffer.append(key.fileobj)
                    key.fileobj.close()

#This function handle a new connection or disconnection by a client on the buffer
    def buffer_updater(self):
        #Disconnection
        if len(self.deconnection_buffer):
            for deconnection in self.deconnection_buffer:
                if self.buffer_list:
                    for i in self.buffer_list:
                        if i != None:
                            exec(self.compiled[2])
            self.deconnection_buffer.clear()
        #Connection
        if len(self.connection_buffer):
            for connection in self.connection_buffer:
                exec(self.compiled[0])
            self.connection_buffer.clear()

#This function handle a new connection to the server by accepting the socket and register him in the selectors
    def connection_handler(self, key):
        import types
        import selectors
        sock = key.fileobj
        self.conn, self.addr = sock.accept()
        self.conn.setblocking(False)
        self.data = types.SimpleNamespace(addr=self.addr, inb=b'', outb=b'')
        self.events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self.selectors.register(self.conn, self.events, data=self.data)
        print("Connection Handled")


#This function analyze the incoming packets and manage where they will be send to the process
    def analyzer(self, key, mask):
        import selectors

        #print(key.data.outb)
        #we define the sock by being the one of the connection and the data by the incoming packets
        sock = key.fileobj
        data = key.data
        msg_length = 0
        msg_data = ""

        #Boolean to check the mode of use, here shell mode is check
        #getting the data from the sock and register it in the data.outb of the user's socket
        if mask & selectors.EVENT_READ:
            recv_data = sock.recv(1024)
            if recv_data:
                data.outb += recv_data

        index_elem = lambda char : data.outb.decode("utf-8").find(char)

        #Parse the data to find his length
        if len(data.outb.decode("utf-8")) != 0:
            msg_length = int(data.outb.decode("utf-8")[index_elem("§")+1:index_elem("µ")])+1
            starting_point = int(index_elem("µ"))+1
            msg_data = data.outb.decode("utf-8")[starting_point:starting_point+msg_length]

        #----------------------------------------------------------------------#

        if self.buffer_list:
            for i in self.buffer_list:
                if i != None:
                    if msg_data.split("/")[0] == i[0]:
                        if eval(f"not(key.fileobj in self.{i[1]})"):
                            self.connection_buffer.append(key.fileobj)
                            print("Register to buffer :", i[1])
                            # print(key.fileobj)
                            # print(msg_data)
                        else:
                            exec(self.compiled[1])

        #ACTION BY DEFAULT
        if mask & selectors.EVENT_WRITE:
            data.outb = data.outb[len(data.outb):]

class NSPy_Client:
    def __init__(self, HOST, PORT):
        import socket
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((HOST, PORT))
