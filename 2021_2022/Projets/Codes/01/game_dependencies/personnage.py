"""
Name : Classe Personnage

Description : Classe de génération des personnages.

starring : MOREL Noah
"""

from random import randint

#dictionnaire d'equipements du personnage
equipement = {'arme':None ,'Torse':None ,'tête': None ,'ailes':None}

class Personnage :
    #initialisation des carctéristiques du personnage
    def __init__(self, nom, classe, lvl, equipement):
        self.nom = nom
        self.lvl = lvl
        self.equipement = equipement
        self.ratio = ratio = {'attaque_physique' : 1, 'attaque_magique' : 1, 'vitesse' : 1, 'defense' : 1}
        self.classe = classe
        self.stats()

    #statistiques du personnage
    def stats(self):
        self.PV = self.classe['PV']
        self.sante = self.classe['PV']
        self.PM = self.classe['PM']
        self.magie = self.classe['PM']
        self.att_basique = self.classe['Attaque Basique']
        self.att_magique = self.classe['Attaque Magique']
        self.defense = self.classe['Defense']
        self.vitesse = self.classe['Vitesse']

    #attaques de types physiques (retourne des dégats):
    def attaque_phy(self, info_attaque):
        res = 0

        #données des attaques de types physiques
        type = info_attaque[0]
        degat_init = info_attaque[1]
        precision = info_attaque[2]
        absorbtion_pm = info_attaque[3]
        bonus_critique = info_attaque[4]

        x = randint(1, 100)
        #calcule de probabilité de rater l'attaque
        if x > precision :
            print("Attaque Ratée !")

        else :
            #calcul des dégats tautaux
            res = (self.att_basique + degat_init)*self.ratio['attaque_physique']

            #boucle pour les attaques physiques permetant de regagner des PM
            if absorbtion_pm != 0:
                self.magie + absorbtion_pm
                print("PM réstaurés !")
                if self.magie > self.PM :
                    self.magie = self.PM
                else :
                    pass
            else:
                pass
            x = randint(1, 100)

            #calcule de la probabilité de faire des dégats critiques
            if x <= 5 :
                res += bonus_critique

        #la fonction retourne les gégats de l'attaque et son type
        return res, type

    #les attaques magiques (retournent des dégats en utilisant des PM)
    def attaque_maj(self, info_attaque):
        res = 0

        #données des attaques de types magiques
        type = info_attaque[0]
        degat_init = info_attaque[1]
        precision = info_attaque[2]
        cout_pm = info_attaque[3]
        bonus_critique = info_attaque[4]

        #calcule de probabilité de rater l'attaque
        x = randint(1, 100)
        if x > precision :
            print("Attaque Ratée !")
        else :
            #vérification que la quantité de PM soit suffisante
            if self.magie - cout_pm < 0 :
                print("Plus de PM !")
            else :
                #calcul des dégats tautaux
                res = (self.att_magique + degat_init)*self.ratio['attaque_magique']
                self.magie -= cout_pm

            x = randint(1, 100)

            #calcule de la probabilité de faire des dégats critiques
            if x <= 5 :
                res += bonus_critique

        #la fonction retourne les gégats de l'attaque et son type
        return res, type

    #attaques de types altération de statut (retourne des valeurs qui peuvent modifier des caractéristiques sur les personnages)
    #les attaques d'altèrations ne sont pas encore utilisées dans notre jeu.
    def attaque_stat(self, info_attaque):
        res = 0

        #données des attaques de types magiques
        type = info_attaque[0]
        precision = info_attaque[1]
        cout_pm = info_attaque[2]
        alteration = info_attaque[3]
        bonus_critique = info_attaque[4]

        #calcule de probabilité de rater l'attaque
        x = randint(1, 100)
        if x > precision :
            print("Attaque Ratée !")
        else :
            #vérification que la quantité de PM soit suffisante
            if self.magie - cout_pm < 0 :
                print("Plus de PM !")
            else :
                res = alteration
                self.magie -= cout_pm

                x = randint(1, 100)

                #calcule de la probabilité de faire des dégats critiques
                if x <= 5 :
                    res += bonus_critique
                else :
                    pass

                #attaques qui modifient les ponts de vie du joueur
                if type == "Soin" :
                    self.sante += res
                    print("Santé réstauré !")
                    if self.sante > self.PV :
                        self.sante = self.PV

                    else :
                        pass
                #attaques qui modifient la vitesse
                elif type == "V" :
                    self.vitesse += res
                #attaques qui modifient la défense
                elif type == "D" :
                    self.defense += res
                #attaques qui modifient les points d'attaques
                elif type == "AB" :
                    self.att_basique += res
                #attaques qui modifient les points de magie
                else :
                    self.att_magique += res
        #la fonction retourne les gégats de l'attaque et son type
        return res, type

    #gestion du résultat des attaques envoyées par l'adversaire
    def retour_attaque(self, info):
        res = info[0]
        type = info[1]
        x = randint(1, 100)
        #probabilité d'esquiver une attaque
        if (self.vitesse*self.ratio['vitesse']) > x :
            res = "Attaque Esquivé !"
            print("Attaque Esquivé !")
        else:
            #application des valeurs retournées par fonction d'attaques, en fonction de leurs type
            if type == "AB":
                self.ratio["attaque_physique"] -= res
            elif type =="AM":
                self.ratio["attaque_magique"] -= res
            elif type =="V":
                self.ratio["vitesse"] -= res
            elif type =="D":
                self.ratio["defense"] -= res
            elif type == "Phy":
                #calcule de la réduction des dégats avec la caractéristique de défense
                if self.defense*self.ratio['defense'] < res :
                    self.sante -= (res - (self.defense*self.ratio['defense']))
                else :
                    pass
            elif type == "Maj":
                try:
                    #calcule de la réduction des dégats avec la caractéristique de défense
                    if (self.defense*self.ratio['defense']) < res :
                        self.sante -= (res -(self.defense*self.ratio['defense']))
                    else :
                        pass
                except:
                    pass
