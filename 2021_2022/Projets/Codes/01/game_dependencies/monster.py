"""
Name : Classe Monstres

Description : Gestion des monstres dans le jeu

starring : Quentin Sautron
"""
from random import randint
import random

 #class possédant les éléments principale
class Monstres :
    def __init__(self, nom, classe, lvl, equipement, ratio, sorts):
        self.nom = nom
        self.classe = classe
        self.lvl = lvl
        self.equipement = equipement
        self.ratio = ratio
        self.PV = classe['PV']
        self.sante = classe['PV']
        self.PM = classe['PM']
        self.magie = classe['PM']
        self.att_basique = classe['Attaque Basique']
        self.att_magique = classe['Attaque Magique']
        self.defense = classe['Defense']
        self.vitesse = classe['Vitesse']

        def attaque_phy(self, info_attaque):
            res = None

            type = info_attaque[0]
            degat_init = info_attaque[1]
            precision = info_attaque[2]
            absorbtion_pm = info_attaque[3]
            bonus_critique = info_attaque[4]

            x = randint(1, 100)
            if x > precision :
                print("Attaque Ratée")
            else :
                res = (self.att_basique + degat_init)*self.ratio['attaque_physique']
                self.magie + absorbtion_pm

                x = randint(1, 100)

                if x <= 5 :
                    res += bonus_critique
                else :
                    pass

        def attaque_mag(self, info_attaque):
            res = None

            type = info_attaque[0]
            degat_init = info_attaque[1]
            precision = info_attaque[2]
            cout_pm = info_attaque[3]
            bonus_critique = info_attaque[4]

            x = randint(1, 100)
            if x > precision :
                print("Attaque Ratée")
            else :
                res = (self.att_magique + degat_init)*self.ratio['attaque_magique']
                self.magie -= cout_pm

                x = randint(1, 100)

                if x <= 5 :
                    res += bonus_critique
                else :
                    pass

#Fonction pour le choix aléatoire des sorts par le monstre
def sorts(self, nature):

    sorts_phy=nature
    sorts_mag=nature

x = randint(1, 10)
if x>5:
    sorts_phy=["PHY"]

if x<5:
    sorts_mag=["MAG"]


#Système de loots basés sur un random.
def looting(self,recompense):
    res = None

    tete = recompense[1]
    torse = recompense[2]
    jambe = recompense[3]
    arme = recompense[4]

x = randint(1, 10)
if x>4:
        print("NONE")

if x<4:
    loot= ['tete', 'torse', 'jambe', "arme"]
    print(loot[x])
