import pygame
from random import randint
import pygame_menu #installation d'un paquedge pip install pygame-menu

pygame.init()


#size = (1080, 800)
size = (1380,800)
screen = pygame.display.set_mode(size,pygame.RESIZABLE) #créer une surface modulable
pygame.display.set_caption("PUISSANCE 4")


noir = (0,0,0)
rouge = (255,0,0)
jaune = (255,255,0)
pattern_color = {-1 : noir, 0 : rouge, 1 : jaune}   #paterne de couleur selon les types de jetons

def surface_size(screen):       #taille du jeu et des jetons
    actual_size = pygame.Surface.get_size(screen)
    actual_size = (actual_size[0]-300,actual_size[1])
    actual_size_x = 7.5+((actual_size[1]-80)/6)/2
    radius = -7.5+((actual_size[1]-40)/6)/2
    return ((actual_size[0],actual_size[1]), actual_size_x, radius)

class game:     #controller
    
    def __init__(self,screen, grille,joueur,fleche):
        self.screen = screen
        self.joueur = joueur
        self.grille = grille
        self.fleche = fleche

        self.p1 = grid(self.screen, self.grille)
        self.p1.set_game(self.fleche)
        self.p1.set_button(self.joueur)

    def set_timer(self):
        self.current_time = pygame.time.get_ticks()
        self.game_time = self.current_time
        self.minute = 0
        self.seconde = 0    

    def help_button_timer(self):
        start = True
        while start:
            help_menu = self.p1.help_button()

            if help_menu.is_enabled():
                help_menu.draw(self.screen)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()

                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        start = False

            pygame.display.flip()
        self.p1.set_game(self.fleche)
        self.p1.set_button(self.joueur) 

    def get_timer(self):
        actual_size, actual_size_x , radius = surface_size(self.screen)
        self.seconde=((self.current_time-self.game_time)//1000) -60*self.minute
        if self.seconde >= 60:
            self.seconde = 0
            self.minute += 1
        if self.seconde < 10:
            self.add = 0
        else:
            self.add=""
        self.p1.timer(self.minute,self.add,self.seconde)

    def change_player(self):    # change de joueur
        if self.joueur == 0:
            self.joueur = 1
        else:
            self.joueur = 0
        return self.joueur
    
    def change_tour(self):                         
        if self.tour == 0:                         
            self.tour = 1                          
        else:                                      
            self.tour = 0                          
                                                   

    def loop(self): # la boucle
        launched = True
        while launched:
            self.get_timer()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:   # si on quite, le jeu se ferme
                    exit()

                elif event.type == pygame.VIDEORESIZE:  #si la taille de l'écran change, on reconstruit le jeu.
                    d = pygame.Surface.get_size(self.screen)
                    if d[0] < 1150:
                        d= (1150, d[1])
                    if d[1] < 700:
                        d =(d[0], 700)
                    screen = pygame.display.set_mode((d),pygame.RESIZABLE)
                    self.p1.set_game(self.fleche)
                    self.p1.set_button(self.joueur)
                elif event.type == pygame.KEYDOWN:  # si on lache une touche du clavier

                    if event.key == pygame.K_ESCAPE:
                        launched = False

                    elif event.key == pygame.K_r:   # si l'on appuie sur "+" on relance la partie
                        launched = False
                        self.regame()

                    elif event.key == pygame.K_SPACE:   # si l'on appuie sur "space" 
                        launched = self.verif_victory(self.fleche)

                    elif self.joueur == 0:  # si joueur =0, les touches pour le deplacer sont <-- et -->
                        modele = model(self.grille,self.fleche)
                        if event.key == pygame.K_RIGHT:
                            self.fleche = modele.place_model_fleche("+")#position de la fleche dans le model

                        elif event.key == pygame.K_LEFT:
                            self.fleche = modele.place_model_fleche("-")#position de la fleche dans le model

                    elif self.joueur == 1: # si joueur =1, les touches pour le deplacer sont Q et D
                        modele = model(self.grille,self.fleche)
                        if event.key == pygame.K_d:
                            self.fleche = modele.place_model_fleche("+")

                        elif event.key == pygame.K_q:
                            self.fleche = modele.place_model_fleche("-")

                    self.p1.fleche_create(self.fleche)#position de la fleche dans m'affichage

                elif event.type == pygame.MOUSEBUTTONUP:    #si l'on lache un bouton de la souris
                    pose = self.get_position()
                    if pose in [0,1,2,3,4,5,6]:
                        launched = self.verif_victory(pose)
                    elif pose == "re":
                        launched = False
                        self.regame()
                        
            self.current_time = pygame.time.get_ticks()
            pygame.display.flip()
        return None

    def verif_victory(self,a): #pose la piece et vérifie les conditions de victoire
        z = model(self.grille,self.fleche).place_model_token(a, self.screen, self.joueur)
        if z[0] == True:
            self.p1.set_game(self.fleche)
            if self.victoire(z[1],z[2]) == True:
                launched = False
                self.p1.set_button(self.joueur)
                self.victory(self.joueur)
            else:
                launched = True
            self.p1.set_button(self.change_player())
        else:
            launched = True
        return launched

    def get_position(self):
        actual_size, actual_size_x , radius = surface_size(self.screen) #méthode initialisation de la taille du jeu

        cursor_x = pygame.mouse.get_pos()[0]    # prendre la position du curseur dans la longueur
        cursor_y = pygame.mouse.get_pos()[1]    # prendre la position du curseur dans la largeur
        a=0
        for i in range(0,7):
            if a <= cursor_x and cursor_x <= a+actual_size[0]/7:
                return i
            else:
                a+=actual_size[0]/7
        a+=50
        if a <= cursor_x and cursor_x <= a+200:
            if 25 <= cursor_y and cursor_y <= (actual_size[1]/4)-25:
                self.help_button_timer()
                return None
            elif (actual_size[1]/4)+25 <= cursor_y and cursor_y <= (actual_size[1]/4)+25+(actual_size[1]/4)-50:
                return "re"

        return None

    def victoire(self,i,j): #dans un procédé compliqué test les contions de victoire
        for sens in ["horizon","vertical","diago_droite","diago_gauche"]:
            num_token = 0
            num_tokenp,num_tokenm =0,0
            ic, jc = i,j
            ic,jc = self.sens(sens,ic,jc, "+")
            while num_token != 3:
                if ic < 0 or jc < 0:
                    break;
                try:
                    self.grille[ic][jc]
                    if self.grille[ic][jc] == self.joueur:
                        num_token +=1
                        num_tokenp +=1
                        ic,jc = self.sens(sens,ic,jc,"+")
                    else:
                        break;
                except:
                    break;

            ic, jc = i,j
            ic,jc = self.sens(sens,ic,jc, "-")
            while num_token != 3:
                if ic < 0 or jc < 0:
                    break;
                try:
                    self.grille[ic][jc]
                    if self.grille[ic][jc] == self.joueur:
                        num_token +=1
                        num_tokenm +=1
                        ic,jc = self.sens(sens,ic,jc, "-")
                    else:
                        break;
                except:
                    break;
            if self.num_tokken(num_token) == True:
                return True
        return False

    def sens(self, sens,ic,jc, signe):
        if signe == "+":
            a=1
        elif signe == "-":
            a=-1
        if sens == "horizon":
            jc+=a
        elif sens == "vertical":
            ic+=a
        elif sens == "diago_droite":
            ic+=a
            jc+=a
        elif sens == "diago_gauche":
            jc+=a
            ic-=a
        return ic,jc

    def num_tokken(self, num_token):
        if num_token >=3:
            return True

    def victory(self, joueur):  #affiche le gagnant et demande de recommencer ou de quiter
        actual_size, actual_size_x , radius = surface_size(self.screen)
        restart = False
        regame = False

        while not(restart):
            self.p1.timer(self.minute,self.add,self.seconde)
            message_menu = self.p1.victoire_message(joueur)

            if message_menu.is_enabled():
                message_menu.draw(self.screen)

            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()
                elif event.type == pygame.VIDEORESIZE:
                    d = pygame.Surface.get_size(self.screen)
                    if d[0] < 1150:
                        d= (1150, d[1])
                    if d[1] < 700:
                        d =(d[0], 700)
                    screen = pygame.display.set_mode((d),pygame.RESIZABLE)
                    self.p1.set_game(self.fleche)
                    self.p1.set_button(self.joueur)
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE: # si l'on lache la touche "-" du keypad
                        exit()
                    elif event.key == pygame.K_r: # si l'on lache la touche "+ " du keypad
                        restart = True
                        regame = True
                elif event.type == pygame.MOUSEBUTTONUP:    #si l'on lache un bouton de la souris
                    pose = self.get_position()
                    if pose == "re":
                        restart = True;
                        self.regame()
            pygame.display.flip()
        if regame == True:
            self.regame()

    def regame(self): # refait le jeu
        l,L = 5,6
        jeu = game(screen, [[-1 for i in range(L+1)] for j in range(l+1)],0,0)
        jeu.set_timer()
        jeu.loop()

class model:
    
    def __init__(self, grille, fleche):
        self.grille = grille
        self.fleche = fleche

    def place_model_fleche(self, direction): #le model de la position de la fleche
        if direction =="+":
            self.fleche += 1
        elif direction =="-":
            self.fleche -=1
        return (self.fleche %7)

    def place_model_token(self,j,size, joueur): #le model de la grille de puissance 4
        for i in range(5,-1,-1):
            if self.grille[i][j] == -1:
                self.grille[i][j] = joueur
                p1 = grid(size, self.grille)
                p1.animation_token(i,j,joueur)
                return (True,i,j)
        return (False,None,None)
    

class grid:     #2D

    def __init__(self, screen, grille):
        self.screen = screen
        self.grille = grille

    def set_game(self,pos_fleche):
        screen.fill((150,150,150))
        self.grid_create()
        self.fleche_create(pos_fleche)
        pygame.display.flip()

    def grid_create(self): #créer ou recréer la grille selon la taille de la surface

        actual_size = pygame.Surface.get_size(self.screen)

        pygame.draw.rect(self.screen, (0,0,255), pygame.Rect(0,0,actual_size[0]-300, actual_size[1]))
        actual_size, actual_size_x , radius = surface_size(self.screen)

        circle_size = [(actual_size[0]/7)/2,actual_size_x]
        circle_size[0]+=0

        for l in range(0,7):
            for k in range(0,6):
                switch_color = pattern_color[self.grille[k][l]]
                pygame.draw.circle(screen,(switch_color), circle_size,radius)
                circle_size[1]+= ((actual_size[1]-80)/6)
            circle_size[1] = actual_size_x
            circle_size[0] += actual_size[0]/7
        circle_size = [(actual_size[0]/7)/2,actual_size_x]
        pygame.display.flip()

    def fleche_create(self,pos_fleche): #créer ou recréer la "fleche" selon la taille de la surface
        actual_size, actual_size_x , radius = surface_size(self.screen)
        self.grid_create()
        pygame.draw.circle(screen,(0,255,0),[(actual_size[0]/7)/2+pos_fleche*(actual_size[0]/7),(actual_size_x+6*((actual_size[1]-80)/6))-25], 15)
        pygame.display.flip()

    def set_button(self,joueur):
        self.separate_button()
        self.button()
        self.blit_text()
        self.tracker_player(joueur)
        pygame.display.flip()

    def separate_button(self):
        actual_size, actual_size_x , radius = surface_size(self.screen)
        pos = 0
        for i in range(0,3):
            pos += actual_size[1]/4
            pygame.draw.line(self.screen, (255,0,0,50), (actual_size[0],pos), (300+actual_size[0],pos), 2)

    def button(self):
        actual_size, actual_size_x , radius = surface_size(self.screen)
        a=0
        for i in range(0,2):
            pygame.draw.rect(self.screen,(0,0,255,0), pygame.Rect(actual_size[0] + 50, a+25, 200,(actual_size[1]/4)-50),1)
            a+=actual_size[1]/4

    def blit_text(self):
        actual_size, actual_size_x , radius = surface_size(self.screen)

        police = pygame.font.SysFont("arial",40)
        text = police.render("HELP",True, (150,0,150))
        self.screen.blit(text,(actual_size[0]+85,60))
        text = police.render("RESTART",True, (150,0,150))
        self.screen.blit(text,(actual_size[0]+85,60+actual_size[1]/4))

    def tracker_player(self,joueur):
        actual_size, actual_size_x , radius = surface_size(self.screen)
        if joueur == 0:
            switch_color = pattern_color[0] 
        else:
            switch_color  =pattern_color[1]
        pygame.draw.circle(self.screen,switch_color, [actual_size[0]+150,700], radius)

    def help_button(self):
        help_menu = pygame_menu.Menu(
        height=300,
        title='Commande de touches',
        width=800,
        )

        HELP = ' Touche "Q/D" ou "Left/Right" pour déplacer le jetons de gauche à droite et inversement\n Touche "R" pour recommencer le jeu\n Touche "BARRE ESPACE" pour placer le jeton\nAPPUIER SUR ECHAP POUR REPRENDRE LA PARTIE' 

        help_menu.add.label(HELP, align=pygame_menu.locals.ALIGN_LEFT, font_size=18)
        return help_menu

    def victoire_message(self,joueur):
        message_menu = pygame_menu.Menu(300,1000,"Félicitations au joueur "+str(joueur)+" Voulez-vous recommencez ?")

        Message = "\n \"R\" pour recommencer\n \"ESCAPE\" pour quitter" 

        message_menu.add.label(Message, align=pygame_menu.locals.ALIGN_LEFT, font_size=18)
        return message_menu

    def timer(self,minutes,add,secondes):
        actual_size, actual_size_x , radius = surface_size(self.screen)

        police = pygame.font.SysFont("arial",100)
        text = police.render(f"{minutes}:{add}{secondes}",True, (150,0,150))
        x=(actual_size[1]-(actual_size[1])/2)
        y=actual_size[1]/4
        pygame.draw.rect(screen,(150,150,150),pygame.Rect(actual_size[0],x+25,actual_size[0]+300,y-50))
        self.screen.blit(text,(actual_size[0]+85,40+y*2))

    def animation_token(self, i,j,joueur): # fonction pour aniamtion de chute d'un jetons
        actual_size, actual_size_x , radius = surface_size(self.screen)
        circle_size = [(actual_size[0]/7)/2,actual_size_x]
        case_color_x= circle_size[0] + j*(actual_size[0]/7)
        case_color_y = circle_size[1] + i*((actual_size[1]-80)/6)
        circle_color = (case_color_x,case_color_y)
        y_pos = circle_size[1]
        switch_color = pattern_color[joueur]
        while (case_color_x, y_pos) <= circle_color:
            pygame.draw.circle(self.screen, switch_color, (case_color_x, y_pos), radius)
            pygame.display.flip()
            pygame.time.delay(40)
            pygame.draw.circle(self.screen, (0,0,0), (case_color_x, y_pos), radius)
            y_pos += ((actual_size[1]-80)/6)
        pygame.display.flip()

def start_the_game():
    l,L = 5,6
    jeu = game(screen, [[-1 for i in range(L+1)] for j in range(l+1)],0,0)
    jeu.set_timer()
    
    jeu.loop()




#--------------------------------------------------------
#            Création du menus principale
#--------------------------------------------------------

main_menu = pygame_menu.Menu(
    height=350, 
    width=450, 
    title='Main Menu', 
    theme=pygame_menu.themes.THEME_DARK, 
    )

#clock = main_menu.add.clock(font_size=25, font_name=pygame_menu.font.FONT_DIGITAL)

main_menu.add_button('Start Play', 
    start_the_game, 
    font_name=pygame_menu.font.FONT_8BIT,
    font_size= 30,
    )

#--------------------------------------------------------
#            Création du menus: Select Difficulty
#--------------------------------------------------------
main_menu.add_selector('Select Difficulty :', [
('Easy', 1), 
('Medium', 2), 
('Hard', 3)
],         )#def set_difficulty



#-------------------------------------------------------
#                 Création du menus: HELP
#-------------------------------------------------------

help_menu = pygame_menu.Menu(
height=400,
title='Commande de touches',
width=900 )

main_menu.add_button('Help', 
    help_menu,
    font_name=pygame_menu.font.FONT_DIGITAL,
    font_size= 25)


HELP = ' Touche "Q/D" ou "Left/Right" pour déplacer le jetons de gauche à droite et inversement\n Touche "R" pour recommencer le jeu\n Touche "BARRE ESPACE" pour placer le jeton\n Touche "ÉCHAP/ESC" pour quitter le jeu et retourner au menu principale' 

help_menu.add_label(HELP, 
    align=pygame_menu.locals.ALIGN_LEFT, 
    font_size= 20,
    )

help_menu.add_button('Return to menu', 
    pygame_menu.events.BACK,
    font_name=pygame_menu.font.FONT_DIGITAL,
    font_size= 25)





#-----------------------------------------------
#             Création du menus: About
#-----------------------------------------------
ABOUT = ['Projet Python: Puissance 4\nAuteur: \nRATANE Dimitri \nMARIAN Hugo \nCHANE WAY Mathias \nSHAN Michel \n©2021 Université de la Réunion']

about_menu = pygame_menu.Menu(
height=360,
title='About',
width=450)

for i in ABOUT:
    about_menu.add.label(i, align=pygame_menu.locals.ALIGN_LEFT, font_size=18 )
    about_menu.add.button('Return to menu', pygame_menu.events.BACK, font_name=pygame_menu.font.FONT_DIGITAL, font_size=18)

main_menu.add_button('About', 
    about_menu,
    font_name=pygame_menu.font.FONT_DIGITAL,
    font_size= 25)


#-------------------------------------------------------
#             Création du menus: Ressources
#-------------------------------------------------------

about_url= pygame_menu.Menu(
    height=300,
    title='URL',
    width=600)

main_menu.add_button('Ressource',
    about_url,
    font_name=pygame_menu.font.FONT_DIGITAL,
    font_size= 25,
    )

about_url.add.url('https://pygame-menu.readthedocs.io/en/master/', 'pygame-menu documentation', font_size= 20)
about_url.add.button('Return to menu', pygame_menu.events.BACK, font_name=pygame_menu.font.FONT_DIGITAL,font_size=18)


#-----------------------------------------------------
#             Création du menus: Quit
#-----------------------------------------------------

main_menu.add_button('Quit', 
    pygame_menu.events.EXIT,
    font_name=pygame_menu.font.FONT_DIGITAL,
    font_size= 25)

main_menu.mainloop(screen) #affichage du Menu principale