import pygame


class Map:
    def __init__(self, game, file):
        self.data = []
        with open(file, 'rt') as f:
            for line in f:
                self.data.append(line)
        #On met les éléments d'un fichier dans une matrice de listes