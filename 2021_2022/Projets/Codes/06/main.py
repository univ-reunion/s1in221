import pygame
import random
import json
from leviathan import *
from player import *
from ennemi_spike import *
from button import *
from wall import *
from map import *
from camera import *
from portal import *
from ennemi_spike_2 import *
from doors import *
from tresor import *
from Skeleton import *
#from phénix import *


pygame.init()
RUNNING = True

'''Le fichier principal avec la game loop'''

class Game:
    def __init__(self):
        '''Fonction d'initialisation qui s'exécute seulement une fois au lancement du programme'''
        
        self.screen = pygame.display.set_mode((960, 540))
        pygame.display.set_caption("Æventure dans le donjon")
        self.playing = True
        self.clock = pygame.time.Clock()
        self.IN_MENU = True

        self.FONT = pygame.font.Font(None, 24)
        #Ici on a initialisé des éléments importants tels que la taille de la fenêtre, et des flags qui permettent de connaitre le game state.

        #Les groupes de Sprites (Un peu comme des listes mais optimisés pour ça)
        self.allsprites = pygame.sprite.Group()
        self.tests = pygame.sprite.Group()
        self.menus = pygame.sprite.Group()
        self.walls = pygame.sprite.Group()
        self.players = pygame.sprite.Group()
        self.ennemis = pygame.sprite.Group()
        self.targeted_ennemis = pygame.sprite.Group()
        self.tresors = pygame.sprite.Group()
        self.bullets = pygame.sprite.Group()

        #On crée des varibables qui servent dans tout le projet (ex: la vie ou des éléments de génération des pièces)
        self.current_floor = 0
        self.current_room = 0
        self.number_of_rooms = 0
        self.all_floor_rooms = []
        self.max_transition_time = 30
        self.transition_time = self.max_transition_time

        self.max_inv_time = 5
        self.inv_time = self.max_inv_time

        self.game_won = False

        #On charge les images qui vont être utilisées dans ce fichier
        self.menu_image_1 = pygame.image.load("./Sprites/UI/test_menu_1.png")
        self.menu_image_2 = pygame.image.load("./Sprites/UI/test_menu_2.png")
        self.titre = pygame.image.load("./Sprites/UI/Titre.png")
        self.win_screen = pygame.image.load("./Sprites/UI/win_screen.png")
        
        self.ranged_idle = pygame.image.load("./Sprites/Personnages/Ranged/Ranged_Idle.png")
        self.bg = pygame.image.load("./Sprites/UI/benis.png")

        #On charge les infos des personnages stockés dans des .json
        with open("./Character_data/ranged.json") as f:
            self.ranged_data = json.load(f)

        with open("./Character_data/cac.json") as f:
            self.cac_data = json.load(f)

        self.data = None

        #Création d'objets qui vont servir de plan des niveaux du jeu
        self.map1 = Map(self, "./Room_layouts/room1.txt")
        self.map2 = Map(self, "./Room_layouts/room2.txt")
        self.map3 = Map(self,"./Room_layouts/room3.txt")
        self.map4 = Map(self, "./Room_layouts/room4.txt")
        self.map5 = Map(self, './Room_layouts/room5.txt')
        
        self.map6 = Map(self, "./Room_layouts/test2.txt")

        #La vie du joueur
        self.current_health = 100

        self.ennemis_in_room = []

        #Dans ces listes on met les maps qui peuvent être des choix pour chaque niveau
        #Si on en met plusieurs dans la liste, celle sur laquelle le joueur jouera sera choisi aléatoirement

        self.floor_one_maps = [self.map1]
        self.floor_two_maps = [self.map2]
        self.floor_three_maps = [self.map3]
        self.floor_four_maps = [self.map4]
        self.floor_five_maps = [self.map5]

        self.floors = [self.floor_one_maps, self.floor_two_maps, self.floor_three_maps, self.floor_four_maps, self.floor_five_maps]
        self.levels = []

        #On choisit quelle map de chaque liste sera utilisée dans cette partie
        for i in self.floors:
            self.levels.append(i[random.randint(0, len(i)-1)])

        #On affiche le menu du jeu
        self.start_menu()

    def start_game(self, player, map, type):
        '''Fonction de lancement du gameplay, sert aussi à change de palier'''

        self.IN_MENU = False

        #Comme cette fonction sert quand on change de niveau, on doit supprimmer toutes les entités pour éviter les problèmes de FPS et bugs.
        for sprite in self.menus:
            sprite.kill()
        for sprite in self.allsprites:
            sprite.kill()
        #On charge la map càd on fait spawn des éléments dans le jeu selon un plan donné
        self.load_map(self.levels[self.current_floor], player, type)
        
        #Ces deux objets spawn tout le temps au même endroit dans chaque niveau
        #Il n'est donc pas nécessaire de les inclure dans les maps
        self.respawn_portal = Portal(self, -1000, -1000, self.levels[self.current_floor], player, type, True)
        self.phoenix = Phenix(self, -300, -300)
        
        #Comme on fait spawner le joueur après les ennemis, on ne peut pas leur donner leur cible dès leur création
        self.give_target()
        
        #On crée la camera qui donnera l'illusion de bouger dans le monde du jeu
        self.camera = Camera(self, 960, 540)


    def start_menu(self):
        '''Le menu du jeu'''

        #Les boutons qui permettent de choisir son personnage
        self.Button2 = Button(self, 416 - 32, 228 + 64, 32, 32, self.menu_image_1, self.start_game, [Player, self.map1, self.ranged_data])
        self.Button1 = Button(self, 512 - 32, 228 + 64, 32, 32, self.menu_image_2, self.start_game, [Player, self.map2, self.cac_data])

    def load_map(self, map, player, type):
        '''Fonction de création des niveaux'''
        
        self.all_floor_rooms = {}
        self.number_of_rooms = 0

        #On regarde chaque caractère de la map et selon si c'est un caractère associé à une classe, on fait spawn on objet à son emplacement de la map
        #On doit faire trois boucles qui font essentiellement la même chose pour éviter les problèmes dus à l'ordre de création des objets

        for row, tiles in enumerate(map.data):
            for col, tile in enumerate(tiles):
                #Boucle de création des murs et limites des pièces
                if tile == 'W':
                    wall = Wall(self, col*32, row*32, 32, 32, (0,0,255))
                if tile.isnumeric():
                    #Ce code permet de détecter si un caractère est un chiffre 
                    #Dans ce cas, il regardera si il y a déjà un élément dans la liste a cet index et sinon on en crééra un
                    #Cela permet d'avoir les coordonnées x et y des coins des pièces
                    if tile not in self.all_floor_rooms:
                        self.all_floor_rooms[tile] = []
                        self.all_floor_rooms[tile].append(col * 32)
                        self.all_floor_rooms[tile].append(row * 32)
                        self.ennemis_in_room.insert(int(tile), 0)

                    else:
                        self.all_floor_rooms[tile].append(col * 32)
                        self.all_floor_rooms[tile].append(row * 32)

        for row, tiles in enumerate(map.data):
                for col, tile in enumerate(tiles):
                    #Boucle de spawn de tous les ennemis et autres éléments
                    if tile == "E":
                        enn = Ennemi_spike(self, col*32, row*32, 32, 32)

                    if tile == "F":
                        enn = Ennemi_spike2(self, col*32, row*32, 32, 32)

                    if tile == "L":
                        enn = Leviathan(self, col*32, row*32, 32)

                    if tile == "S":
                        enn = Skeleton(self, col*32, row*32,32)

                    if tile == "X":
                        enn = Phenix(self, col*32, row*32,32)    

                    if tile == "D":
                        door = Doors(self, col*32, row*32, 32, 32, (0, 255, 0))

                    if tile == "O":
                        if self.current_floor < len(self.levels) - 1:
                            portal1 = Portal(self, col*32, row*32, self.levels[self.current_floor + 1], player, type, False)
                    
                    if tile == "T":
                        tre = Tresor(self, col*32, row*32, 32, 32)

        for row, tiles in enumerate(map.data):
                for col, tile in enumerate(tiles):            
                    #Boucle de création du joueur
                    if tile == 'P':
                        self.player = player(self, col, row, 32, 32, type)
    
    def give_target(self):
        #On donne le joueur comme cible aux ennemis 
        for enn in self.targeted_ennemis:
            enn.target = self.player

    def run(self):
        #La boucle principale du jeu
        if self.playing:
            #On limite le jeu a 60 fps pour éviter les différences entre les machines puissantes et celles qui le sont moins
            self.clock.tick(60) 
            #On update et affiche les éléments selon si on est dans un menu ou en jeu
            if not self.IN_MENU:
                self.update()
                self.draw()
            else:
                self.menu_update()
                self.menu_draw()
            self.events()

    def update(self):
        '''On exécute les fonctions qui doivent être exécutées à chaque frame du programme'''
        
        self.allsprites.update() #Exécute la fonction update de tous les sprites dans le groupe allsprites
        self.camera.update(self.player) #Centre la camera et le jeu sur le joueur 

        if self.current_health <= 0:  #Quand le personnage meurt il est renvoyé au premier niveau
            self.respawn_portal.rect.x = self.player.rect.x
            self.respawn_portal.rect.y = self.player.rect.y 
            self.current_health = 100

    def menu_update(self):
        #On update les menus
        self.menus.update()

    def screen_transition(self):
        if self.transition_time >= 0:
            self.screen.fill((0, 0, 0))
            self.transition_time -= 1

    def draw(self):
        self.screen.fill((0,0,0)) #On remplit l'écran de noir pour effacer ce qui était affiché à la frame d'avant
        self.screen.blit(self.bg, (0, 0)) #On affiche l'image de fond
        #On affiche tous les autres sprites
        for sprite in self.allsprites:
            self.screen.blit(sprite.image, self.camera.apply(sprite))
        self.draw_stats() 
        #Affiche l'écran de victoire si on gagne
        if self.game_won:
            self.screen.blit(self.win_screen, (0, 0))
        pygame.display.flip()

    def menu_draw(self):
        #Même chose que au dessus mais pour les ennemis
        self.screen.fill((0,0,0))
        self.screen.blit(self.titre, (20, 20))

        for sprite in self.menus:
            self.screen.blit(sprite.image, (sprite.x, sprite.y))
        
        pygame.display.flip()

    def draw_stats(self):
        '''Affiche des informations en haut de l'écran telles que la vie, la pièce actuelle, le nombre d'ennemis'''
        
        room = self.FONT.render(f"Current room: {self.current_room}", True, (255, 255, 255))
        num_enn = self.FONT.render(f'Ennemis: {self.ennemis_in_room[self.current_room]}', True, (255, 255, 255))
        fps = self.FONT.render("FPS:" + str(self.clock.get_fps()), True, (255, 255, 255))

        #La vie change de couleure selon si le joueur est en bonne santée ou pas
        if self.current_health <= 100:
            health = self.FONT.render(f"Vie: {self.current_health} %", True, (0,255,0))
        if self.current_health <= 75:
            health = self.FONT.render(f"Vie: {self.current_health} %", True, (173,255,47))
        if self.current_health <= 50:
            health = self.FONT.render(f"Vie: {self.current_health} %", True, (255,255,0))
        if self.current_health <= 20:
            health = self.FONT.render(f"Vie: {self.current_health} %", True, (255, 0, 0))

        

        self.screen.blit(room, (15, 15))
        self.screen.blit(health, (450, 15))
        self.screen.blit(fps, (15, 35))
        self.screen.blit(num_enn, (15, 55))

    def events(self):
        for event in pygame.event.get():
            #Si le joueur clique sur la croix rouge le jeu se ferme
            if event.type == pygame.QUIT:
                self.playing = False
                pygame.quit()
            

game = Game()

while RUNNING:
    game.run()
