import pygame


class Camera:
    def __init__(self, game, width, height):
        self.camera = pygame.Rect(0, 0, width, height)
        self.width = width
        self.height = height
        self.game = game
        self.X = 0
        self.Y = 0
    
    def apply(self, entity):
        return entity.rect.move(self.camera.topleft)
        #On bouge un sprite d'un offset pour donner l'illusion que la camera suit le joueur

    def update(self, target):
        #On positionne la camera pour qu'elle soit bien alignée quand on décale des objets
        x =  - target.rect.x + int(960/2)
        y = - target.rect.y + int(540/2)
        
        self.X = x
        self.Y = y
        
        self.camera = pygame.Rect(x, y, self.width, self.height)