import pygame
import math



class Leviathan (pygame.sprite.Sprite):
    def __init__(self,game,x,y,player):
        self.groups = game.allsprites, game.ennemis, game.targeted_ennemis
        pygame.sprite.Sprite.__init__(self,self.groups)
         #Ici je veux un monstre un peu plus original et je respecte les multiples de 32 afin que rien ne crashe
        image = pygame.image.load("./Sprites/Ennemis/Leviathanbg.png")
        self.image = image
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y 
        self.rect.x = x
        self.rect.y = y

        self.speed = 2
    
        self.target = None
       
        self.game = game
        
        #On donne au monstre un rayon dans lequel il va attaquer le joueur
        #Si le joueur sort de ce rayon, l'ennemi retournera là où il était stationné
        self.spawn = self.rect.center
        self.Angery_time = 60
        self.Angery_radius = 256

        self.room = 0


        for i in range (0, len(self.game.all_floor_rooms)):
            if self.x >= self.game.all_floor_rooms[str(i)][0] and self.x <= self.game.all_floor_rooms[str(i)][2]:
                if self.y >= self.game.all_floor_rooms[str(i)][1] and self.y <= self.game.all_floor_rooms[str(i)][3]:
                    self.room = int(i)
                    self.game.ennemis_in_room[self.room] += 1

    def update (self):
        if self.target != None:
            #Comme le joueur est créé après il faut vérifier qu'on a bien une cible

            distance_x = 0
            distance_y = 0
            distance = 0
            
            if self.spawn[0] < self.target.rect.center[0]:
                distance_x = self.target.rect.center[0] - self.spawn[0] 
            else:
                distance_x = self.spawn[0] - self.target.rect.center[0] 

            if self.spawn[1] < self.target.rect.center[1]:
                distance_y = self.target.rect.center[1] - self.spawn[1] 
            else:
                distance_y = self.spawn[1] - self.target.rect.center[1] 
            
            distance = math.sqrt(distance_x**2 + distance_y**2)
            #On utilise pythagore pour déterminer la distance entre le joueur et l'ennemi

            if  self.Angery_time <= 0 and self.game.current_room == self.room:

                    #Si le joueur est dans la pièce et dans le rayon d'attaque, l'ennemi se dirige vers lui
                
                    if distance <= self.Angery_radius:
                        if self.target.rect.center[0] > self.rect.center[0]:
                            self.rect.x += self.speed
                        if self.target.rect.center[0] < self.rect.center[0]:
                            self.rect.x -= self.speed

                        if self.target.rect.center[1] > self.rect.center[1]:
                            self.rect.y += self.speed
                        if self.target.rect.center[1] < self.rect.center[1]:
                            self.rect.y -= self.speed
                    
                    #Si le joueur n'est pas dans le rayon d'attaque, l'ennemi retourne où il était
                    else:
                        if self.spawn[0] > self.rect.center[0]:
                            self.rect.x += self.speed
                        if self.spawn[0] < self.rect.center[0]:
                            self.rect.x -= self.speed

                        if self.spawn[1] > self.rect.center[1]:
                            self.rect.y += self.speed
                        if self.spawn[1] < self.rect.center[1]:
                            self.rect.y -= self.speed
            else:
                self.Angery_time -= 1
    

            


            