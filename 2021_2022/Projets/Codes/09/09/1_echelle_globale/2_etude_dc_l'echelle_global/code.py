import csv

                                            #définition de fonctions
def file_to_list(filename):
#permet de mettre le contenu d'un fichier csv dans une liste de dictionnaire
    with open(filename, 'r', encoding='utf-8') as csvfile:
        return list(csv.DictReader(csvfile, delimiter=';'))


def vide_en_0(liste):
#remplace les espaces vides en 0 dans la liste de dictionnairepris en paramètre
    for dico in liste:
        for clé in dico:
            if dico[clé]=='':
                dico[clé]='0'


def valeur_dico_mois(liste,annee,mois,clé):
#prend une date (année et mois) ainsi qu'une clé et retourne la liste des valeurs, associé à la clé, de chaque jours du mois choisi
    l_valeur=[]
    for dico in liste:
        if int(dico['date'][0:4])==annee and int(dico['date'][5:7])==mois:
            l_valeur.append(int(dico[clé]))
    return l_valeur


def tot_mois(clé) :
# prend une clé et retourne une liste des totaux à chaque mois associé à la clé
    l_tot_mois=[]
    for mois in range(1,13):
        if valeur_dico_mois(l,2020,mois,clé)!=[]:
            l_tot_mois.append(valeur_dico_mois(l,2020,mois,clé)[0])
    for mois in range(0,13):
            if valeur_dico_mois(l,2021,mois,clé)!=[]:
                l_tot_mois.append(valeur_dico_mois(l,2021,mois,clé)[0])
    return l_tot_mois


def moyenne_mois(liste,clé):
# même fonction que celle ci-dessus sauf qu'elle ne prend pas des totaux
    l_tot_mois=[]
    for mois in range(1,13):
        if valeur_dico_mois(liste,2020,mois,clé)!=[]:
            l_tot_mois.append(sum(valeur_dico_mois(liste,2020,mois,clé)))
    for mois in range(1,13):
            if valeur_dico_mois(liste,2021,mois,clé)!=[]:
                l_tot_mois.append(sum(valeur_dico_mois(liste,2021,mois,clé)))
    return l_tot_mois


def dico_en_l_dico(l_clé,liste_l_v):
#prend en argument une liste de clés ainsi qu'une liste de liste de valeur et retourne une liste de dictionnaire 
    tab_dico=[]
    for num_dico in range(len(liste_l_v[0])):
        tab_dico.append({})
        for num_clé in range(len(liste_l_v)):
            tab_dico[num_dico][l_clé[num_clé]]=liste_l_v[num_clé][num_dico]
    return tab_dico
                         
def l_dico_to_csv(label,tab_dico,nom_fichier):
#crée un fichier csv nommé de nom_fichier à partir d'une liste de mots clé en en-tete du fichier et d'une liste de dictionnaire 
    with open(nom_fichier, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=label)
        writer.writeheader()
        for valeur in tab_dico:
            writer.writerow(valeur)
            


                                            #code
            
l_pratique=['2020-3', '2020-4', '2020-5', '2020-6', '2020-7', '2020-8', '2020-9','2020-10', '2020-11', '2020-12','2021-1','2021-2', '2021-3', '2021-4','2021-5']
l=file_to_list('data01.csv')
vide_en_0(l)
j=dico_en_l_dico(['date','moy_mois','tot_mois'],[l_pratique,moyenne_mois(l,'dc_par_jour'),tot_mois('tot_dc')])
l_dico_to_csv(['date', 'moy_mois', 'tot_mois'],j,'nouveau_dico.csv')
   
