import csv
import pandas
                                            #définition de fonctions
def file_to_list(filename):
    with open(filename, 'r', encoding='utf-8') as csvfile:
        return list(csv.DictReader(csvfile, delimiter=','))
 

def delim_dep(liste_dico,dep,valeur):
    l_dep=[]
    for dico in liste_dico:  
        if dico['dep']==dep:   
            l_dep.append(int(dico[valeur]))
    return l_dep


def liste_en_dico(liste_k,liste_v):
    dico={}
    for i in range(len(liste_k)):
        dico[liste_k[i]]=liste_v[i]
    return dico


def liste_lib_dep(liste_dico,l_dep,valeur):
    l_nom_dep=[]
    for dep in l_dep:
        for dico in liste_dico:
            if dico['date']=='2021-04-11' and dico['dep']==dep:
                    l_nom_dep.append(dico[valeur])
    return l_nom_dep

def max_valeur(liste_dico,liste_dep,valeur):
    l_max=[]
    for dep in liste_dep:
        l_max.append(max(delim_dep(liste_dico,dep,valeur)))
    return l_max

def classement_dico(dictionnaire,liste):
    dico_classé={} 
    for dico in liste:
        for clé in dictionnaire.keys():
            if dico==dictionnaire[clé]:
                dico_classé[clé]=dico
    return dico_classé


def classement_dep(dico):
    nouvelle_l_dep=[]
    for i in dico.keys():
        nouvelle_l_dep.append(i)
    return nouvelle_l_dep


def dico_en_l_dico(l_clé,liste_l_v):
    tab_dico=[]
    for num_dico in range(len(liste_l_v[0])):
        tab_dico.append({})
        for num_clé in range(len(liste_l_v)):
            tab_dico[num_dico][l_clé[num_clé]]=liste_l_v[num_clé][num_dico]
    return tab_dico


def fonctions_regroupées(liste_dico,valeur):  
    l_max_valeur=max_valeur(liste_dico,l_dep,valeur)
    dico_dep=liste_en_dico(l_dep,l_max_valeur)
    l_max_valeur.sort(reverse=True)
    dico_classé=classement_dico(dico_dep,l_max_valeur)
    l_dep_classé=classement_dep(dico_classé)
    nom_dep_classé=liste_lib_dep(liste_dico,l_dep_classé,'lib_dep')
    return dico_en_l_dico(['dep','lib_dep',valeur],[l_dep_classé,nom_dep_classé,l_max_valeur])

def affichage(l_dico,nb_lignes,rang):
    return pandas.DataFrame(data=[i for i in l_dico[rang:(rang+nb_lignes)]],index=[0+i for i in range(rang,(rang+nb_lignes))])

                                    
                                                #code
l_dep=[]
with open('liste_num_dep.txt','r',encoding='utf-8') as fich:
    for ligne in fich:
        l_dep.append(ligne[:-1])
l=file_to_list("nv_tables.csv")
top10_dchosp=affichage(fonctions_regroupées(l,'dchosp'),10,len(fonctions_regroupées(l,'dchosp'))-10)
#top10_rea=affichage(fonctions_regroupées(l,'rea'),10)
#top10_hosp=affichage(fonctions_regroupées(l,'hosp'),10)
print(top10_dchosp,end='\n\n\n')
print(top10_rea,end='\n\n\n')
print(top10_hosp)