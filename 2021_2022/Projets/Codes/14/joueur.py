import pygame
import random


class joueur: #creation class joueur
	def __init__(self):
		self.ind = 0 #variable pour la fonction rotate
		self.images = [pygame.image.load("images/perso.png").convert_alpha(), #creation liste pour les differentes images du perso
						pygame.image.load("images/perso_droite.png").convert_alpha(), #+ conversion en un format plus leger
						pygame.image.load("images/perso_bas.png").convert_alpha(),
						pygame.image.load("images/perso_gauche.png").convert_alpha()]
		self.image = pygame.transform.scale(self.images[self.ind], (40,40)) #variable pour l'image du perso affichee a l'ecrant + redimension
		self.rect = self.image.get_rect() #recuperation du rectangle du perso
		self.rect.x = 100 #position perso sur axe x
		self.rect.y = 300 #position perso sur axe y
		self.saut = False #varaible pour le saut
		self.vel_y = 10 #pour gerer la hauteur du saut
		
	def jump(self): #fonction saut
		if self.saut: 
			self.rect.y -= self.vel_y
			self.vel_y -= 0.1
		if self.vel_y <= 0:
			self.vel_y = 10
			self.saut = False

	def rotate(self): #fonction rotation du perso
		if self.ind != 3:
			self.ind += 1
		else:
			self.ind = 0
		self.image = pygame.transform.scale(self.images[self.ind],(40,40))

class caisse: #creation de la class caisse(plateforme)
	def __init__(self, x):
		self.x = x
		self.image = pygame.transform.scale(pygame.image.load("images/caisse.png").convert_alpha(),(1000,50))
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = 460
		self.vitesse = 4

	def move_caisse(self): #methode pour deplacer les caisses
		self.rect.x -= self.vitesse
		if self.rect.x < -1000:
			self.rect.x=900
			self.caisseAlea()

	def init_caisse(self): #initialise la position des caisses
		self.rect.x = self.x
		self.rect.y = 460

	def caisseAlea(self): #choisit une position pre defini dans la liste choix pour le positionnement sur l'axe y
		self.choix = [460, 334, 360, 310]
		self.rect.y = random.choice(self.choix)

class obstacle(): #class de l'obstacle
	def __init__(self):
		self.image = pygame.transform.scale(pygame.image.load("images/o1.png"), (50,50)).convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.x = 920
		self.rect.y = 410
		self.vitesse = 4

	def move_obstacle(self): #methode pour deplacer l'obstacle
		self.rect.x-= self.vitesse
		if self.rect.x < - 50:
			self.rect.x=1000