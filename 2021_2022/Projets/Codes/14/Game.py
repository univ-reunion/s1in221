import pygame, random, sys
from joueur import joueur, caisse, obstacle
 

class game: #creation class game
	def __init__(self):
		self.screen = pygame.display.set_mode((900, 500)) #creation fenetre de jeu
		self.grav = 5 #variable pour la "quantite" de gravite applique au joueur
		self.joueur = joueur() #initialisation class joueur
		self.caisse1 = caisse(0) #initialisation class caisse
		self.caisse2 = caisse(1200) #idem
		self.obstacle = obstacle() #idem pour class obstacle	
		self.bg = pygame.image.load("images/bg.jpeg").convert_alpha() #chargement du bg
		self.bg2 = pygame.image.load("images/bg.jpeg").convert_alpha() #chargement du 2eme bg
		self.gameover = pygame.image.load("images/gameover.png").convert_alpha() #chargement image game-over
		self.x=0 #variable pour gerer le deplacement du bg
		self.tomber = False #variable pour gerer le passage a travers la plateforme en utilisant la fleche du bas
		self.jouer = 0 #variable pour gerer le game-over
		self.quitter = False #variable pour quitter le jeu (si le joueur clique() sur la croix rouge)
		self.r_joueur = self.joueur.rect #variable pour la detection des collisions
		self.r_caisse1 = self.caisse1.rect #variable pour la detection des collisions
		self.r_caisse2 = self.caisse2.rect #variable pour la detection des collisions
		self.r_obstacle = self.obstacle.rect #variable pour la detection des collisions
		self.score = 0 #initialise le score
		self.compteur = pygame.USEREVENT #creation d'un evenement pour le score
		pygame.time.set_timer(self.compteur, 100) #execute l'evenement toutes les 100 milisecondes
		self.pauseimg = pygame.image.load("images/pause.png").convert_alpha() #chargement de l'image pe pause

	def init_game(self):
		self.screen.blit(self.bg, (self.x,0)) #ajout du bg a l'ecrant
		self.screen.blit(self.bg2, (self.x+1920,0)) #ajout du bg a l'ecrant
		self.screen.blit(self.joueur.image, self.joueur.rect) #ajout du joueur a l'ecrant
		self.event()

	def accueil(self):
		font = pygame.font.SysFont("monospace", 20)
		accueil_text = font.render("Appuyez sur espace pour jouer", 1,(0,0,0))
		titre = pygame.font.SysFont("Raleway", 80, bold=True).render("RUNNING ESCAPE", 1, (0,0,0))
		self.screen.blit(accueil_text, (280,200))
		self.screen.blit(titre, (160, 100))
		self.screen.blit(self.caisse1.image, self.caisse1.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(self.caisse2.image, self.caisse2.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(self.obstacle.image, self.obstacle.rect) #ajout de l'obstacle a l'ecrant
		self.caisse1.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.caisse2.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.event()

		if self.x == self.bg.get_width() * -1: #condition pour le bg infini
			self.x=0
		self.x-=2

		if self.r_caisse1.colliderect(self.r_caisse2): #condition pour eviter que deux caisses se chevauchent
			self.caisse1.caisseAlea() #fonction pour deplacer aleatoirement une caisse sur l'axe y
			self.caisse1.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
			self.caisse2.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
			

		if self.joueur.rect.y > 500:
			self.joueur.rect.y = 0

		if self.caisse1.rect.x <= -750 or self.caisse2.rect.x <= -750:
			self.joueur.saut = True
			self.joueur.jump()

		if self.r_joueur.colliderect(self.r_caisse1): #si joueur collision avec plateforme
			self.joueur.rect.bottom = self.caisse1.rect.top #le joueur reste sur le haut de la platform
			if self.tomber and self.joueur.rect.y < 420: #si le joueur appuit sur fleche du bas
				self.joueur.rect.top = self.caisse1.rect.bottom #le perso passe a travers la plateforme
				self.tomber = False 
			self.tomber = False
			self.joueur.saut = False

		if self.r_joueur.colliderect(self.r_caisse2): #si joueur collision avec plateforme
			self.joueur.rect.bottom = self.caisse2.rect.top #le joueur reste sur le haut de la platform
			if self.tomber and self.joueur.rect.y < 420: #si le joueur appuit sur fleche du bas et qu'il n'est pas sur la plateforme du bas
				self.joueur.rect.top = self.caisse2.rect.bottom #le perso passe a travers la plateforme
				self.tomber = False
			self.tomber = False
			self.joueur.saut = False

	def event(self):
		for event in pygame.event.get(): #boucle des evenements (touches pressees)
			if event.type == pygame.QUIT: #si le joueur clique sur la croix rouge
				self.quitter = True #on sort de la boucle de jeu
				sys.exit() #et la fenetre se ferme
			if event.type == pygame.KEYDOWN: #si l'event = a touche pressee
				if event.key == pygame.K_UP and self.joueur.saut == False: #si la touche pressee = fleche du haut
					self.joueur.saut = True #actualisation de la variable du saut dans la class du joueur
					self.joueur.rotate() #appel de la fonction rotate() de la class joueur pour effectuer une rotation de 90 degres du perso
					self.jouer = 1
					
				if event.key == pygame.K_SPACE and self.joueur.saut == False: #si la touche pressee = barre d'espace
					self.joueur.saut = True #actualisation de la variable du saut dans la class du joueur
					self.joueur.rotate()#appel de la fonction rotate() de la class joueur pour effectuer une rotation de 90 degres du perso
					self.jouer = 1

				if event.key == pygame.K_DOWN and self.joueur.saut == False: #sila touche pressee = fleche du bas
						self.tomber = True #actualisation de la variable tomber

				if event.key == pygame.K_p:
						self.jouer = 3
				
			if event.type == self.compteur: 
				if self.jouer == 1: #arrete le score en cas de game-over
					self.score += 1

	def partie_en_cours(self):
		self.caisse1.move_caisse() #appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.screen.blit(self.caisse1.image, self.caisse1.rect) #ajout de la plateforme a l'ecrant
		self.caisse2.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.screen.blit(self.caisse2.image, self.caisse2.rect) #ajout de la plateforme a l'ecrant
		font = pygame.font.SysFont("monospace", 20)
		score_text = font.render(f"Score : {self.score}", 1, (0,0,0))
		self.screen.blit(score_text, (20,20))

		if self.joueur.saut: #si le joueur appuit sur espace ou fleche du haut
			self.joueur.jump() #appel de la fonction saut de la class joueur pour sauter

		if self.r_caisse1.colliderect(self.r_caisse2): #condition pour eviter que deux caisses se chevauchent
			self.caisse1.caisseAlea() #fonction pour deplacer aleatoirement une caisse sur l'axe y

		if self.x == self.bg.get_width() * -1: #condition pour le bg infini
			self.x=0
		self.x-=2

		if self.obstacle.rect.x >= 900 :
			self.obstacle.rect.y += 5
			if self.obstacle.rect.y >= 500:
				self.obstacle.rect.y = 0

			if self.r_obstacle.colliderect(self.r_caisse1):
				self.obstacle.rect.y = self.caisse1.rect.y - 50
				self.obstacle.move_obstacle()

			if self.r_obstacle.colliderect(self.r_caisse2):
				self.obstacle.rect.y = self.caisse2.rect.y - 50
				self.obstacle.move_obstacle()
		else:
			self.screen.blit(self.obstacle.image, self.obstacle.rect)
			self.obstacle.move_obstacle()

		if self.r_joueur.colliderect(self.r_caisse1): #si joueur collision avec plateforme
			self.joueur.rect.bottom = self.caisse1.rect.top #le joueur reste sur le haut de la platform
			if self.tomber and self.joueur.rect.y < 420: #si le joueur appuit sur fleche du bas
				self.joueur.rect.top = self.caisse1.rect.bottom #le perso passe a travers la plateforme
				self.tomber = False 
			self.tomber = False
			self.saut = False

		if self.r_joueur.colliderect(self.r_caisse2): #si joueur collision avec plateforme
			self.joueur.rect.bottom = self.caisse2.rect.top #le joueur reste sur le haut de la platform
			if self.tomber and self.joueur.rect.y < 420: #si le joueur appuit sur fleche du bas et qu'il n'est pas sur la plateforme du bas
				self.joueur.rect.top = self.caisse2.rect.bottom #le perso passe a travers la plateforme
				self.tomber = False
			self.tomber = False
			self.saut = False

		if self.r_joueur.colliderect(self.r_obstacle): #si joueur collision avec obstacle
			self.saut = False
			self.jouer = 2 #game-over
			
		
		if self.joueur.rect.y >= 510: #si le joueur tombe de la plateforme
			self.jouer = 2 #game-over

		if self.score%401 == 400:
			self.obstacle.vitesse += 0.1
			self.caisse1.vitesse += 0.1
			self.caisse2.vitesse += 0.1

	def game_over(self):
		self.screen.blit(self.caisse1.image, self.caisse1.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(self.caisse2.image, self.caisse2.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(pygame.transform.scale(self.gameover, (388,235)), (250,150)) #affiche game-over a l'ecrant
		font = pygame.font.SysFont("monospace", 20, bold=True) #choix de la police pour le score
		score_text = font.render(f"Ton score est de {self.score} points", 1, (255,255,255)) 
		play_again = font.render("[Espace] pour recommencer !", 1, (255,255,255))
		self.screen.blit(score_text, (285,150)) #affiche le score sur l'écran game-over
		self.screen.blit(play_again, (280, 360))
		self.screen.blit(self.joueur.image,self.joueur.rect) #ajout du joueur a l'ecrant

		if self.r_caisse1.colliderect(self.r_caisse2): #condition pour eviter que deux caisses se chevauchent
			self.caisse1.caisseAlea() #fonction pour deplacer aleatoirement une caisse sur l'axe y
		self.caisse1.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.caisse2.move_caisse()#appel de la fonction move_caisse de la class caisse pour deplacer la plateforme vers la gauche
		self.obstacle.vitesse = 4
		self.caisse1.vitesse = 4
		self.caisse2.vitesse = 4
		
		if self.joueur.saut == True: #si le joueur appuit sur espace ou fleche du haut
			self.joueur.saut = False
			self.jouer = 1 #re lance le jeu
			self.joueur.rect.y = 100 #reinitialise la posiiton du joueur
			self.caisse1.init_caisse() #reinitialise la position de la plateform
			self.caisse2.init_caisse() #idem
			self.score = 0 #reinitilalise la valeur du score

	def pause(self):
		self.event()
		self.joueur.saut = False
		font = pygame.font.SysFont("monospace", 20)
		score_text = font.render(f"Score : {self.score}", 1, (0,0,0))
		pause_text = font.render(f"Appuye sur espace pour reprendre", 1, (0,0,0))
		self.screen.blit(self.caisse1.image, self.caisse1.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(self.caisse2.image, self.caisse2.rect) #ajout de la plateforme a l'ecrant
		self.screen.blit(self.obstacle.image, self.obstacle.rect) #ajout de l'obstacle a l'ecrant
		self.screen.blit(pygame.transform.scale(self.pauseimg, (290,250)), (290,150))
		self.screen.blit(score_text, (20,20))
		self.screen.blit(pause_text, (240,100))

	def gravite(self): #fonction pour appliquer la gravite au joueur
		self.joueur.rect.y += self.grav