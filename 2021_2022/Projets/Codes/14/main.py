import pygame,random,sys
from Game import game

accueil = 0
jouer = 1
game_over = 2
pause = 3

pygame.init() #initialisation pygame
pygame.display.set_caption('Running Escape') #nom de la fenetre de jeu

game = game() #initialisation class game
clock = pygame.time.Clock() #creation d'une clock stocke dans la variable clock

while not game.quitter: #boucle de jeu
	game.init_game() #affiche les differents elements a l'ecrant

	if game.jouer == jouer: #si le jeu est en cours
		game.partie_en_cours()
		game.gravite() #aplique la gravite sur le joueur


	elif game.jouer == game_over: #si game-over
		game.game_over()
		game.gravite() #aplique la gravite sur le joueur

	elif game.jouer == pause:
		game.pause()

	elif game.jouer == accueil:
		print(game.joueur.saut)
		game.gravite()
		game.accueil()
				
	
	
	pygame.display.update() #actualise l'ecrant
	clock.tick(120) #fps