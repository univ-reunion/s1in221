from tkinter.ttk import *
from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Ceci est le titre de la fenetre principal')
root.geometry("1334x768")

Frame1 = Frame(root, borderwidth=10, relief=GROOVE)
Frame1.place(width = 1000, height = 500, x= 150, y = 0)
Label(Frame1, text ="Bienvenue sur votre logiciel de suivi de santé.", font = 50, fg = 'red').pack()
Label(Frame1, text ="Ce logiciel permet de faire un relevé de différentes données personnelles sur votre santé:",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre poids et IMC (Indice de Masse Corporelle)",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Vos informations nutritives",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre taux de glycémie",fg = 'blue', font = 20).pack()

root.mainloop()