from tkinter.ttk import *
from tkinter import *
from tkinter import messagebox
import csv
import datetime
import os
import ast
from datetime import date

#Fonction needed

# création variable globale contenant la date du jour d'exécution du programme en format yyyy-mm-jj
jour_brut = date.today()
heure_brut = datetime.time()
jour = jour_brut.isoformat()
heure = heure_brut.strftime("%H-%M-%S")
def calcul_IMC(poids, taille):
# """Fonction prenant en argument le poids en float ou entier, la taille en entier, la sauvegarde de l'imc du profil
# utilisateur, calcule l'imc du jour, le stocke dans la sauvegarde et le retourne"""
    return round(poids / (taille / 100) ** 2, 2)
 
def save_exist():
    """Fonction créant un fichier de sauvegarde si non existant"""
    if not os.path.isfile("users_save.csv"):
        with open("users_save.csv", 'a', newline='') as file:
            champs = ["prenom", "nom", "naissance", "sexe", "nutrition", "nutriscore", "poids", "taille", "imc",
                      "date_modif"]
            writer = csv.DictWriter(file, fieldnames=champs, delimiter = ';')
            writer.writeheader() 
def add_user(self):
    """Fonction ajoutant un nouvel utilisateur s'il n'existait pas, et prépare les différents éléments"""
    # création de dictionnaires pour chaque caractéristiques à sauvegarder, avec en clé une date et en valeurs les
    # valeurs correspondantes
    poids_u = {}
    taille_u = {}
    imc_u = {}
    nutrition_u = {}
    val = {}
    for i in liste_nutriments:
        val[i] = 0
    nutrition_u[jour] = val
    nutriscore_u = {}
    poids_u[jour] = self.poids
    taille_u[jour] = self.taille
    self.poids = poids_u
    self.taille = taille_u
    self.nutriscore = nutriscore_u
    self.nutrition = nutrition_u
    save_exist()
    new_user = {"prenom": self.prenom, "nom": self.nom, "naissance": self.naissance, "sexe": self.sexe,
                "nutrition": self.nutrition, "nutriscore": self.nutriscore, "poids": self.poids,
                "taille": self.taille, "imc": self.imc, "date_modif": jour}

    with open("users_save.csv", 'a', newline='') as file:
        champs = ["prenom", "nom", "naissance", "sexe", "nutrition", "nutriscore", "poids", "taille", "imc",
                  "date_modif"]
        writer = csv.DictWriter(file, fieldnames=champs, delimiter = ';')
        writer.writerow(new_user)

def save(self):
    # """Fonction sauvegardant les modifications apportées au cours de l'utilisation du logiciel"""
    sans_modif = []
    with open("users_save.csv", "r") as file:
        for i in list(csv.DictReader(file, delimiter=',')):
            if i["prenom"] != self.prenom and i["nom"] != self.nom:
                sans_modif.append(i)
    with open("users_save.csv", "w") as file:
        champs = ["prenom", "nom", "naissance", "sexe", "nutrition", "nutriscore", "poids", "taille", "imc","date_modif"]
        writer = csv.DictWriter(file, fieldnames=champs)
        writer.writeheader()
        for i in sans_modif:
            writer.writerow(i)
        writer.writerow({"prenom": self.prenom, "nom": self.nom, "naissance": self.naissance, "sexe": self.sexe,
                         "nutrition": self.nutrition, "nutriscore": self.nutriscore, "poids": self.poids,
                         "taille": self.taille, "imc": self.imc, "date_modif": jour})

liste_nutriments = ['energy-kj_u', 'energy-kcal_u', 'energy_u', 'fat_u', 'saturated-fat_u', 'omega-3-fat_u',
                    'carbohydrates_u', 'sugars_u', 'fiber_u', 'proteins_u', 'salt_u', 'sodium_u', 'vitamin-a_u',
                    'vitamin-d_u', 'vitamin-e_u', 'vitamin-k_u', 'vitamin-c_u', 'vitamin-b1_u', 'vitamin-b2_u',
                    'vitamin-pp_u', 'vitamin-b6_u', 'vitamin-b9_u', 'folates_u', 'vitamin-b12_u', 'biotin_u',
                    'pantothenic-acid_u', 'silica_u', 'bicarbonate_u', 'potassium_u', 'calcium_u', 'phosphorus_u',
                    'iron_u', 'magnesium_u', 'zinc_u', 'manganese_u', 'fluoride_u', 'selenium_u', 'chromium_u',
                    'molybdenum_u', 'iodine_u', 'caffeine_u']



#Commencement de l'interface        
root = Tk()
root.title('Suivi de santé')
root.geometry("1334x768")

Frame1 = Frame(root, borderwidth=10, relief=GROOVE)
Frame1.place(width = 1000, height = 500, x= 150, y = 0)
Label(Frame1, text ="Bienvenue sur votre logiciel de suivi de santé.", font = 50, fg = 'red').pack()
Label(Frame1, text ="Ce logiciel permet de faire un relevé de différentes données personnelles sur votre santé:",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre poids et IMC (Indice de Masse Corporelle)",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Vos informations nutritives",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre taux de glycémie",fg = 'blue', font = 20).pack()
class User:
    def __init__(self, prenom, nom, naissance, sexe, nutrition, nutriscore, poids, taille, imc):
        self.prenom = prenom
        self.nom = nom
        self.naissance = naissance
        self.sexe = sexe  # 'H' ou 'F'
        self.nutrition = nutrition
        self.nutriscore = nutriscore
        self.taille = taille  # taille en cm
        self.poids = poids  # poids en kg
        self.age = (jour_brut - date(int(self.naissance[2]), int(self.naissance[1]),
                                     int(self.naissance[0]))).days // 365  # age calculé en fonction de la date saisie
        if type(poids) == dict and type(taille) == dict and imc == '':
            dates = list(poids)
            dates = sorted(dates)
            self.imc = {jour: calcul_IMC(poids[dates[len(dates)-1]], taille[dates[len(dates)-1]])}
        elif type(imc) == dict:
            self.imc = imc
        else:
            self.imc = {jour: calcul_IMC(poids, taille)}


    def Profil():
        #Fenetre le page profil
        newWindow = Toplevel(root)
        newWindow.title("Profil utilisateur")
        newWindow.geometry("800x600")
        Label(newWindow, text ="[Informations de l'utilisateur]", font='Arial 15 bold').grid(row = 0, column = 2)
        
        # Valeur prenom  
        Prenom = Label(newWindow, text = "Indiquer votre prénom : ")
        Prenom.grid(row = 1, column = 0)
        saisiPrenom = Entry(newWindow, width=10)
        saisiPrenom.grid(row=1, column=1)
        
        # Valeur nom  
        Nom = Label(newWindow, text = "Indiquer votre nom : ")
        Nom.grid(row = 2, column = 0)
        saisiNom = Entry(newWindow, width=10)
        saisiNom.grid(row=2, column=1)
        
        # Valeur date de naissance
        Date = Label(newWindow, text = 'Date de naissance (jj/mm/aaaa) : ').grid(row = 3, column = 0)
        Datej = Entry(newWindow, width=10)
        Datej.grid(row = 3, column = 1)
        Datem = Entry(newWindow, width=10)
        Datem.grid(row = 3, column = 2)
        Datea = Entry(newWindow, width=10)
        Datea.grid(row = 3, column = 3)
        
        # Valeur taille       
        Taille = Label(newWindow, text = "Indiquer votre taille (en cm) : ")
        Taille.grid(row = 4, column = 0)
        saisiTaille = Entry(newWindow, width=10)
        saisiTaille.grid(row=4, column=1)
        
        # Valeur poids   
        Poids = Label(newWindow, text = "Indiquer votre poids (en KG) : ")
        Poids.grid(row = 5, column = 0)
        saisiPoids = Entry(newWindow, width=10)
        saisiPoids.grid( row = 5, column = 1)

        # Valeur sexe      
        Sexe = Label(newWindow, text = "Indiquer votre sexe (Homme ou Femme) : ")
        Sexe.grid(row = 6, column = 0)
        value_sexe = StringVar() 
        bouton1 = Radiobutton(newWindow, text="Homme", variable=value_sexe, value="H")
        bouton2 = Radiobutton(newWindow, text="Femme", variable=value_sexe, value="F")
        bouton1.grid(row = 6, column = 1)
        bouton2.grid(row = 6, column = 2)
        
        #Fonction save sert a enregistrer les infos entrès dans l'interface dans le fichier de sauvegarde users_save.csv
        def save1():
            PRENOM = saisiPrenom.get()
            NOM = saisiNom.get()
            DATE = (Datej.get(), Datem.get(), Datea.get())
            TAILLE = int(saisiTaille.get())
            POIDS = int(saisiPoids.get())
            SEXE = value_sexe.get()
            user = User(PRENOM,NOM, DATE, SEXE, '', '',POIDS, TAILLE, '')
            add_user(user)
            messagebox.showinfo("succes","Votre profil a été crée")
            newWindow.destroy()
            
        #boutton sauvegarder
        Boutton_save = Button(newWindow, text = "Enregistrer", command = save1).grid(row =7, column = 0)
    # Touche pour la page profil
    Boutton_2= Button(root, text = "Profil utilisateur", command = Profil).place(width = 200, height = 50, x = 150, y = 550)

root.mainloop()