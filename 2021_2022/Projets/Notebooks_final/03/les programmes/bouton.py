from tkinter.ttk import *
from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Ceci est le titre de la fenetre principal')
root.geometry("1334x768")

Frame1 = Frame(root, borderwidth=10, relief=GROOVE)
Frame1.place(width = 1000, height = 500, x= 150, y = 0)
Label(Frame1, text ="Bienvenue sur votre logiciel de suivi de santé.", font = 50, fg = 'red').pack()
Label(Frame1, text ="Ce logiciel permet de faire un relevé de différentes données personnelles sur votre santé:",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre poids et IMC (Indice de Masse Corporelle)",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Vos informations nutritives",fg = 'blue', font = 20).pack()
Label(Frame1, text ="- Votre taux de glycémie",fg = 'blue', font = 20).pack()

def Profil():
    newWindow = Toplevel(root)
    newWindow.title("Profil utilisateur")
    newWindow.geometry("800x600")
    Label(newWindow, text ="[Informations de l'utilisateur]", font='Arial 15 bold').pack()

def Poids_IMC():
    newWindow = Toplevel(root)
    newWindow.title("test")
    newWindow.geometry("800x600")
    Label(newWindow, text ="[Informations de l'IMC]", font='Arial 15 bold').pack()
    
def Nutrition():    
    newWindow = Toplevel(root)
    newWindow.title("Nutrition")
    newWindow.geometry("800x600")
    Label(newWindow, text ="[Informations sur la nutrition]", font='Arial 15 bold').pack()

# Touche pour la page profil
Boutton_2= Button(root, text = "Profil utilisateur", command = Profil()).place(width = 200, height = 50, x = 150, y = 550)

# Touche pour la page Poids/IMC
Boutton_3= Button(root, text = "Poids et IMC", command = Poids_IMC).place(width = 200, height = 50, x = 550, y = 550)

# Touche pour la page Nutrition/Glycémie
Boutton_4= Button(root, text = "Nutrition", command = Nutrition).place(width = 200, height = 50, x = 950, y = 550)
root.mainloop()