# Les projets Python

## La grille d'évaluation

### 2 pts -- Début du projet

- groupe constitué, 1er rapport non rendu : 1 
- rapport intro : 2 

### 4 pts -- Rapport écrit

- Rapport truffé de fautes et peu explicatif : 1
- Rapport moyennenment explicatif, manque des exemples : 2 
- Bon rapport général, un peu décousu : 3 
- Très bon rapport : 4 

### 5 pts -- Exposé 

- Décousu, temps non maîtrisé : 1 
- Temps maîtrisé mais peu clair : 2 
- Temps maîtrisé et exposé compréhensif : 3 
- Très bon exposé : 4
- Question répondue : +1

### 9 pts -- Programme

- Programme structuré (docstrings, constantes, fonctions etc.) : sur 1 pt
- Difficulté (module, des exos Codingame etc.) : sur 2 pts
- Maîtrise des concepts de base (boucles, if, fonctions, structures etc.) : sur 5 pts 
    - les concepts de base semblent incompris majoritairement : 1
    - encore beaucoup de confusion sur qq concepts mais pas tous : 2
    - globalement ok mais des soucis sur les structures : 3
    - bon programme, qq petites erreurs : 4
    - on sent que les concepts du S1 sont maîtrisés : 5
- Le programme fonctionne globalement : sur 1 pt
    