"""
balles.py

Une mini animation de balles rebondissantes. Utilisation de pygame
"""

# ---
# --- IMPORT

import random
import pygame


# ---
# --- CONSTANTES

DIM = 600
CENTRE = DIM // 2, DIM // 2
RAYON_MIN = 5
VITESSE_MIN = 2
VITESSE_MAX = 5
SMALL = 10
EPSILON = 5

COULEUR_FOND = pygame.Color('white')
DELAI_MS = 10

ROUGE = 0
VERT = 1
BLEU = 2

# ---
# --- FONCTIONS UTILITAIRES

def alea(a, b):
    return random.randint(a, b)

# ---
# --- CLASSES

class Balle:

    def __init__(self, ecran, rayon, couleur, position, vitesse):
        self.ecran = ecran
        self.rayon = rayon
        self.couleur = couleur
        self.x, self.y = position
        self.vx, self.vy = vitesse

    def position(self):
        return self.x, self.y

    def se_dessine(self):
        pygame.draw.circle(self.ecran, self.couleur, self.position(), self.rayon)

    def collision_mur_est(self):
        return self.x >= DIM - self.rayon    

    def collision_mur_ouest(self):
        return self.x <= self.rayon    

    def collision_mur_nord(self):
        return self.y <= self.rayon    

    def collision_mur_sud(self):
        return self.y >= DIM - self.rayon    

    def change_direction(self, target):
        x, y = target
        dx = (x - self.x) + alea(-EPSILON, EPSILON)
        dy = (y - self.y) + alea(-EPSILON, EPSILON)
        self.vx = 0 if dx == 0 else dx/abs(dx)
        self.vy = 0 if dy == 0 else dy/abs(dy)

    def bouge(self):
        self.x += self.vx
        self.y += self.vy
        if self.collision_mur_est():
            self.x = DIM - self.rayon
            self.vx = -self.vx
        if self.collision_mur_ouest():
            self.x = self.rayon
            self.vx = -self.vx
        if self.collision_mur_nord():
            self.y = self.rayon
            self.vy = -self.vy
        if self.collision_mur_sud():
            self.y = DIM - self.rayon
            self.vy = -self.vy

class Animation:

    def __init__(self, nb_balles):
        self.ecran = pygame.display.set_mode((DIM, DIM))
        self.balles = [Balle(self.ecran, RAYON_MIN, 
            self.monochrome(ROUGE),
            self.presque_centre(), 
            (alea(VITESSE_MIN, VITESSE_MAX), alea(VITESSE_MIN, VITESSE_MAX))) for _ in range(nb_balles)]
        self.end = False
        self.mouse = CENTRE

    def reglages(self):
        pygame.init()
        pygame.display.set_caption('Balles...')
        self.ecran.fill(COULEUR_FOND)        

    def presque_centre(self):
        x, y = CENTRE
        return x + alea(-SMALL, SMALL), y + alea(-SMALL, SMALL)

    def monochrome(self, teinte):
        autres = alea(0, 150)
        if teinte == ROUGE:
            return pygame.Color(255, autres, autres)
        if teinte == VERT:
            return pygame.Color(autres, 255, autres)
        if teinte == BLEU:
            return pygame.Color(autres, autres, 255)

    def couleur_alea(self):
        return pygame.Color(alea(0, 255), alea(0, 255), alea(0, 255))

    def ajoute_balle(self):
        self.balles.append(Balle(self.ecran, RAYON_MIN, 
            self.monochrome(ROUGE),
            self.presque_centre(), 
            (alea(VITESSE_MIN, VITESSE_MAX), alea(VITESSE_MIN, VITESSE_MAX))))

    def se_dessine(self):
        for balle in self.balles:
            balle.se_dessine()

    def anime(self):
        for balle in self.balles:
            balle.bouge()

    def change_direction(self, target):
        for balle in self.balles:
            balle.change_direction(target)

    def efface(self):
        self.ecran.fill(COULEUR_FOND)        

    def pause(self):
        pygame.time.delay(DELAI_MS)

    def refresh(self):
        pygame.display.flip()            

    def stop(self):
        pygame.quit()

    def start(self):
        while not self.end:
            self.pause()
            self.efface()
            self.anime()
            self.se_dessine()
            self.refresh()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.end = True
                    self.stop()
                if event.type == pygame.MOUSEMOTION:
                    self.change_direction(event.pos)
                if event.type == pygame.MOUSEBUTTONUP:
                    self.ajoute_balle()

# ---
# --- MAIN

test = Animation(5)
test.start()
