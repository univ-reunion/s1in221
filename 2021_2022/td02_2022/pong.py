"""
pong.py

Another PONG game...
"""

import random
import pygame
from constantes import *


class Paddle:

    def __init__(self, p_id, screen):
        self.screen = screen
        self.pid = p_id
        self.rect = pygame.Rect(PADDLES_POSITIONS[p_id], (PADDLE_WIDTH, PADDLE_HEIGHT))

    def draw(self):
        pygame.draw.rect(self.screen, COLOR, self.rect)


class Ball:

    def __init__(self, screen):
        self.screen = screen
        self.x = WIDTH // 2
        self.y = HEIGHT // 2
        self.direction = 0, 0

    def pos(self):
        return self.x, self.y

    def draw(self):
        pygame.draw.circle(self.screen, COLOR, self.pos(), BALL_RADIUS)


class Board:

    def __init__(self, ctrl):
        self.ctrl= ctrl
        self.screen = ctrl.screen
        self.paddles = [Paddle(0, self.screen), Paddle(1, self.screen)]
        self.ball = Ball(self.screen)

    def draw(self):
        pygame.draw.line(self.screen, DECOR_COLOR, (0, BORDER_TOP), (WIDTH, BORDER_TOP), width=BORDER_THICK)
        pygame.draw.line(self.screen, DECOR_COLOR, (0, HEIGHT - BORDER_TOP), (WIDTH, HEIGHT - BORDER_TOP), width=BORDER_THICK)
        for y in range(BORDER_TOP, HEIGHT - BORDER_TOP, DASH + DASH//2):
            pygame.draw.line(self.screen, DECOR_COLOR, 
                            (WIDTH // 2 - 1, y), (WIDTH // 2 - 1, y + DASH), width=2)  

    def draw_paddles(self):
        for paddle in self.paddles:
            paddle.draw()

    def draw_ball(self):
        self.ball.draw()


class Pong:

    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.board = Board(self)
        self.player_id = 0
        self.scores = [5, 17]
        self.end = False
        self.font = None

    def draw_scores(self):
        for p_id in (0, 1):
            score = self.font.render(f'{self.scores[p_id]:02}', True, COLOR)
            self.screen.blit(score, SCORES_POSITIONS[p_id])


    def refresh(self):
        pygame.display.flip()

    def settings(self):
        pygame.init()
        pygame.display.set_caption('Another PONG game...')
        self.font = pygame.font.SysFont('Uroob', 38, bold=True)
        self.screen.fill(BG_COLOR)

    def start(self):
        self.settings()
        self.board.draw()
        self.board.draw_paddles()
        self.board.draw_ball()
        self.loop()
    

    def loop(self):
        while not self.end:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    self.end = True
            self.draw_scores()
            self.refresh()
        pygame.quit()
    
mon_jeu = Pong()
mon_jeu.start()
