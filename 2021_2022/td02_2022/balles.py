"""
balles.py

Une mini animation de balles rebondissantes. Utilisation de pygame
"""

# ---
# --- IMPORT

import random
import pygame


# ---
# --- CONSTANTES

DIM = 400
CENTRE = DIM // 2, DIM // 2
RAYON_MIN = 10
RAYON_MAX = 30
VITESSE_MIN = 2
VITESSE_MAX = 5

GRIS_CLAIR = pygame.Color(240, 240, 240)
BLANC = pygame.Color('white')
COULEUR_FOND = GRIS_CLAIR
DELAI_MS = 10

ROUGE = 0
VERT = 1
BLEU = 2
TEINTES = {pygame.K_r: ROUGE, pygame.K_v: VERT, pygame.K_b: BLEU}

# ---
# --- FONCTIONS UTILITAIRES

def alea(a, b):
    return random.randint(a, b)

# ---
# --- CLASSES

class Balle:

    def __init__(self, ecran, rayon, couleur, position, vitesse):
        self.ecran = ecran
        self.rayon = rayon
        self.couleur = couleur
        self.x, self.y = position
        self.vx, self.vy = vitesse

    def position(self):
        return self.x, self.y

    def se_dessine(self):
        pygame.draw.circle(self.ecran, self.couleur, self.position(), self.rayon)

    def collision_mur_est(self):
        return self.x >= DIM - self.rayon    

    def collision_mur_ouest(self):
        return self.x <= self.rayon    

    def collision_mur_nord(self):
        return self.y <= self.rayon    

    def collision_mur_sud(self):
        return self.y >= DIM - self.rayon    


    def bouge(self):
        self.x += self.vx
        self.y += self.vy
        if self.collision_mur_est():
            self.x = DIM - self.rayon
            self.vx = -self.vx
        if self.collision_mur_ouest():
            self.x = self.rayon
            self.vx = -self.vx
        if self.collision_mur_nord():
            self.y = self.rayon
            self.vy = -self.vy
        if self.collision_mur_sud():
            self.y = DIM - self.rayon
            self.vy = -self.vy

class Animation:

    def __init__(self, nb_balles, teinte=BLEU):
        self.ecran = pygame.display.set_mode((DIM, DIM))
        self.teinte = teinte
        self.balles = [Balle(self.ecran, alea(RAYON_MIN, RAYON_MAX), 
            self.monochrome(self.teinte),
            CENTRE, 
            (-1**alea(1,2)*alea(VITESSE_MIN, VITESSE_MAX), -1**alea(1,2)*alea(VITESSE_MIN, VITESSE_MAX))) for _ in range(nb_balles)]
        self.end = False
        self.pause = False

    def reglages(self):
        pygame.init()
        pygame.display.set_caption('Balles...')
        self.ecran.fill(COULEUR_FOND)        

    def monochrome(self, teinte):
        autres = alea(0, 150)
        if teinte == ROUGE:
            return pygame.Color(255, autres, autres)
        if teinte == VERT:
            return pygame.Color(autres, 255, autres)
        if teinte == BLEU:
            return pygame.Color(autres, autres, 255)

    def couleur_alea(self):
        return pygame.Color(alea(0, 255), alea(0, 255), alea(0, 255))

    def ajoute_balle(self):
        self.balles.append(Balle(self.ecran, alea(RAYON_MIN, RAYON_MAX), 
            self.monochrome(self.teinte),
            CENTRE, 
            (-1**alea(1,2)*alea(VITESSE_MIN, VITESSE_MAX), -1**alea(1,2)*alea(VITESSE_MIN, VITESSE_MAX))))
        print(len(self.balles))

    def change_teinte(self, teinte):
        self.teinte = teinte
        for balle in self.balles:
            balle.couleur = self.monochrome(teinte)

    def se_dessine(self):
        for balle in self.balles:
            balle.se_dessine()

    def anime(self):
        for balle in self.balles:
            balle.bouge()

    def efface(self):
        self.ecran.fill(COULEUR_FOND)        

    def delai(self):
        pygame.time.delay(DELAI_MS)

    def refresh(self):
        pygame.display.flip()            

    def stop(self):
        pygame.quit()

    def gerer_clavier(self, event):
        if event.key == pygame.K_SPACE:
            self.pause = not self.pause
        elif event.key == pygame.K_q:
            self.end = True
        elif event.key in TEINTES:
            self.change_teinte(TEINTES[event.key])


    def start(self):
        self.reglages()
        while not self.end:
            if not self.pause:
                self.delai()
                self.efface()
                self.anime()
                self.se_dessine()
                self.refresh()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.end = True
                if event.type == pygame.MOUSEBUTTONUP:
                    self.ajoute_balle()
                if event.type == pygame.KEYDOWN:
                    self.gerer_clavier(event)
        self.stop()


# ---
# --- MAIN

test = Animation(5)
test.start()
