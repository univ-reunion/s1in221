
ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def caesar(msg, n):
    result = ''
    if n < 0:
        coef = -1
    else:
        coef = 1
    for indice, c in enumerate(msg):
        result += ALPHA[(ALPHA.index(c) + coef * indice + n) % 26]
    return result

def by_rotor(msg, rotor, decode=False):
    if decode:
        s_to, s_from = ALPHA, rotor
    else:
        s_to, s_from = rotor, ALPHA
    return ''.join([s_to[s_from.index(c)] for c in msg])

def coder(msg, rotors, n):
    code = caesar(msg, n)
    for rotor in rotors:
        code = by_rotor(code, rotor)
    return code

def decoder(code, rotors, n):
    for rotor in rotors[::-1]:
        code = by_rotor(code, rotor, decode=True)
    return caesar(code, -n)

def enigma(op, num, rotors, msg):
    f_enigma = {'ENCODE':coder, 'DECODE':decoder}
    print(f_enigma[op](msg, rotors, num))

def get_datas():
    operation = input()
    pseudo_random_number = int(input())
    rotors = [input() for _ in range(3)]
    message = input()
    return operation, pseudo_random_number, rotors, message

# MAIN

op, num, rotors, msg = get_datas()
enigma(op, num, rotors, msg)