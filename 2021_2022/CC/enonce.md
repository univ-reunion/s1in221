# Contrôle continu

- **thème** : Programmation orientée objet
- **durée** : 45mn

Une **file** est une structure de données linéaire où l'arrivée de nouveaux éléments se fait à une extrémité et le retrait des éléments se fait à l'autre extrémité. Il s'agit donc d'une structure dite _fifo_ : _first in first out_ (premier arrivé, premier sorti).

## Modèle

On utilise une structure de liste chainée pour implémenter une file. Un _maillon_ de la file contient une valeur (par souci de simplification, nous allons considérer ici une file d'entiers) et une référence vers un autre _maillon_.

### Q1. La classe `Maillon`

Définir une classe `Maillon` qui comporte deux propriétés :

- `value` qui sera une valeur entière
- `suivant` qui vaudra `None` (valeur par défaut) ou une référence vers un autre `Maillon` 

À l'initialisation, on doit passer la valeur (on ne fera aucune vérification que la valeur est bien entière). 

#### Exemple

Créons trois maillons, chainés les uns aux autres :

```python
>>> m1 = Maillon(10)
>>> m2 = Maillon(-1, m1)
>>> m3 = Maillon(8, m2)
```

Voici un diagramme _pythontutor_ du résultat :

![maillons_complet.png](maillons_complet.png)

On supposera définie une méthode `__str__` qui permet d'afficher les maillons sous la forme `( )-->` (la valeur entre parenthèses et une flèche vers le suivant. Par exemple :

```python
>>> print(m3)
(8)--> 

>>> print(m3.suivant)
(-1)--> 

>>> print(m1.suivant)
None
```


## 2. La classe `File`

#### Q2.1 Définir l'initialiseur

Une File est une liste chaînée de 0, 1 ou plusieurs maillons. L'objet `File` possède deux propriétés :

- `tete` qui vaut soit `None` (quand la file ne contient aucun maillon, ce sera la valeur par défaut), soit une référence vers le premier maillon, c'est-à-dire celui qu'on va retirer en premier
- `queue` qui vaut soit `None` (quand la file ne contient aucun maillon, ce sera la valeur par défaut), soit une référence vers le dernier maillon, c'est-à-dire là où l'on va ajouter le prochain arrivant.

Voici la liste chainée précédente, vue comme une file (on supposera donnée la méthode `__str__` pour la file qui donne cet affichage) :

```
tete : (8)--> (-1)--> queue : (10)--> ∅
```

#### Q2.2 `est_vide`

Définir la méthode `est_vide` qui renvoie `True` si et seulement si la file est vide.

#### Q2.3 `add`

Définir la méthode `add` qui prend un entier `a` en paramètre et ajoute cet entier dans la file (sous la forme d'un nouveau maillon). Voici les étapes à réaliser :

- Créer un nouveau Maillon contenant la valeur `a` :
   ```
   (a)--> ∅
   ```
- Si la File n'est pas vide, faire pointer le suivant de la _queue_ vers ce nouveau Maillon :
    ```
    tete : ( )--> ...    queue : ( )--> (a)--> ∅
    ```
- Sinon, faire pointer la _tete_ vers ce nouveau Maillon :
    ```
    tete : (a)--> ∅
    ```
- Dans les 2 cas faire pointer _queue_ vers le nouveau Maillon :
   ```
  tete : ( )-->  ...  ( )--> queue : (a)--> ∅
  ```

  ou

  ```
  tete : queue : (a)--> ∅
  ```


#### Q2.4 `pop`

`pop` est la méthode pour retirer un élément de la file. Le retrait se fait **toujours** du côté de la `tete`.

1. Dessiner la file contenant les valeurs 42 (première arrivée) et 24. Faites bien apparaître les maillons, le lien entre eux ainsi que la référence _tete_ et la _queue_
2. On retire une valeur, et re-dessine la nouvelle file.
3. Quelles sont les étapes de retrait d'un élément si la file contient plus d'un élément ?
4. Quelles sont les étapes de retrait dans le cas d'un seul élément ?
5. Définir la méthode `pop`

## Exemples

```python
>>> F = File()
>>> print(F)
tete : queue : ∅

>>> F.add(42)
>>> print(F)
tete : queue : (42)--> ∅

>>> F.add(24)
>>> print(F)
tete : (42)-->  queue : (24)--> ∅

>>> F.add(1)
>>> print(F)
tete : (42)-->  (24)-->  queue : (1)--> ∅

>>> F.pop()
42
>>> print(F)
tete : (24)-->  queue : (1)--> ∅

>>> F.pop()
24
>>> F.pop()
1
>>> F.est_vide()
True

>>> F.pop()
>>> print(F)
tete : queue : ∅
```

