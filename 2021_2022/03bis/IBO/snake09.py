import random
import time
from constantes import *

def xy_vers_pixels(coords):
    x, y = coords
    return x * SIZE, y * SIZE

class Pomme:
    
    def __init__(self, pos, arene, date_peremption):
        self.arene = arene
        self.ecran = arene.ecran
        self.__pos = pos
        self.__date_peremption = date_peremption

    @property
    def pos(self):
        return self.__pos

    @property
    def date_peremption(self):
        return self.__date_peremption

    def se_dessine(self):
        px, py = xy_vers_pixels(self.pos)
        pygame.draw.rect(self.ecran, COULEUR_POMME, pygame.Rect(px, py, SIZE, SIZE))



class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.__pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.__tete = -1
        self.__direction = 0, 0

    # -- INTERFACE

    @property
    def pos_tete(self):
        """renvoie la position de la tête"""
        return self.__pos[self.__tete]

    @pos_tete.setter
    def pos_tete(self, nouvelle_pos):
        """change la position de la tête"""
        self.__pos.append(nouvelle_pos)

    @property
    def pos_queue(self):
        """renvoie la position de la queue"""
        return self.__pos[0]

    @pos_queue.setter
    def pos_queue(self, nouvelle_pos):
        """ajoute une position en queue de serpent"""
        self.__pos.insert(0, nouvelle_pos)

    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, delta):
        self.__direction = delta

    def declenche_event(self, event):
        pygame.event.post(pygame.event.Event(event))

    def se_mord(self):
        return any(self.__pos[i] == self.pos_tete for i in range(len(self.__pos)-1))

    def dans_arene(self):
        tete_x, tete_y = self.pos_tete
        return 0 <= tete_x < COLS and 0 <= tete_y < ROWS

    def crash(self):
        if self.se_mord() or not self.dans_arene():
            self.declenche_event(CRASH_EVENT)
            return True
        return False

    def bouge(self):
        x, y = self.pos_tete
        dx, dy = self.direction
        nouvelle_pos = x + dx, y + dy
        self.pos_tete =  nouvelle_pos
        if not self.crash():
            if self.arene.une_pomme():
                self.declenche_event(SCORE_EVENT)
                self.mange()
            self.__pos.pop(0)

    def mange(self):
        pos_queue = self.pos_queue
        for _ in range(CROISSANCE):
            self.pos_queue = pos_queue

    # --- Partie VUE

    def se_dessine(self):
        for coords in self.__pos:
            px, py = xy_vers_pixels(coords)
            pygame.draw.rect(self.ecran, COULEUR_CORPS, pygame.Rect(px, py, SIZE, SIZE))
        px, py = xy_vers_pixels(self.pos_tete) 
        pygame.draw.rect(self.ecran, COULEUR_TETE, pygame.Rect(px, py, SIZE, SIZE))

    # def efface(self, pos):
    #     """le serpent peut effacer une case : sa queue par exemple"""
    #     px, py = xy_vers_pixels(pos) 
    #     pygame.draw.rect(self.ecran, COULEUR_FOND, pygame.Rect(px, py, SIZE, SIZE))


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)
        self.pommes = {}
        self.date = int(time.time()) # pour gestion apparition pommes

    def random_position(self):
        return random.randrange(COLS), random.randrange(ROWS)

    def anime(self):
        self.gestion_pommes()
        self.serpent.bouge()
        for pomme in self.pommes.values():
            pomme.se_dessine()

    def serpent_change_direction(self, dx, dy):
        self.serpent.direction = dx, dy

    def une_pomme(self):
        """renvoie True pour prévenir le serpent qu'une pomme
        a été trouvée et retire cette pomme des pommes"""
        position = self.serpent.pos_tete
        if position in self.pommes:
            self.retrait_pomme(position)
            return True

    def retrait_pomme(self, pos):
        self.pommes.pop(pos)

    def retire_pommes(self):
        """retire les pommes périmées"""
        date = int(time.time())
        for pos in list(self.pommes.keys()):
            pomme = self.pommes[pos]
            if pomme.date_peremption < date:
                self.retrait_pomme(pos)

    def ajout_pomme(self, horodatage):
        pos = self.random_position()
        self.pommes[pos] = Pomme(pos, self, horodatage)

    def gestion_pommes(self):
        date = int(time.time())
        delai_apparition = random.randint(*DELAI_POMME)
        delai_peremption = random.randint(*VALIDITE)
        if (date - self.date) > delai_apparition:
            self.ajout_pomme(date + delai_peremption)
            self.date = date
        self.retire_pommes() 

    # -- Vue

    def se_dessine(self):
        self.serpent.se_dessine()
        for pomme in self.pommes.values():
            pomme.se_dessine()

class Jeu:
    
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)
        self.fini = False 
        self.__debut = None # heure de lancement du temps écoulé
        self.score = 0
        self.mode = MODE_START 
        self.font = None

    def start(self):
        pygame.init()
        self.font = pygame.font.SysFont(None, 24)
        pygame.display.set_caption('Another SNAKE game...')
        self.ecran.fill(COULEUR_FOND)
        self.loop()

    def move_events(self, event):
        if event.key == pygame.K_DOWN:
            self.arene.serpent_change_direction(0, 1)
        elif event.key == pygame.K_UP:
            self.arene.serpent_change_direction(0, -1)
        elif event.key == pygame.K_LEFT:
            self.arene.serpent_change_direction(-1, 0)
        elif event.key == pygame.K_RIGHT:
            self.arene.serpent_change_direction(1, 0)
        elif event.key == pygame.K_SPACE:
            self.mode = 1 - self.mode

    def gerer_event(self, event):
        if event.type == pygame.KEYDOWN:
            if self.mode == MODE_START:
                self.__debut = int(time.time())
                self.mode = MODE_MOVE
            if self.mode != MODE_STOP:
                self.move_events(event)
            elif event.key == pygame.K_q:
                self.fini = True
        elif event.type == SCORE_EVENT:
            self.score += 1
        elif event.type == CRASH_EVENT and self.mode == MODE_MOVE:
            self.game_over()
            self.mode = MODE_STOP
        elif event.type == pygame.QUIT:
            self.fini = True

    def refresh_et_pause(self):
        pygame.display.flip()
        pygame.time.delay(DELAI_MS)

    def effacer(self):
        """méthode un peu brutale d'effacer"""
        self.ecran.fill(COULEUR_FOND)        

    def temps_ecoule(self):
        return 0 if self.__debut is None else int(time.time()) - self.__debut
        
    def infos(self):
        ecoule = self.temps_ecoule()
        temps = self.font.render(f'{ecoule//60:02}:{ecoule%60:02}', True, COULEUR_TEXTE)
        score = self.font.render(f'{self.score:03}', True, COULEUR_TEXTE)
        self.ecran.blit(temps, POSITION_TEMPS)
        self.ecran.blit(score, POSITION_SCORE)

    def game_over(self):
        big = pygame.font.SysFont(None, 36)
        fin = big.render('GAME OVER', True, COULEUR_TEXTE)
        self.ecran.blit(fin, CENTRE)
        self.refresh_et_pause()
    
    def loop(self):
        while not self.fini:
            for event in pygame.event.get():
                self.gerer_event(event)
            if self.mode != MODE_STOP:
                self.effacer()
                if self.mode == MODE_MOVE:
                    self.arene.anime()
                self.arene.se_dessine()
                self.infos()
                self.refresh_et_pause()
        pygame.quit()    
    


snake_game = Jeu()
snake_game.start()