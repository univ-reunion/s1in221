import pygame
from constantes import *

class Jeu:
    
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR))
    
    def start(self):
        pygame.init()
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            reponse = input('Tapez ce que vous voulez, puis entrée pour terminer')
            fini = True
        pygame.quit()    
    
mon_jeu = Jeu()
mon_jeu.start()