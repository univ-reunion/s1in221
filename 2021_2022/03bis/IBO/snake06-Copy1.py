import pygame
from constantes import *

class Serpent:
    def __init__(self, arene):
        self.arene = arene
        self.ecran = arene.ecran
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.tete = -1


class Arene:
    def __init__(self, jeu):
        self.jeu = jeu
        self.ecran = jeu.ecran
        self.serpent = Serpent(self)


class Jeu:
    def __init__(self):
        self.ecran = pygame.display.set_mode((LARGEUR, HAUTEUR)) 
        self.arene = Arene(self)   

    def start(self):
        pygame.init()
        self.loop()

    def loop(self):
        fini = False
        while not fini:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    fini = True
        pygame.quit()    
    
snake_game = Jeu()
snake_game.start()