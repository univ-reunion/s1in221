class Jeu:

    def __init__(self, msg):
        self.message = msg
    
    def coucou(self):
        print(self.message)

mon_jeu = Jeu('Salut')
mon_autre_jeu = Jeu('Bonjour, ça va ?')
mon_jeu.coucou()
mon_autre_jeu.coucou()