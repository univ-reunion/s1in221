import pygame

LARGEUR = 600
HAUTEUR = 450
TOP_BORDER = 30
SIZE = 10
LENGTH = 4

TOP_MARGE = 5
POSITION_TEMPS = LARGEUR - 50, TOP_MARGE
POSITION_SCORE = 10, TOP_MARGE
CENTRE = LARGEUR // 2 - 70, HAUTEUR // 2 - 40

COLS = LARGEUR // SIZE
ROWS = HAUTEUR // SIZE

COULEUR_TETE = pygame.Color('lightcoral')
COULEUR_CORPS = pygame.Color('lightslategrey')
COULEUR_FOND = pygame.Color('sienna')
COULEUR_POMME = pygame.Color('greenyellow')
COULEUR_TEXTE = pygame.Color('darkred')

DELAI_MS = 100  # milliseconds

CROISSANCE = 4
NB_POMMES = 3
DELAI_POMME = 3, 6 # une pomme apparait entre 3 et 6 secondes 
VALIDITE = 10, 15 # les pommes pourrissent au bout d'un délai variable de 10 à 15s

MODE_PAUSE = 0
MODE_MOVE = 1
MODE_START = 2
MODE_STOP = 3

SCORE_EVENT = pygame.USEREVENT
CRASH_EVENT = pygame.USEREVENT + 1
