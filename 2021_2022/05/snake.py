import pygame
import random
from constantes import *

def model_to_xy(ij):
    i, j = ij
    return i * SIZE, j * SIZE

class Apple:
    
    def __init__(self, pos, arena, screen):
        self.pos = pos
        self.arena = arena
        self.screen = screen

    def position(self):
        return self.pos

    def draw(self):
        x, y = model_to_xy(self.pos)
        pygame.draw.rect(self.screen, APPLE_COLOR, pygame.Rect(x, y, SIZE, SIZE))


class Snake:

    def __init__(self, arena, screen):
        self.arena = arena
        self.pos = [(COLS//2 + i, ROWS//2) for i in range(-LENGTH//2, LENGTH//2)]
        self.head = -1
        self.delta = 0, 0
        self.screen = screen

    def __len__(self):
        return len(self.pos)

    def change_direction(self, dx, dy):
        self.delta = dx, dy
 
    # def move(self):
    #     h = self.head
    #     x, y = self.pos[h]
    #     h = (h + 1) % len(self)
    #     self.pos[h][0] = x + self.delta[0]
    #     self.pos[h][1] = y + self.delta[1]
    #     self.head = h

    def move(self):
        x, y = self.pos[self.head]
        dx, dy = self.delta
        self.pos.append((x + dx, y + dy))
        self.pos.pop(0)


    def position(self):
        return self.pos[self.head]

    def eat(self):
        x, y = self.pos[0]
        for _ in range(GROWTH):
            self.pos.insert(0, (x, y))

    def pause(self):
        self.delta = 0, 0

    def draw(self):
        # draw body snake
        for i in range(len(self)):
            x, y = model_to_xy(self.pos[i])
            pygame.draw.rect(self.screen, BODY_COLOR, pygame.Rect(x, y, SIZE, SIZE))
        # draw head
        x, y = model_to_xy(self.pos[self.head])
        pygame.draw.rect(self.screen, HEAD_COLOR, pygame.Rect(x, y, SIZE, SIZE))


class Arena:
    
    def __init__(self, game):
        self.game = game
        self.screen = self.game.screen
        self.snake = Snake(self, self.screen)
        self.apples = [Apple(self.random_position(), self, self.screen) for _ in range(3)]

    def random_position(self):
        return random.randrange(COLS), random.randrange(ROWS)

    def clear(self):
        self.screen.fill(BG_COLOR)


    def remove_apple(self, apple):
        self.apples.remove(apple)

    def animate(self):
        self.snake.move()
        for apple in self.apples:
            if apple.position() == self.snake.position():
                self.remove_apple(apple)
                self.snake.eat()


    def snake_change_direction(self, dx, dy):
        self.snake.change_direction(dx, dy)

    def pause(self):
        self.snake.pause()

    def draw(self):
        self.clear()
        self.snake.draw()
        for apple in self.apples:
            apple.draw()


class Game:

    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.arena = Arena(self)
        self.gameover = False
        self.mode = MODE_PAUSE
        
    def start(self):
        pygame.init()
        pygame.display.set_caption('Another SNAKE game...')
        self.screen.fill(BG_COLOR)
        self.loop()
    
    def stop(self):
        pygame.quit()
    
    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN:
                self.arena.snake_change_direction(0, 1)
                self.mode = MODE_MOVE
            elif event.key == pygame.K_UP:
                self.arena.snake_change_direction(0, -1)
                self.mode = MODE_MOVE
            elif event.key == pygame.K_LEFT:
                self.arena.snake_change_direction(-1, 0)
                self.mode = MODE_MOVE
            elif event.key == pygame.K_RIGHT:
                self.arena.snake_change_direction(1, 0)
                self.mode = MODE_MOVE
            elif event.key == pygame.K_SPACE:
                self.mode = MODE_PAUSE
                self.arena.pause()
            else:
                self.gameover = True


    def loop(self):
        while not self.gameover:
            for event in pygame.event.get():
                self.handle_event(event)
            if self.mode == MODE_MOVE:
                self.arena.animate()
            self.arena.draw()
            pygame.display.flip()
            pygame.time.delay(CLOCK_TICK) 
        self.stop()


test = Game()
test.start()