import pygame

# DIMENSIONS ET COORDONNEES
# ---
WIDTH = 600
HEIGHT = 450
SIZE = 10
COLS = WIDTH // SIZE
ROWS = HEIGHT // SIZE
LENGTH = 4
GROWTH = 2


# COULEURS
# ---
HEAD_COLOR = pygame.Color('lightcoral')
BODY_COLOR = pygame.Color('lightslategrey')
APPLE_COLOR = pygame.Color('greenyellow')
BG_COLOR = pygame.Color('sienna')
# BG_COLOR = '#000000'

# GAME MANAGEMENT
# ---
MODE_PAUSE = 0
MODE_MOVE = 1
MODE_STOP = 2

CLOCK_TICK = 100  # milliseconds