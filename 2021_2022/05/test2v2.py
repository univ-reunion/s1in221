import pygame

WIDTH = 600
HEIGHT = 400
SIZE = 20
COLS = WIDTH // SIZE
ROWS = HEIGHT // SIZE
COLORS = ['sienna', 'orange']

class Board:

    def __init__(self, game):
        self.game = game
        self.grid = {}

    def play(self, pos):
        x, y = pos
        self.grid[x,y] = self.game.player
    
    def draw(self):
        for x, y in self.grid:
            pygame.draw.rect(self.game.screen, COLORS[self.grid[x, y]], pygame.Rect(x, y, SIZE, SIZE))


class Game:

    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.board = Board(self)
        self.player = 0
        self.gameover = False

    def start(self):
        pygame.init()
        self.screen.fill('white')
        self.loop()
    
    def loop(self):
        while not self.gameover:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    self.gameover = True
                if event.type == pygame.MOUSEBUTTONDOWN:
                    self.board.play(event.pos)
                    # print(self.board.grid)
                    self.player = 1 - self.player

            self.board.draw()
            pygame.display.flip()
            
test = Game()
test.start()